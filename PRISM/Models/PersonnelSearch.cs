﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace PRISM.Models
{
    public class PersonnelSearch
    {
        [Display(Name = "Name", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "NameRequired")]
        public string Name { get; set; }

        [Display(Name = "Rank", ResourceType = typeof(Resource))]
        public string Rank { get; set; }

        public IEnumerable<SelectListItem> Ranks { get; set; }
    }
}