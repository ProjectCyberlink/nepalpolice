﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class TreeViewNode
    {
        public string text { get; set; }
        public string id { get; set; }
        public string classes { get; set; }
        public bool hasChildren { get; set; }

        public TreeViewNode[] children { get; set; }
        public bool expanded { get; set; }

    }
}