﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class spMeasurinUnit
    {
        public string unitNmae { get; set; }
        public string unitShort { get; set; }
        public string description { get; set; }
        public string  entryid { get; set; }
        public DateTime entrydate { get; set; }
        public string pk_UoM { get; set; }
    }
}