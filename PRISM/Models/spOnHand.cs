﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class spOnHand
    {
        public string ale { get; set; }
        public string item { get; set; }

        [Display(Name = "Item Name", ResourceType = typeof(Resource))]
        public string ITEMNAME { get; set; }
        public decimal qty { get; set; }
        public string SPECIFICATION { get; set; }
        public string Unit_of_measure { get; set; }
        public string item_Desc { get; set; }

        public int itemCatCode { get; set; }
        public decimal vat  { get; set; }
        public string extraexp { get; set; }
        public decimal totalexp { get; set; }
        public DateTime ENTRYDATE { get; set; }
        public string ENTRYFOR { get; set; }
    }
}