﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class spConsumption
    {
        public string FK_ASSET_ID { get; set; }
        public string ITEMNAME { get; set; }
        public string allowedQty { get; set; }
        public decimal consumpPeriod { get; set; }
        public string ConsumpTime { get; set; }
        public string Specification { get; set; }
        public string itemDesc { get; set; }
        public string Dateid { get; set; }
    }
}