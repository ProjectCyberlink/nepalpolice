﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRISM
{
    [MetadataType(typeof(WeaponDetailMetadata))]
    public partial class TBL_WEAPON_DETAIL
    {
        public TBL_WEAPON_DISTRIBUTION WeaponDistribution { get; set; }
        public TBL_WEAPON_PURCHASE_INFO PurchaseInfo { get; set; }        
    }

    public class WeaponDetailMetadata
    {        
        public string PK_WEAPONS_ID { get; set; }

        [Display(Name = "WeaponName", ResourceType=typeof(Resource))]
        [Required(ErrorMessageResourceName="WeaponNameRequired",ErrorMessageResourceType=typeof(Resource))]
        [Remote("DoesWeaponNameWithSerialExists", "Weapons", AdditionalFields = "WEAPON_NAME, PK_WEAPONS_ID", HttpMethod = "POST", ErrorMessageResourceName = "WeaponNameExist", ErrorMessageResourceType = typeof(Resource))]
        public string WEAPON_NAME { get; set; }

        [Display(Name = "WeaponType", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "WeaponNameRequired", ErrorMessageResourceType = typeof(Resource))]
        public string FK_MASTER_INV { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Resource))]
        public string DESCRIPTION { get; set; }

        [Display(Name = "SerialNum", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "SerialNumRequired", ErrorMessageResourceType = typeof(Resource))]
        [Remote("DoesSerialNumberExists", "Weapons", AdditionalFields = "PK_WEAPONS_ID", HttpMethod = "POST", ErrorMessageResourceName = "SerialNumExists", ErrorMessageResourceType = typeof(Resource))]
        public string SERIAL_NO { get; set; }

        [Display(Name = "BookPageId", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "BookPageRequired", ErrorMessageResourceType = typeof(Resource))]
        public int BOOKPAGEID { get; set; }
        
    }
}