﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class paybookModel
    {
        public string pbITEMNAME { get; set; }
        public DateTime? pbPOSTINGDATE { get; set; }
        public string pbENTRYTYPE { get; set; }
        public string pbENTRYFOR { get; set; }
        public string pbDesc { get; set; }
        public decimal pbQUANTITY { get; set; }
        public int pbitemCatCode { get; set; }
        public string pbvendorid { get; set; }
        public int pbExtraExp { get; set; }
        public int pbTotalExp { get; set; }
        public string pbCOMMENTS { get; set; }
        public string pbhandedby { get; set; }
        public string pbAddress { get; set; }
        public int pbWAIVED { get; set; }
        public string pbWAIVEDReason { get; set; }
        public int consumpPeriod { get; set; }
        public string ConsumpTime { get; set; }
        public string Period
        {
            get
            {
                return consumpPeriod + " " + ConsumpTime;
            }
        }

        public string ExpiryDate
        {
            get
            {
                var postedDate = Convert.ToDateTime(pbPOSTINGDATE);
                DateTime? expiryDate = null;
                switch (ConsumpTime)
                {
                    case "Year":
                        expiryDate = postedDate.AddYears(consumpPeriod);
                        break;
                    case "Month":
                        expiryDate = postedDate.AddYears(consumpPeriod);
                        break;
                    case "Day":
                        expiryDate = postedDate.AddYears(consumpPeriod);
                        break;
                }
                return expiryDate.HasValue ? expiryDate.Value.ToString("yyyy-MM-dd") : null;
            }
        }

    }
}