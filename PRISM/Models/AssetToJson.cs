﻿namespace PRISM.Models
{
    public class AssetToJson
    {
        public string id { get; set; }
        public string Name { get; set; }
        public string PageID { get; set; }
        public string Description { get; set; }
        public string Spec { get; set; }
        public string UoM { get; set; }
        public string EType { get; set; }
        //EType = (alx.ENTRYTYPE == null ? " " : (alx.ENTRYTYPE == 0 ? "New Entry" : "Transferred")),
        public string TType { get; set; }
        public string Posted_Date { get; set; }
        public string QTY { get; set; }
        public string InvQty { get; set; }
        public string VAT { get; set; }
        public string IncVAT { get; set; }
        public string UnitPrice { get; set; }
        public string Code { get; set; }
        public string Open { get; set; }
        public string ExtraExp { get; set; }
        public string Expense { get; set; }
        public string Vendor { get; set; }
        public string waive { get; set; }
        public string waiveReason { get; set; }
        public string EntryFor { get; set; }
        public string EntryBy { get; set; }
        public string EntryByN { get; set; }
        public string EntryFrom { get; set; }
        public string comment { get; set; }
    }
}