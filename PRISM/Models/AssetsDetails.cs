﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class spAssetsDetails
    {
                           public string ALE{get; set;}
                           public string itemid{get; set;}
                           public string postingdate{get; set;}
                           public string Entrytype{get; set;}
                           public string Description{get; set;}
                           public string Quantity{get; set;}
                           public string InvoicedQuantity{get; set;}
                           public string VAT{get; set;}
                           public string PriceIncludeVAT{get; set;}
                           public string UnitPrice{get; set;}
                           public string ItemCategoryCode{get; set;}
                           public string ItemTrackingCode{get; set;}
                           public string ExtraExpenses{get; set;}
                           public string TotalExpenses{get; set;}
                           public string comments{get; set;}
                           public string bookpageid{get; set;}
                           public string ItemName{get; set;}
                           public string Specification{get; set;}
                           public string item_Desc{get; set;}
                           public string unitShort{get; set;}
                            public string PERSON_CODE{get; set;}
                           public string waived{get; set;}
                           public string waivedReason{get; set;}
                           public string UNIT_NAME { get; set; }
                           public string entryby { get; set; }
                           public string entryfrom { get; set; }
    }
}