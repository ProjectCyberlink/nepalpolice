﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class spWeaponsDetail
    {
        public string ITEMCATEGORYCODE { get; set; }public string ITEMNAME { get; set; }public string ITEMTYPE { get; set; }public string SPECIFICATION { get; set; }public string WEAPON_NAME { get; set; }
        public string PK_WEAPONS_ID
        { get; set; }public string DESCRIPTION { get; set; }public string SERIAL_NO { get; set; }public string BOOKPAGEID { get; set; }public string STATUS { get; set; }public string DISTRIBUTIONBY { get; set; }public string DISTRIBUTIONTO { get; set; }public string ASSIGNEDBYTO { get; set; }public string DISTRIBUTEDDATE { get; set; }
        public string EXTRAEXPENSES { get; set; }public string PURCHASEDDATE { get; set; }public string UNITPRICE { get; set; }
        public string pk_master_inv_id { get; set; }
        public string pk_wdistribution_id { get; set; }
        public string pk_purchase_id { get; set; }
    }
}