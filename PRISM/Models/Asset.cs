﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRISM
{
    [MetadataType(typeof(AssetMetadata))]
    public partial class TBL_ASSET
    {        
        public string SelectedUnitId { get; set; }

        public virtual UOM Unit { get; set; } 
    }

    public class UOM
    {
        public string UnitId { get; set; }
        public string UnitName { get; set; }
    }

    public class AssetMetadata
    {
        public string PK_ASSET_ID { get; set; }

        [Display(Name = "Unit", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType=typeof(Resource),ErrorMessageResourceName="UnitRequired")]
        public int SelectedUnitId { get; set; }
        
        [Display(Name="BookPageId", ResourceType=typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "BookPageRequired")]
        //[Required(ErrorMessage = "Please enter a book page id")]
        public Nullable<int> BOOKPAGEID { get; set; }


        [Display(Name = "ItemName", ResourceType = typeof(Resource))]
        //[Required(ErrorMessage = "Please enter a item name.")]/
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ItemRequired")]
        [Remote("DoesItemNameExists", "Assets", AdditionalFields = "PK_ASSET_ID", HttpMethod = "POST", ErrorMessage = "Item name already exists")]
        public string ITEMNAME { get; set; }

        [Display(Name = "Specification", ResourceType = typeof(Resource))]
        //[Required(ErrorMessage = "Please enter specification of the item")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "SpecificationRequired")]
        public string SPECIFICATION { get; set; }

        [Display(Name = "UOM", ResourceType = typeof(Resource))]
        //[Required(ErrorMessage = "Please select a unit.")]
        public Nullable<System.Guid> FK_UOM_ID { get; set; }

        [Display(Name = "Particulars", ResourceType = typeof(Resource))]
        public string ITEM_DESC { get; set; }
        
    }
}