﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PRISM
{
   

    [MetadataType(typeof(AssetLedgerMetadata))]
    public partial class TBL_ASSETLEDGER
    {
        [Display(Name = "Handover")]
        public string UnitOrPerson { get; set; }
        public string SelectedVendorId { get; set; }
        public virtual Vendor Vendors { get; set; }
        public string SelectedItemId { get; set; }
        public virtual Items Items { get; set; }        

    }
  

        public class Vendor
    {
        public System.Guid VendorId { get; set; }
        public string VendorName { get; set; }
    }

    public class Items
    {
        public System.Guid ItemId { get; set; }
        public string ItemName { get; set; }
    }

    public class Unit
    {
        public string UnitId { get; set; }
        public string UnitName { get; set; }
    }

    public class AssetLedgerMetadata
    {
        [Display(Name = "Unit", ResourceType=typeof(Resource))]
        //[Required(ErrorMessage = "Please select the unit")]
        [Required(ErrorMessageResourceName="UnitRequired", ErrorMessageResourceType=typeof(Resource))]
        public string ENTRYFOR { get; set; }

        [Display(Name = "Item", ResourceType = typeof(Resource))]
        //[Required(ErrorMessage = "Please select the item")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ItemRequired")]
        public int SelectedItemId { get; set; }

        [Display(Name = "Vendor", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "VendorRequired")]
        //[Required(ErrorMessage = "Please select the vendor")]
        public int SelectedVendorId { get; set; }

        [Display(Name = "PostingDate", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "PostingDateRequired")]
        //[Required(ErrorMessage = "Please select the posting date")]
        public Nullable<System.DateTime> POSTINGDATE { get; set; }

        [Display(Name = "EntryDate", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "EntryDateRequired")]
        //[Required(ErrorMessage = "Please select the posting date")]
        public Nullable<System.DateTime> ENTRYDATE { get; set; }

        //public Nullable<int> Entrytype { get; set; }
        //public int entryfor { get; set; }

        [Display(Name = "Particulars", ResourceType = typeof(Resource))]
        public string DESCRIPTION { get; set; }

        [Display(Name = "Quantity", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "QuantityRequired")]
        //[Required(ErrorMessage = "Please enter the quantity")]
        public Nullable<decimal> QUANTITY { get; set; }

        //public Nullable<decimal> RemainingQuantity { get; set; }

        [Display(Name = "InvoicedQuantity", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "InvoicedQuantityRequired")]
        //[Required(ErrorMessage = "Please enter the invoiced quantity")]
        public Nullable<decimal> INVOICEDQUANTITY { get; set; }

        //public Nullable<decimal> Adj { get; set; }
        //public Nullable<int> AdjType { get; set; }
        //public Nullable<int> TransactionType { get; set; }

        [Display(Name = "VAT", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "VatRequired")]
        //[Required(ErrorMessage = "Please enter the vat")]
        public Nullable<double> VAT { get; set; }

        public Nullable<int> PRICEINCLUDEVAT { get; set; }

        [Display(Name = "UnitPrice", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "UnitPriceRequired")]
        //[Required(ErrorMessage = "Please enter the unit price")]
        public string UNITPRICE { get; set; }

        [Display(Name = "ItemCategoryCode", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ItemCategoryCodeRequired")]
        //[Required(ErrorMessage = "Please enter the item category code")]
        public Nullable<int> ITEMCATEGORYCODE { get; set; }
        
        //public Nullable<System.Guid> ItemTrackingCode { get; set; }
        //public Nullable<int> open { get; set; }

        [Display(Name = "ExtraExpenses", ResourceType = typeof(Resource))]        
        public Nullable<decimal> EXTRAEXPENSES { get; set; }

        [Display(Name = "TotalExpenses", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "TotalExpensesRequired")]
        //[Required(ErrorMessage = "Please enter the total expenses")]
        public Nullable<decimal> TOTALEXPENSES { get; set; }
        
        //public Nullable<System.Guid> FK_VendorId { get; set; }

        [Display(Name = "Comments", ResourceType = typeof(Resource))]
        public string COMMENTS { get; set; }

        //public int entryby { get; set; }
        //public int entryUnit { get; set; }
        //public Nullable<System.DateTime> entrydate { get; set; }
        //public Nullable<System.DateTime> editdate { get; set; }
        //public Nullable<System.DateTime> lasteditdat { get; set; }
        //public Nullable<int> waived { get; set; }
        //public string waivedReason { get; set; }
    }

   
}