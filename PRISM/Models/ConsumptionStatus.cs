﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class ConsumptionStatus
    {
        public string status { get; set; }
        public string data { get; set; }
        public string uprice { get; set; }
        public string maxallqty { get; set; }
    }
}