﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PRISM
{
    [MetadataType(typeof(ConsumptionPeriodMetadata))]
    public partial class TBL_CONSUMPTION_PERIOD
    {
    }

    public class ConsumptionPeriodMetadata
    {
        public System.Guid PK_DATEID { get; set; }

        [Display(Name = "Item",ResourceType= typeof(Resource))]        
        [Required(ErrorMessageResourceName="ItemRequired", ErrorMessageResourceType=typeof(Resource))]
        public System.Guid FK_ITEMID { get; set; }

        [Display(Name = "Quantity",ResourceType= typeof(Resource))]
        [Required(ErrorMessageResourceName = "QuantityRequired", ErrorMessageResourceType = typeof(Resource))]
        public Nullable<int> ALLOWEDQTY { get; set; }

        [Display(Name = "Period",ResourceType= typeof(Resource))]
        [Required(ErrorMessageResourceName = "ConsumptionPeriodRequired", ErrorMessageResourceType = typeof(Resource))]
        public decimal CONSUMPTIONPERIOD { get; set; }

        [Display(Name = "Duration",ResourceType= typeof(Resource))]
        [Required(ErrorMessageResourceName = "DurationRequired", ErrorMessageResourceType = typeof(Resource))]
        public string CONSUMPTIONTIME { get; set; }

        public int ENTRYBY { get; set; }
        public System.DateTime ENTRYTIME { get; set; }
        public Nullable<System.DateTime> LASTEDITDATE { get; set; }
    }
}