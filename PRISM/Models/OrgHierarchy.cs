﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class OrgHierarchy
    {
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public List<OrgHierarchy> ChildNodes { get; set; }
    }

    public class UserRoles
    {
        public string  RoleName { get; set; }
        public int RoleId { get; set; }
    }
}