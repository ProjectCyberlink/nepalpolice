﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class spWeaponsCount
    {
        public string RegionName { get; set; }
        public string StateName { get; set; }
        public string WeaponCnt { get; set; }
    }
}