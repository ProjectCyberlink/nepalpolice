﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PRISM
{
    [MetadataType(typeof(MeasuringUnitsMetadata))]
    public partial class TBL_MEASURINGUNITS_MASTER
    {
    }

    public class MeasuringUnitsMetadata
    {
        public string PK_UOM_ID { get; set; }
        
        [Display(Name = "UnitName", ResourceType=typeof(Resource))]
        [Required(ErrorMessageResourceName="UnitNameRequired", ErrorMessageResourceType=typeof(Resource))]
        public string UNITNAME { get; set; }

        [Display(Name = "UnitShort", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "UnitShortRequired", ErrorMessageResourceType = typeof(Resource))]
        public string UNITSHORT { get; set; }

        [Display(Name = "Description", ResourceType = typeof(Resource))]
        public string DESCRIPTION { get; set; }
        
        public int ENTRYID { get; set; }

        public System.DateTime CREATEDDATE { get; set; }
    }
}