﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class AssetsQuantity
    {
        public string ItemName { get; set; }
        public int Quantity { get; set; }
        public int RemainingQuantity { get; set; }
        public int UnitPrice { get; set; }
        public string Vendor { get; set; }

    }
}