﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRISM
{
    [MetadataType(typeof(tbl_Master_Inv_Metadata))]
    public partial class TBL_MASTER_INV
    {
    }

    public class tbl_Master_Inv_Metadata
    {
        public string PK_MASTER_INV_ID { get; set; }
        //public int itemCategoryCode { get; set; }

        [Display(Name = "ItemName", ResourceType=typeof(Resource))]
        [Required(ErrorMessageResourceType=typeof(Resource),ErrorMessageResourceName="ItemRequired")]
        //[Required(ErrorMessage = "Please enter the item")]
        [Remote("DoesItemNameExists", "Weapons", AdditionalFields = "PK_MASTER_INV_ID", HttpMethod = "POST", ErrorMessageResourceName = "ItemAlreadyExist", ErrorMessageResourceType = typeof(Resource))]
        public string ITEMNAME { get; set; }

        //public string itemType { get; set; }

        [Display(Name = "Specification", ResourceType = typeof(Resource))]
        //[Required(ErrorMessage = "Please enter the specification")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "SpecificationRequired")]
        public string SPECIFICATION { get; set; }

        [Display(Name = "Unit", ResourceType = typeof(Resource))]
        //[Required(ErrorMessage = "Please select the unit")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "UOMRequired")]
        public System.Guid FK_UOM_ID { get; set; }

        [Display(Name = "Comments", ResourceType = typeof(Resource))]
        public string COMMENTS { get; set; }
    }
}