﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PRISM
{
    [MetadataType(typeof(VendorMetadata))]
    public partial class TBL_VENDOR
    {

    }

    public class VendorMetadata
    {
        public string  PK_VENDORID { get; set; }

        [Display(Name = "VendorName",ResourceType=typeof(Resource))]
        [Required(ErrorMessageResourceName="VendorRequired",ErrorMessageResourceType=typeof(Resource))]
        public string VENDORNAME { get; set; }

        [Display(Name = "Address", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "AddressRequired", ErrorMessageResourceType = typeof(Resource))]
        public string ADDRESS { get; set; }

        [Display(Name = "Address2", ResourceType = typeof(Resource))]
        public string ADDRESS2 { get; set; }

        [Display(Name = "ContactPerson", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "ContactRequired", ErrorMessageResourceType = typeof(Resource))]
        public string CONTACTPERSON { get; set; }

        [Display(Name = "Phone", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "PhoneRequired", ErrorMessageResourceType = typeof(Resource))]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$",ErrorMessageResourceName="PhoneValid",ErrorMessageResourceType=typeof(Resource))]
        public string PHONENO { get; set; }

        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessageResourceName = "PhoneValid", ErrorMessageResourceType = typeof(Resource))]
        [Display(Name="Phone1", ResourceType = typeof(Resource))]
        public string PHONENO1 { get; set; }

        [Display(Name = "Email", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "EmailRequired", ErrorMessageResourceType = typeof(Resource))]
        //[EmailAddress(ErrorMessageResourceName="EmailValid",ErrorMessageResourceType=typeof(Resource))]
        public string EMAIL { get; set; }

        [Display(Name = "Fax", ResourceType = typeof(Resource))]
        public string FAXNO { get; set; }

        [Display(Name = "Terretory", ResourceType = typeof(Resource))]
        public string TERRETORY { get; set; }

        [Display(Name = "Country", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "CountryRequired", ErrorMessageResourceType = typeof(Resource))]
        public string COUNTRY { get; set; }

        [Display(Name = "CurrencyCode", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "CurrencyRequired", ErrorMessageResourceType = typeof(Resource))]
        public string CURRENCYCODE { get; set; }

        public int BLOCKED { get; set; }

        [Display(Name = "VatRegNum", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "VatRegRequired", ErrorMessageResourceType = typeof(Resource))]
        public string VATREGNO{ get; set; }
    }
}