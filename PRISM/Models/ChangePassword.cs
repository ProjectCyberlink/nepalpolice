﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRISM.Models
{
    public class ChangePassword
    {
        [Display(Name = "OldPassword", ResourceType = typeof(Resource))]        
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "OldPasswordRequired")]
        [Remote("DoesOldPasswordMatch", "ChangePassword", HttpMethod = "POST", ErrorMessageResourceName = "OldPasswordMisMatch", ErrorMessageResourceType = typeof(Resource))]
        public string OldPassword { get; set; }

        [Display(Name = "NewPassword", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "NewPasswordRequired")]
        public string  NewPassword { get; set; }

        [Display(Name = "ConfirmPassword", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "ConfirmPasswordRequired")]
        public string ConfirmPassword { get; set; }
    }
}