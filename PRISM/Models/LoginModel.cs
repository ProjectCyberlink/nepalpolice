﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class LoginModel2
    {

        public string UserId { get; set; }
        public string Passwd { get; set; }
        public string Status { get; set; }
        public string Code { get; set; }
        public string Rank { get; set; }
        public string FullName { get; set; }
        public string UnitId { get; set; }
        public string UnitName { get; set; }
        public string UnitUpperId { get; set; }
        public string UserRoleId { get; set; }
        public string UserRole { get; set; }
        public string ActiveSince { get; set; }
        public string LastActivityOn { get; set; }

        public string Address { get; set; }
        public bool statusbool { get; set; }
        public decimal Usr { get; set; }
    }
}