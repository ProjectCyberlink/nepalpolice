﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PRISM
{
    [MetadataType(typeof(WeaponDistributionMetadata))]
    public partial class TBL_WEAPON_DISTRIBUTION
    {
        public System.Guid FK_MASTER_INV { get; set; }
        public string SerialNo { get; set; }
        //public Unit unit { get; set; }
        public string Weapon_Id { get; set; }
    }


    public class WeaponDistributionMetadata
    {
        [Display(Name = "SerialNum", ResourceType=typeof(Resource))]
        [Required(ErrorMessageResourceName="SerialNumRequired", ErrorMessageResourceType=typeof(Resource))]
        public string SerialNo { get; set; }

        [Display(Name = "ItemName", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "WeaponTypeRequired", ErrorMessageResourceType = typeof(Resource))]
        public string FK_MASTER_INV { get; set; }

        //[Display(Name = "Unit Name")]
        //[Required(ErrorMessage = "Please select the unit")]
        //public Unit unit { get; set; }

        public string PK_WDISTRIBUTION_ID { get; set; }

        [Display(Name = "WeaponName", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "WeaponNameRequired", ErrorMessageResourceType = typeof(Resource))]
        public string FK_WEAPON_DETAIL { get; set; }



        [Display(Name = "Unit", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "UnitRequired", ErrorMessageResourceType = typeof(Resource))]
        public int DISTRIBUTIONTO { get; set; }
        
        public Nullable<int> ASSIGNEDBYTO { get; set; }
        public int ENTRYTYPE { get; set; }


        [Display(Name = "DistributedDate", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "DistributedDateRequired", ErrorMessageResourceType = typeof(Resource))]
        public System.DateTime DISTRIBUTEDDATE { get; set; }
        
        public System.DateTime ENTRYDATE { get; set; }


        [Display(Name = "Comments", ResourceType = typeof(Resource))]
        //[Required(ErrorMessage = "Please enter the comments")]
        public string COMMENTS { get; set; }

        
        [Required(ErrorMessageResourceName = "UnitRequired", ErrorMessageResourceType = typeof(Resource))]
        public int DISTRIBUTIONBY { get; set; }

        //[Display(Name = "Distributed Date")]
        //[Required(ErrorMessage = "Please select the date")]
        //public int DistributionTo { get; set; }

        [Display(Name = "Status", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "StatusRequired", ErrorMessageResourceType = typeof(Resource))]
        public string STATUS { get; set; }
    }
}