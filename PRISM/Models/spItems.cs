﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class spItems
    {
        public int id { get; set; }
        public string bookpgid { get; set; }
        public string ITEMNAME { get; set; }
        public string SPECIFICATION { get; set; }
        public string classification { get; set; }
        public string unit { get; set; }
        public Nullable<double> onHand { get; set; }
        public Nullable<double> costPerUnit { get; set; }
        public Nullable<double> taxPU { get; set; }
        public Nullable<double> purchaseOrderNo { get; set; }
        public Nullable<System.DateTime> ENTRYDATE { get; set; }
        public Nullable<double> Total { get; set; }
    }
}