﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class WeaponDistributionDetail
    {
        public string weaponName { get; set; }
        public string serialNumber { get; set; }
        public string distributedBy { get; set; }
        public string distributedTo { get; set ; }
        public string status { get; set; }
        public Nullable<System.DateTime> DISTRIBUTEDDATE { get; set; }

        public string RegionName { get; set; }
        public string StateName { get; set; }
        public string WeaponCnt { get; set; }
    }
}