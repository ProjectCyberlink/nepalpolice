﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class spVendor
    {
        public string PK_VendorId { get; set; }

        [Display(Name = "Vendor Name")]
        [Required(ErrorMessage = "Please select the serial number")]
        public string VendorName { get; set; }
        
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string ContactPerson { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneNo1 { get; set; }
        public string Email { get; set; }
        public string FaxNo { get; set; }
        public string Terretory { get; set; }
        public string Country { get; set; }
        public string CurrencyCod { get; set; }
        public string VatRegistrationNo { get; set; }
        public string isActice { get; set; }
    }
}