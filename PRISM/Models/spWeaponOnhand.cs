﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class spWeaponOnhand
    {
        public string whi { get; set; }
        public string whguid { get; set; }
        public string whname { get; set; }
        public string whtype { get; set; }
        public string whSPECIFICATION { get; set; }
        public string UNITSHORT { get; set; }
        public string whpurchasedate { get; set; }
        public decimal qty { get; set; }
        public string whENTRYDATE { get; set; }
        public string whENTRYBY { get; set; }
        public string whCOMMENTS { get; set; }

        public string whSerial { get; set; }
    }
}