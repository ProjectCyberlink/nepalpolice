﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PRISM
{
    public partial class TBL_WEAPON_PURCHASE_INFO
    {
    }

    public class WeaponPurchaseInfoMetadata
    {
        public string PK_PURCHASE_ID { get; set; }
        public string FK_WEAPONDETAIL { get; set; }
        public string PURCHASEDITEMTYPE { get; set; }
        public int PURCHASEBY_FK_PERSONCODE { get; set; }
        public int ENTRYUNIT { get; set; }

        [Display(Name = "Quantity", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "QuantityRequired", ErrorMessageResourceType = typeof(Resource))]       
        public int QUANTITY { get; set; }

        [Display(Name = "UnitPrice", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "UnitPriceRequired", ErrorMessageResourceType = typeof(Resource))]
        public decimal UNITPRICE { get; set; }

        [Display(Name = "VAT", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceName = "VatRequired", ErrorMessageResourceType = typeof(Resource))]
        public Nullable<decimal> VAT { get; set; }

        [Display(Name = "Extra Expenses", ResourceType=typeof(Resource))]
        [Required(ErrorMessageResourceName = "ExtraExpenses", ErrorMessageResourceType = typeof(Resource))]
        public decimal EXTRAEXPENSES { get; set; }

        [Display(Name = "PurchasedDate",ResourceType=typeof(Resource))]
        [Required(ErrorMessageResourceName = "PurchasedDate", ErrorMessageResourceType = typeof(Resource))]
        public System.DateTime PURCHASEDDATE { get; set; }

        [Display(Name = "Vendor", ResourceType=typeof(Resource))]
        [Required(ErrorMessageResourceName="Vendor",ErrorMessageResourceType=typeof(Resource))]
        public Nullable<Guid> FK_VENDORID { get; set; }

        public System.DateTime ENTRYDATE { get; set; }
        public string UNIXDATETIME { get; set; }

        public virtual PERSONDETAIL PERSONNEL { get; set; }
        public virtual TBL_VENDOR TBL_VENDOR { get; set; }
        public virtual TBL_WEAPON_DETAIL TBL_WEAPON_DETAIL { get; set; }
    }
}