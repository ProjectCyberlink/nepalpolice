﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Models
{
    public class spWeaponsSearch
    {
        public string WDistributionId { get; set; }
        public string Weapon_detailId { get; set; }
        public string Weapons_Id { get; set; }
        public string Weapon_Name { get; set; }
        public string Serial_No { get; set; }
        public string DistributionBy { get; set; }
        public string Name { get; set; }
        public string Regions { get; set; }
        public string regionGuid { get; set; }
        public string State { get; set; }
        public string stateGuid { get; set; }
        public string DistributionTo { get; set; }
        public string AssignedByTo { get; set; }
        public string EntryType { get; set; }
        public string Status { get; set; }
        public string DistributedDate { get; set; }
        public string EntryDate { get; set; }
        public string UnixDate { get; set; }
        public string Comments { get; set; }


    }
}