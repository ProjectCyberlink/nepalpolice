﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM
{
    public class RoleRightHelper
    {
        public static bool HasUserRight(string RightName)
        {
            if (HttpContext.Current.Session["UserRights"] != null)
            {
                List<string> roleRights = HttpContext.Current.Session["UserRights"] as List<string>;
                if (roleRights.Exists(right => right.Equals(RightName)))
                    return true;
                else
                    return false;
            }
            return false;
        }
    }

    public enum RightNames
    {
        ViewDashboard,
        AddAssetMaster,
        AddAssetItem,
        ViewReports,
        HandoverAssetToUnit,
        HandoverAssetToPerson,
        UploadCsvFIle,
        AddWeaponMaster,
        AddWeapon,
        HandoverWeaponToUnit,
        HandoverWeaponToPerson,
        ReturnWeaponToUnit,
        ReturnWeaponToPerson,
        AddUnitOfMeasureSettings,
        AddVendorSettings,
        AddConsumptionPeriodSettings,
        ChangePassword
    }
}