//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PRISM
{
    using System;
    using System.Collections.Generic;
    
    public partial class TBL_WEAPON_PURCHASE_INFO
    {
        public string PK_PURCHASE_ID { get; set; }
        public string FK_WEAPONDETAIL { get; set; }
        public string PURCHASEDITEMTYPE { get; set; }
        public Nullable<int> PURCHASEDBY_FK_PERSONCODE { get; set; }
        public string ENTRYUNIT { get; set; }
        public Nullable<decimal> QUANTITY { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<decimal> EXTRAEXPENSES { get; set; }
        public Nullable<System.DateTime> PURCHASEDDATE { get; set; }
        public string FK_VENDORID { get; set; }
        public Nullable<System.DateTime> ENTRYDATE { get; set; }
        public string UNIXDATETIME { get; set; }
        public Nullable<decimal> UNITPRICE { get; set; }
    }
}
