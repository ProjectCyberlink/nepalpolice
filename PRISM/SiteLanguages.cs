﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace PRISM
{
    public class SiteLanguages
    {
        public static List<Languages> AvailableLanguages = new List<Languages>
        {
            new Languages{LangFullName="English", LangCulturename="en-US"},
            new Languages{LangFullName="Nepali", LangCulturename="ne-NP"}
        };

        public static bool IsLanguageAvailable(string lang)
        {
            var language = AvailableLanguages.Where(l => l.LangCulturename==lang);
            return AvailableLanguages.Where(l => l.LangCulturename==lang).FirstOrDefault() != null ? true : false;
        }
        public static string GetDefaultLanguage()
        {
            return AvailableLanguages[0].LangCulturename;
        }

        public void SetLanguage(string lang)
        {
            try
            {
                if (!IsLanguageAvailable(lang))
                    lang = GetDefaultLanguage();
                var cultureInfo = new CultureInfo(lang);
                Thread.CurrentThread.CurrentUICulture = cultureInfo;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureInfo.Name);
                HttpCookie langCookie = new HttpCookie("Culture", lang);
                langCookie.Expires = DateTime.Now.AddYears(1);
                HttpContext.Current.Response.Cookies.Add(langCookie);
            }
            catch(Exception ex)
            {
            }
        }
    }




    public class Languages
    {
        public string LangFullName { get; set; }
        public string LangCulturename { get; set; }
    }
}