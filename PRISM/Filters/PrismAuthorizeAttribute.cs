﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace PRISM.Filters
{
    public class PrismAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var _auth = base.AuthorizeCore(httpContext);
            if (!_auth)
            {
                return false;
                
            }
            var myvar = httpContext.Session["Person"];
            if (myvar == null)
            {
                 return false;
            }
            return true;
            
            
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (HttpContext.Current.Session["Person"] == null)
            {
                filterContext.Result = new RedirectResult("Account/logOfff", true);
                /*filterContext.Result = new RedirectResult("Account/Login", true);*/
            }
        }
    }
}