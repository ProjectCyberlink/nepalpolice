﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace PRISM.Filters
{
    public class PrismRoleProvider : RoleProvider
    {
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }
        public override string[] GetRolesForUser(string username)
        {
            var ubi = username;
            ////throw new NotImplementedException();
            //string[]a={"admin"};
            ////return string[]{"admin"};
            //return a;
            using (NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities()) {
               
                var objUser = db.VIEW_AUTH.FirstOrDefault(x => x.PERSON_CODE == ubi);
                if (objUser == null)
                {
                    return null;
                }
                else
                {
                    string[] ret = db.VIEW_AUTH.Where(xi=> xi.PERSON_CODE == objUser.PERSON_CODE).Select(x => x.ROLENAME).ToArray();
                    return ret;
                }
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}