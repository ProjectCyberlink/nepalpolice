﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;

namespace PRISM.Filters
{
    public class PrismBaseHelper
    {
        private HttpContextBase Context { get; set; }
        public string GetLoggedInUsrId() { return Context.Session["person"].ToString();  }
        public string GetLoggedInUsrUnitId(){return Context.Session["UnitCode"].ToString();}
        public string GetLoggedInUsrUnitMaster(){return Context.Session["unitmaster"].ToString();}

        public static List<SelectListItem> ItemCatgoryCodeSelectList = new List<SelectListItem>{
                                                                   new SelectListItem{Text="47", Value="47"},
                                                                   new SelectListItem{Text="52", Value="52"}
                                                                   };
    }
}