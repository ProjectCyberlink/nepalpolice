﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRISM.Filters
{
    public static class ConsumptionRule
    {
        public static DateTime GetConsumptionDate(DateTime fromDate, int ConsumptionPeriod, string consumptionTime)
        { 
                DateTime toDate;
                switch (consumptionTime)
                {
                    case "Year":
                        toDate = fromDate.AddYears(ConsumptionPeriod);
                        break;
                    case "Month":
                        toDate = fromDate.AddMonths(ConsumptionPeriod);
                        break;
                    case "Day":
                        toDate = fromDate.AddDays(ConsumptionPeriod);
                        break;
                    default:
                        toDate = DateTime.Now;
                        break;
                }
                return toDate;
        }

        public static DateTime GetConsumptionStartDate(int ConsumptionPeriod, string consumptionTime)
        {
            DateTime startDate;
            switch (consumptionTime)
            {
                case "Year":
                    startDate = DateTime.Now.AddYears(-ConsumptionPeriod);
                    break;
                case "Month":
                    startDate = DateTime.Now.AddMonths(-ConsumptionPeriod);
                    break;
                case "Day":
                    startDate = DateTime.Now.AddDays(-ConsumptionPeriod);
                    break;
                default:
                    startDate = DateTime.Now;
                    break;
            }
            return startDate;
        }
    }
}