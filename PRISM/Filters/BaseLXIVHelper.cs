﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PRISM.Filters
{
    public class BaseLXIVHelper
    {
        public static string base64Decode(string data)
        {
            try
            {

                System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

                System.Text.Decoder utf8Decode = encoder.GetDecoder();

                byte[] byte_data = Convert.FromBase64String(data);

                int charCount = utf8Decode.GetCharCount(byte_data, 0, byte_data.Length);

                char[] decoded_char = new char[charCount];

                utf8Decode.GetChars(byte_data, 0, byte_data.Length, decoded_char, 0);

                string result = new String(decoded_char);

                return result;

            }
            catch (Exception e)
            {

                throw new Exception(e.Message);

            }

        }

        public static string base64Encode(string data)
        {
            try
            {
                byte[] byt_dat = new byte[data.Length];
                byt_dat = System.Text.Encoding.UTF8.GetBytes(data);
                string encodedData = Convert.ToBase64String(byt_dat);
                //Console.WriteLine(encodedData);
                //Console.ReadLine();
                return encodedData;
            }
            catch (Exception e)
            {

                return new Exception(e.Message).ToString();
            }
        }
        public static string datediff(string d,string span,decimal period)
        {
            DateTime dt = Convert.ToDateTime(d);
            DateTime dt1 = DateTime.Now;
            double _p = double.Parse(period.ToString());
            double nowDate = DateTime.Now.Subtract(dt).TotalDays;
            string returnstring ="NaN";
            //DateTime realdate = new DateTime(dt.Year, dt.Day, dt.Month).AddDays(nowDate);
            switch (span)
            {
                case "Year":
                    var years = (nowDate / 365);
                    if (years >= _p)
                    {
                        returnstring = "Ok";
                    }
                    
                    break;

                case "Month":
                    var months = ((nowDate % 365) / 30);
                    if (months >= _p)
                    {
                        returnstring = "Ok";
                    }
                    break;
                case "Days":
                    var days = ((nowDate % 365) % 30);
                    if (days >= _p)
                    {
                        returnstring = "Ok";
                    }
                    break;
                default :
                    returnstring = "NaN";
                    break;
            }

            return returnstring;
                
        }

        public static DateTime GetDateBeforeSpanPeriod(string span, int period)
        {
            switch (span)
            {
                case "Year":
                    return DateTime.Now.AddYears(-period);

                case "Month":
                    return DateTime.Now.AddMonths(-period);

                case "Days":
                    return DateTime.Now.AddDays(-period);

                default:
                    return DateTime.Now;
            }
        }

        static public string GetMd5Sum(string str)
        {
            Encoder enc = System.Text.Encoding.Unicode.GetEncoder();

            byte[] unicodeText = new byte[str.Length * 2];
            enc.GetBytes(str.ToCharArray(), 0, str.Length, unicodeText, 0, true);

            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(unicodeText);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                sb.Append(result[i].ToString("X2"));
            }

            return sb.ToString();
        }


    }

}