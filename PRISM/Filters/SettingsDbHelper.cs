﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRISM.Filters
{
    public class SettingsDbHelper
    {
        public NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
        public List<Models.spConsumption> GetConsumptionPeriodDetail() {
            List<Models.spConsumption> lst = new List<Models.spConsumption>();
            var query = (
            from a in db.TBL_CONSUMPTION_PERIOD
            join b in db.TBL_ASSET on new { FK_ItemId = a.FK_ITEMID } equals new { FK_ItemId = b.PK_ASSET_ID}
            select new
            {
                a.PK_DATEID,
                b.ITEMNAME,
                a.ALLOWEDQTY,
                a.CONSUMPTIONPERIOD,
                a.CONSUMPTIONTIME,
                b.SPECIFICATION,
                b.ITEM_DESC
            }).AsEnumerable().Select(x => new Models.spConsumption { 
            Dateid =x.PK_DATEID,
            ITEMNAME = x.ITEMNAME,
            allowedQty = x.ALLOWEDQTY.ToString(),
            consumpPeriod = x.CONSUMPTIONPERIOD,
            ConsumpTime = x.CONSUMPTIONTIME,
            Specification = x.SPECIFICATION,
            itemDesc = x.ITEM_DESC
            }).ToList();
            lst = query;
            return lst;
        }

        public List<Models.spVendor> getVendorDetail() {
            List<Models.spVendor> lst = new List<Models.spVendor>();
            var query = (from a in db.TBL_VENDOR
                         select new
                         {
                             a.PK_VENDORID,
                             a.VENDORNAME,
                             a.ADDRESS,
                             a.CONTACTPERSON,
                             a.PHONENO,
                             a.EMAIL,
                             a.TERRETORY,
                             a.COUNTRY,
                             a.VATREGNO
                         }).AsEnumerable().Select(
                         x=>new Models.spVendor{
                         PK_VendorId = x.PK_VENDORID,
                         VendorName = x.VENDORNAME,
                         Address = x.ADDRESS,
                         ContactPerson = x.CONTACTPERSON,
                         PhoneNo = x.PHONENO,
                         Email = x.EMAIL,
                         Terretory = x.TERRETORY,
                         Country = x.COUNTRY,
                         VatRegistrationNo = x.VATREGNO
                         }).ToList();
            lst = query;
            return lst;
        }

        public List<Models.spMeasurinUnit> getMeasuringUnitDetail() {
            List<Models.spMeasurinUnit> lst = new List<Models.spMeasurinUnit>();
            var query = (from a in db.TBL_MEASURINGUNITS_MASTER
                         select new
                         {
                             a.PK_UOM_ID,
                             a.UNITNAME,
                             a.UNITSHORT,
                             a.DESCRIPTION,
                             a.ENTRYID,
                             a.CREATEDDATE
                         }).AsEnumerable().Select(
                         x => new Models.spMeasurinUnit { 
                         pk_UoM = x.PK_UOM_ID,
                         unitNmae = x.UNITNAME,
                         unitShort = x.UNITSHORT,
                         description = x.DESCRIPTION,
                         entryid = x.ENTRYID.ToString(),
                         entrydate = x.CREATEDDATE                         
                         }
                         ).ToList();
            lst = query;
            return lst;
        }
    }
}