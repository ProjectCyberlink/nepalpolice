﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRISM.Filters
{
    public class DbHelper
    {
        private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();

       
        public Models.spAssetsDetails GetItemSpecificDetail(string id, string unitcode, string unitMaste)
        {

            Models.spAssetsDetails spd = new Models.spAssetsDetails();
            string _entryFor = "", _transType = "";
            var condition = (from asl in db.TBL_ASSETLEDGER where asl.PK_ALE_ID == id select new { asl.ENTRYTYPE, asl.TRANSACTIONTYPE }).ToList();
            foreach (var item in condition)
            {
                _entryFor = item.ENTRYTYPE.ToString();
                _transType = item.TRANSACTIONTYPE.ToString();
            }
            if (unitcode == "PHQ")
            {
                /*For PHQ*/
                if (_entryFor == "0" && _transType == "0")
                {
                    var query = (from a in db.TBL_ASSETLEDGER
                                 join b in db.TBL_ASSET on new { itemid = a.FK_ASSET_ID } equals new { itemid = b.PK_ASSET_ID }
                                 join c in db.TBL_MEASURINGUNITS_MASTER on new { UOM = b.FK_UOM_ID } equals new { UOM = c.PK_UOM_ID }
                                 join d in
                                     (
                                         ((from Auths in db.VIEW_AUTH
                                           select new
                                           {
                                               Auths.ID,
                                               Auths.UNIT_NAME
                                           }).Distinct())) on new { entryfor = a.ENTRYFOR } equals new { entryfor = d.ID }
                                 join e in
                                     (
                                         (from Auths in db.VIEW_AUTH
                                          select new
                                          {
                                              Auths.PERSON_CODE,
                                              Auths.NAME,
                                              Auths.UNIT_NAME,
                                              Auths.ADDRESS
                                          })) on new { entryby = a.ENTRYBY } equals new { entryby = e.PERSON_CODE }
                                 where
                                   a.PK_ALE_ID == id
                                 select new
                                 {
                                     a.PK_ALE_ID,
                                     itemid = a.FK_ASSET_ID,
                                     a.POSTINGDATE,
                                     a.ENTRYTYPE,
                                     a.TRANSACTIONTYPE,
                                     a.DESCRIPTION,
                                     a.QUANTITY,
                                     a.INVOICEDQUANTITY,
                                     a.VAT,
                                     a.PRICEINCLUDEVAT,
                                     a.UNITPRICE,
                                     a.ITEMTRACKINGCODE,
                                     a.ITEMCATEGORYCODE,
                                     a.EXTRAEXPENSES,
                                     a.TOTALEXPENSES,
                                     a.COMMENTS,
                                     b.BOOKPAGEID,
                                     b.ITEMNAME,
                                     b.SPECIFICATION,
                                     b.ITEM_DESC,
                                     c.UNITSHORT,
                                     a.WAIVED,
                                     a.WAIVEDREASON,
                                     d.UNIT_NAME,
                                     e.NAME,
                                     Column1 = e.UNIT_NAME,
                                     e.ADDRESS
                                     
                                 }).AsEnumerable().Select

                                    (x => new PRISM.Models.spAssetsDetails
                                    {
                                         ALE = x.PK_ALE_ID,
                                        itemid = x.itemid,
                                         postingdate = x.POSTINGDATE.HasValue ? x.POSTINGDATE.Value.ToString("yyyy-MM-dd") : "",
                                        Description = x.DESCRIPTION,
                                        Quantity = x.QUANTITY.ToString(),
                                        InvoicedQuantity = x.INVOICEDQUANTITY.ToString(),
                                        VAT = x.VAT.ToString(),
                                        PriceIncludeVAT = (x.PRICEINCLUDEVAT == 0 ? "NO" : "YES"),
                                        UnitPrice = x.UNITPRICE.ToString(),
                                        ItemCategoryCode = (x.ITEMCATEGORYCODE == 52 ? "less than year" : "more than a year"),
                                        ItemTrackingCode = (x.ITEMTRACKINGCODE == null ? " " : (x.ITEMTRACKINGCODE.ToString())),
                                        ExtraExpenses = x.EXTRAEXPENSES.ToString(),
                                        TotalExpenses = x.TOTALEXPENSES,
                                        comments = (x.COMMENTS == null ? " " : (x.COMMENTS)),
                                        bookpageid = x.BOOKPAGEID.ToString(),
                                        ItemName = x.ITEMNAME,
                                        Specification = x.SPECIFICATION,
                                        item_Desc = x.ITEM_DESC,
                                        unitShort = x.UNITSHORT,
                                        PERSON_CODE = x.NAME,
                                        waived = x.WAIVED.ToString(),
                                        waivedReason= (x.WAIVEDREASON == null ? " " : (x.COMMENTS)),
                                        UNIT_NAME = x.UNIT_NAME,
                                        entryfrom = x.Column1,

                                       Entrytype = (x.ENTRYTYPE == null ? " " : ((x.ENTRYTYPE == 0 && x.TRANSACTIONTYPE == 1) ? "Handed From PHQ" : "Handed From Unit"))
                                       
                                    }).ToList();
                    spd = query.SingleOrDefault();
                }
                else if (_entryFor == "1" && _transType == "1")
                {
                    var query = (
                    from a in db.TBL_ASSETLEDGER
                    join b in db.TBL_ASSET on new { itemid = a.FK_ASSET_ID } equals new { itemid = b.PK_ASSET_ID }
                    join c in db.TBL_MEASURINGUNITS_MASTER on new { UOM = b.FK_UOM_ID } equals new { UOM = c.PK_UOM_ID }
                    join d in
                        (
                            (from Auths in db.VIEW_AUTH
                             select new
                             {
                                 Auths.PERSON_CODE,
                                 Auths.NAME
                             })) on new { entryfor = a.ENTRYFOR } equals new { entryfor = d.PERSON_CODE }
                    join e in
                        (
                            (from Auths in db.VIEW_AUTH
                             select new
                             {
                                 Auths.PERSON_CODE,
                                 Auths.NAME,
                                 Auths.UNIT_NAME,
                                 Auths.ADDRESS
                             })) on new { entryby = a.ENTRYBY } equals new { entryby = e.PERSON_CODE }
                    where
                      a.PK_ALE_ID == id
                    select new
                    {
                        a.PK_ALE_ID,
                        itemid = a.FK_ASSET_ID,
                        a.POSTINGDATE,
                        a.ENTRYTYPE,
                        a.TRANSACTIONTYPE,
                        a.DESCRIPTION,
                        a.QUANTITY,
                        a.INVOICEDQUANTITY,
                        a.VAT,
                        a.PRICEINCLUDEVAT,
                        a.UNITPRICE,
                        a.ITEMTRACKINGCODE,
                        a.ITEMCATEGORYCODE,
                        a.EXTRAEXPENSES,
                        a.TOTALEXPENSES,
                        a.COMMENTS,
                        b.BOOKPAGEID,
                        b.ITEMNAME,
                        b.SPECIFICATION,
                        b.ITEM_DESC,
                        c.UNITSHORT,
                        a.WAIVED,
                        a.WAIVEDREASON,
                        Entryfor = d.PERSON_CODE,
                        column1 = d.NAME,
                        e.NAME,
                        e.UNIT_NAME,
                        e.ADDRESS
                    }).AsEnumerable().Select

                                    (x => new PRISM.Models.spAssetsDetails
                                    { ALE = x.PK_ALE_ID.ToString(),
                                        itemid = x.itemid.ToString(),
                                      postingdate = x.POSTINGDATE.HasValue ? x.POSTINGDATE.Value.ToString("yyyy-MM-dd") : "",
                                        Description = x.DESCRIPTION,
                                        Quantity = x.QUANTITY.ToString(),
                                        InvoicedQuantity = x.INVOICEDQUANTITY.ToString(),
                                        VAT = x.VAT.ToString(),
                                        PriceIncludeVAT = (x.PRICEINCLUDEVAT == 0 ? "NO" : "YES"),
                                        UnitPrice = x.UNITPRICE.ToString(),
                                        ItemCategoryCode = (x.ITEMCATEGORYCODE == 52 ? "less than year" : "more than a year"),
                                        ItemTrackingCode = (x.ITEMTRACKINGCODE == null ? " " : (x.ITEMTRACKINGCODE.ToString())),
                                        ExtraExpenses = x.EXTRAEXPENSES.ToString(),
                                        TotalExpenses = x.TOTALEXPENSES.ToString(),
                                        comments = (x.COMMENTS == null ? " " : (x.COMMENTS)),
                                        bookpageid = x.BOOKPAGEID.ToString(),
                                        ItemName = x.ITEMNAME,
                                        Specification = x.SPECIFICATION,
                                        item_Desc = x.ITEM_DESC,
                                        unitShort = x.UNITSHORT,
                                        PERSON_CODE = x.NAME,
                                        waived = x.WAIVED.ToString(),
                                        waivedReason= (x.WAIVEDREASON == null ? " " : (x.COMMENTS)),
                                        UNIT_NAME = x.UNIT_NAME,
                                        entryfrom = x.ADDRESS,
                                        Entrytype= (x.ENTRYTYPE == null ? " " : ((x.ENTRYTYPE == 0 && x.TRANSACTIONTYPE == 1) ? "Handed From PHQ" : "Handed From Unit")),
                                        
                                    }).ToList();
                    spd = query.SingleOrDefault();
                }
                else
                {
                    var query = (from a in db.TBL_ASSETLEDGER
                                 join b in db.TBL_ASSET on new { itemid = a.FK_ASSET_ID} equals new { itemid = b.PK_ASSET_ID}
                                 join c in db.TBL_MEASURINGUNITS_MASTER on new { UOM = b.FK_UOM_ID } equals new { UOM = c.PK_UOM_ID }
                                 join d in
                                     (
                                         ((from Auths in db.VIEW_AUTH
                                           select new
                                           {
                                               Auths.ID,
                                               Auths.UNIT_NAME
                                           }).Distinct())) on new { entryfor = a.ENTRYFOR } equals new { entryfor = d.ID }
                                 join e in
                                     (
                                         (from Auths in db.VIEW_AUTH
                                          select new
                                          {
                                              Auths.PERSON_CODE,
                                              Auths.NAME,
                                              Auths.UNIT_NAME,
                                              Auths.ADDRESS
                                          })) on new { entryby = a.ENTRYBY } equals new { entryby = e.PERSON_CODE }
                                 where
                                   a.PK_ALE_ID == id
                                 select new
                                 {
                                     a.PK_ALE_ID,
                                     itemid = a.FK_ASSET_ID,
                                     a.POSTINGDATE,
                                     a.ENTRYTYPE,
                                     a.TRANSACTIONTYPE,
                                     a.DESCRIPTION,
                                     a.QUANTITY,
                                     a.INVOICEDQUANTITY,
                                     a.VAT,
                                     a.PRICEINCLUDEVAT,
                                     a.UNITPRICE,
                                     a.ITEMTRACKINGCODE,
                                     a.ITEMCATEGORYCODE,
                                     a.EXTRAEXPENSES ,
                                     a.TOTALEXPENSES,
                                     a.COMMENTS,
                                     b.BOOKPAGEID,
                                     b.ITEMNAME,
                                     b.SPECIFICATION,
                                     b.ITEM_DESC,
                                     c.UNITSHORT,
                                     a.WAIVED,
                                     a.WAIVEDREASON,
                                     d.UNIT_NAME,
                                     e.NAME,
                                     Column1 = e.UNIT_NAME,
                                     e.ADDRESS
                                 }).AsEnumerable().Select

                                    (x => new PRISM.Models.spAssetsDetails
                                    {
                                        ALE = x.PK_ALE_ID.ToString(),
                                        itemid = x.itemid.ToString(),
                                        postingdate = x.POSTINGDATE.HasValue ? x.POSTINGDATE.Value.ToString("yyyy-MM-dd") : "",
                                        Entrytype = (x.ENTRYTYPE == null ? " " : ((x.ENTRYTYPE == 0 && x.TRANSACTIONTYPE == 1) ? "Handed From PHQ" : "Handed From Unit")),
                                        Description = x.DESCRIPTION,
                                        Quantity = x.QUANTITY.ToString(),
                                        InvoicedQuantity = x.INVOICEDQUANTITY.ToString(),
                                        VAT = x.VAT.ToString(),
                                        PriceIncludeVAT = (x.PRICEINCLUDEVAT == 0 ? "NO" : "YES"),
                                        UnitPrice = x.UNITPRICE.ToString(),
                                        ItemCategoryCode = (x.ITEMCATEGORYCODE == 52 ? "less than year" : "more than a year"),
                                        ItemTrackingCode = (x.ITEMTRACKINGCODE == null ? " " : (x.ITEMTRACKINGCODE.ToString())),
                                        ExtraExpenses = x.EXTRAEXPENSES.ToString(),
                                        TotalExpenses = x.TOTALEXPENSES.ToString(),
                                        comments = (x.COMMENTS == null ? " " : (x.COMMENTS)),
                                        bookpageid = x.BOOKPAGEID.ToString(),
                                        ItemName = x.ITEMNAME,
                                        Specification = x.SPECIFICATION,
                                        item_Desc = x.ITEM_DESC,
                                        unitShort = x.UNITSHORT,
                                        PERSON_CODE = x.NAME,
                                        waived = x.WAIVED.ToString(),
                                        waivedReason= (x.WAIVEDREASON == null ? " " : (x.COMMENTS)),
                                        UNIT_NAME = x.UNIT_NAME,
                                        entryfrom = x.Column1
                                    }).ToList();
                    spd = query.SingleOrDefault();
                }
            }
            else
            {/*For unit*/
                if (_entryFor == "0" && _transType == "1")
                {
                    var query = (from a in db.TBL_ASSETLEDGER
                                 join b in db.TBL_ASSET on new { itemid = a.FK_ASSET_ID } equals new { itemid = b.PK_ASSET_ID }
                                 join c in db.TBL_MEASURINGUNITS_MASTER on new { UOM = b.FK_UOM_ID } equals new { UOM = c.PK_UOM_ID }
                                 join d in
                                     (
                                         ((from Auths in db.VIEW_AUTH
                                           select new
                                           {
                                               Auths.ID,
                                               Auths.UNIT_NAME
                                           }).Distinct())) on new { entryfor = a.ENTRYFOR } equals new { entryfor = d.ID }
                                 join e in
                                     (
                                         (from Auths in db.VIEW_AUTH
                                          select new
                                          {
                                              Auths.PERSON_CODE,
                                              Auths.NAME,
                                              Auths.UNIT_NAME,
                                              Auths.ADDRESS
                                          })) on new { entryby = a.ENTRYBY } equals new { entryby = e.PERSON_CODE }
                                 where
                                   a.PK_ALE_ID == id
                                 select new
                                 {
                                     a.PK_ALE_ID,
                                     itemid = a.FK_ASSET_ID,
                                     a.POSTINGDATE,
                                     a.ENTRYTYPE,
                                     a.TRANSACTIONTYPE,
                                     a.DESCRIPTION,
                                     quantity = (Double?)Math.Abs((double)a.QUANTITY),
                                     a.INVOICEDQUANTITY,
                                     a.VAT,
                                     a.PRICEINCLUDEVAT,
                                     a.UNITPRICE,
                                     a.ITEMTRACKINGCODE,
                                     a.ITEMCATEGORYCODE,
                                     a.EXTRAEXPENSES,
                                     a.TOTALEXPENSES,
                                     a.COMMENTS,
                                     b.BOOKPAGEID,
                                     b.ITEMNAME,
                                     b.SPECIFICATION,
                                     b.ITEM_DESC,
                                     c.UNITSHORT,
                                     a.WAIVED,
                                     a.WAIVEDREASON,
                                     d.UNIT_NAME,
                                     e.NAME,
                                     Column1 = e.UNIT_NAME,
                                     e.ADDRESS
                                 }).AsEnumerable().Select

                                       (x => new PRISM.Models.spAssetsDetails
                                       {
                                            ALE = x.PK_ALE_ID.ToString(),
                                        itemid = x.itemid.ToString(),
                                            postingdate = x.POSTINGDATE.HasValue ? x.POSTINGDATE.Value.ToString("yyyy-MM-dd") : "",
                                        Description = x.DESCRIPTION,
                                        Quantity = x.quantity.ToString(),
                                        InvoicedQuantity = x.INVOICEDQUANTITY.ToString(),
                                        VAT = x.VAT.ToString(),
                                        PriceIncludeVAT = (x.PRICEINCLUDEVAT == 0 ? "NO" : "YES"),
                                        UnitPrice = x.UNITPRICE.ToString(),
                                        ItemCategoryCode = (x.ITEMCATEGORYCODE == 52 ? "less than year" : "more than a year"),
                                        ItemTrackingCode = (x.ITEMTRACKINGCODE == null ? " " : (x.ITEMTRACKINGCODE.ToString())),
                                        ExtraExpenses = x.EXTRAEXPENSES.ToString(),
                                        TotalExpenses = x.TOTALEXPENSES.ToString(),
                                        comments = (x.COMMENTS == null ? " " : (x.COMMENTS)),
                                        bookpageid = x.BOOKPAGEID.ToString(),
                                        ItemName = x.ITEMNAME,
                                        Specification = x.SPECIFICATION,
                                        item_Desc = x.ITEM_DESC,
                                        unitShort = x.UNITSHORT,
                                        PERSON_CODE = x.NAME,
                                        waived = x.WAIVED.ToString(),
                                        waivedReason= (x.WAIVEDREASON == null ? " " : (x.COMMENTS)),
                                        UNIT_NAME = x.UNIT_NAME,
                                        entryfrom = x.Column1,
                                        Entrytype= (x.ENTRYTYPE == null ? " " : ((x.ENTRYTYPE == 0 && x.TRANSACTIONTYPE == 1) ? "Handed From PHQ" : "Handed From Unit")),
                                       }).ToList();
                    spd = query.SingleOrDefault();
                }
                else if (_entryFor == "1" && _transType == "0")
                {
                    var query = (from a in db.TBL_ASSETLEDGER
                                 join b in db.TBL_ASSET on new { itemid = a.FK_ASSET_ID } equals new { itemid = b.PK_ASSET_ID }
                                 join c in db.TBL_MEASURINGUNITS_MASTER on new { UOM = b.FK_UOM_ID } equals new { UOM = c.PK_UOM_ID }
                                 join d in
                                     (
                                         ((from Auths in db.VIEW_AUTH
                                           select new
                                           {
                                               Auths.ID,
                                               Auths.UNIT_NAME
                                           }).Distinct())) on new { entryfor = a.ENTRYFOR } equals new { entryfor = d.ID }
                                 join e in
                                     (
                                         (from Auths in db.VIEW_AUTH
                                          select new
                                          {
                                              Auths.PERSON_CODE,
                                              Auths.NAME,
                                              Auths.UNIT_NAME,
                                              Auths.ADDRESS
                                          })) on new { entryby = a.ENTRYBY } equals new { entryby = e.PERSON_CODE}
                                 where
                                   a.PK_ALE_ID == id
                                 select new
                                 {
                                     a.PK_ALE_ID,
                                     itemid = a.FK_ASSET_ID,
                                     a.POSTINGDATE,
                                     a.ENTRYTYPE,
                                     a.TRANSACTIONTYPE,
                                     a.DESCRIPTION,
                                     a.QUANTITY,
                                     a.INVOICEDQUANTITY,
                                     a.VAT,
                                     a.PRICEINCLUDEVAT,
                                     a.UNITPRICE,
                                     a.ITEMTRACKINGCODE,
                                     a.ITEMCATEGORYCODE,
                                     a.EXTRAEXPENSES,
                                     a.TOTALEXPENSES,
                                     a.COMMENTS,
                                     b.BOOKPAGEID,
                                     b.ITEMNAME,
                                     b.SPECIFICATION,
                                     b.ITEM_DESC,
                                     c.UNITSHORT,
                                     a.WAIVED,
                                     a.WAIVEDREASON,
                                     d.UNIT_NAME,
                                     e.NAME,
                                     Column1 = e.UNIT_NAME,
                                     e.ADDRESS
                                 }).AsEnumerable().Select

                                    (x => new PRISM.Models.spAssetsDetails
                                    {
                                         ALE = x.PK_ALE_ID.ToString(),
                                        itemid = x.itemid.ToString(),
                                         postingdate = x.POSTINGDATE.HasValue ? x.POSTINGDATE.Value.ToString("yyyy-MM-dd") : "",
                                       
                                        Description = x.DESCRIPTION,
                                        Quantity = x.QUANTITY.ToString(),
                                        InvoicedQuantity = x.INVOICEDQUANTITY.ToString(),
                                        VAT = x.VAT.ToString(),
                                        PriceIncludeVAT = (x.PRICEINCLUDEVAT == 0 ? "NO" : "YES"),
                                        UnitPrice = x.UNITPRICE.ToString(),
                                        ItemCategoryCode = (x.ITEMCATEGORYCODE == 52 ? "less than year" : "more than a year"),
                                        ItemTrackingCode = (x.ITEMTRACKINGCODE == null ? " " : (x.ITEMTRACKINGCODE.ToString())),
                                        ExtraExpenses = x.EXTRAEXPENSES.ToString(),
                                        TotalExpenses = x.TOTALEXPENSES.ToString(),
                                        comments = (x.COMMENTS == null ? " " : (x.COMMENTS)),
                                        bookpageid = x.BOOKPAGEID.ToString(),
                                        ItemName = x.ITEMNAME,
                                        Specification = x.SPECIFICATION,
                                        item_Desc = x.ITEM_DESC,
                                        unitShort = x.UNITSHORT,
                                        PERSON_CODE = x.NAME,
                                        waived = x.WAIVED.ToString(),
                                        waivedReason= (x.WAIVEDREASON == null ? " " : (x.COMMENTS)),
                                        UNIT_NAME = x.UNIT_NAME,
                                        entryfrom = x.Column1,


                                        Entrytype = (x.ENTRYTYPE == null ? " " : ((x.ENTRYTYPE == 0 && x.TRANSACTIONTYPE == 1) ? "Handed From PHQ" : "Handed From Unit")),
                                       
                                    }).ToList();
                    spd = query.SingleOrDefault();
                }
                else if (_entryFor == "2" && _transType == "2")
                {
                    var query = (from a in db.TBL_ASSETLEDGER
                                 join b in db.TBL_ASSET on new { itemid = a.FK_ASSET_ID } equals new { itemid = b.PK_ASSET_ID }
                                 join c in db.TBL_MEASURINGUNITS_MASTER on new { UOM = b.FK_UOM_ID } equals new { UOM = c.PK_UOM_ID }
                                 join d in
                                     (
                                         (from Auths in db.VIEW_AUTH
                                          select new
                                          {
                                              Auths.PERSON_CODE,
                                              Auths.NAME
                                          })) on new { entryfor = a.ENTRYFOR } equals new { entryfor = d.PERSON_CODE }
                                 join e in
                                     (
                                         (from Auths in db.VIEW_AUTH
                                          select new
                                          {
                                              Auths.PERSON_CODE,
                                              Auths.NAME,
                                              Auths.UNIT_NAME,
                                              Auths.ADDRESS
                                          })) on new { entryby = a.ENTRYBY } equals new { entryby = e.PERSON_CODE }
                                 where
                                   a.PK_ALE_ID == id
                                 select new
                                 {
                                     a.PK_ALE_ID,
                                     itemid = a.FK_ASSET_ID,
                                     a.POSTINGDATE,
                                     a.ENTRYTYPE,
                                     a.TRANSACTIONTYPE,
                                     a.DESCRIPTION,
                                     a.QUANTITY,
                                     a.INVOICEDQUANTITY,
                                     a.VAT,
                                     a.PRICEINCLUDEVAT,
                                     a.UNITPRICE,
                                     a.ITEMTRACKINGCODE,
                                     a.ITEMCATEGORYCODE,
                                     a.EXTRAEXPENSES,
                                     a.TOTALEXPENSES,
                                     a.COMMENTS,
                                     b.BOOKPAGEID,
                                     b.ITEMNAME,
                                     b.SPECIFICATION,
                                     b.ITEM_DESC,
                                     c.UNITSHORT,
                                     a.WAIVED,
                                     a.WAIVEDREASON,
                                     Entryfor = d.PERSON_CODE,
                                     e.NAME,
                                     e.UNIT_NAME,
                                     e.ADDRESS
                                 }).AsEnumerable().Select(x => new PRISM.Models.spAssetsDetails
                                      {
                                           ALE = x.PK_ALE_ID.ToString(),
                                        itemid = x.itemid.ToString(),
                                           postingdate = x.POSTINGDATE.HasValue ? x.POSTINGDATE.Value.ToString("yyyy-MM-dd") : "",
                                        
                                        Description = x.DESCRIPTION,
                                        Quantity = x.QUANTITY.ToString(),
                                        InvoicedQuantity = x.INVOICEDQUANTITY.ToString(),
                                        VAT = x.VAT.ToString(),
                                        PriceIncludeVAT = (x.PRICEINCLUDEVAT == 0 ? "NO" : "YES"),
                                        UnitPrice = x.UNITPRICE.ToString(),
                                        ItemCategoryCode = (x.ITEMCATEGORYCODE == 52 ? "less than year" : "more than a year"),
                                        ItemTrackingCode = (x.ITEMTRACKINGCODE == null ? " " : (x.ITEMTRACKINGCODE.ToString())),
                                        ExtraExpenses = x.EXTRAEXPENSES.ToString(),
                                        TotalExpenses = x.TOTALEXPENSES.ToString(),
                                        comments = (x.COMMENTS == null ? " " : (x.COMMENTS)),
                                        bookpageid = x.BOOKPAGEID.ToString(),
                                        ItemName = x.ITEMNAME,
                                        Specification = x.SPECIFICATION,
                                        item_Desc = x.ITEM_DESC,
                                        unitShort = x.UNITSHORT,
                                        PERSON_CODE = x.NAME,
                                        waived = x.WAIVED.ToString(),
                                        waivedReason= (x.WAIVEDREASON == null ? " " : (x.COMMENTS)),
                                        UNIT_NAME = x.UNIT_NAME,
                                        entryfrom = x.ADDRESS,

                                         
                                          Entrytype = (x.ENTRYTYPE == null ? " " : ((x.ENTRYTYPE == 0 && x.TRANSACTIONTYPE == 1) ? "Handed From PHQ" : "Handed From Unit")),
                                       
                                      }).ToList();
                    spd = query.SingleOrDefault();

                }
            }


            return spd;
        }

        public List<PRISM.TBL_ASSET> GetAssetMasterList() {
            List<PRISM.TBL_ASSET> lst = new List<TBL_ASSET>();
            return lst;
        }

        
    }
}