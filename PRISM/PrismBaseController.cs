﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRISM
{
    public class PrismBaseController : Controller
    {
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string lang = null;
            HttpCookie langCookie = Request.Cookies["culture"];
            if (langCookie != null)
            {
                lang = langCookie.Value;
            }
            else
            {
                var userLanguage = Request.UserLanguages;
                var userlang = userLanguage != null ? userLanguage[0] : "";
                if (userlang != null)
                    lang = userlang;
                else
                    lang = SiteLanguages.GetDefaultLanguage();
            }
            new SiteLanguages().SetLanguage(lang);
            return base.BeginExecuteCore(callback, state);
        }
    }
}