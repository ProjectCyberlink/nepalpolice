﻿using System.Web.Mvc;
using System.Web.Mvc.Ajax;

public static class ImageActionLinkHelper
{
    public static string ImageActionLink(this AjaxHelper helper, string imageUrl, string altText, string titleText, string actionName, string controller, AjaxOptions ajaxOptions)
    {
        var builder = new TagBuilder("img");
        builder.MergeAttribute("src", imageUrl);
        builder.MergeAttribute("alt", altText);
        builder.MergeAttribute("title", titleText);
        var link = helper.ActionLink("[replaceme]", actionName, controller, ajaxOptions, new {style="float:left;" });
        return link.ToHtmlString().Replace("[replaceme]", builder.ToString(TagRenderMode.SelfClosing));
    }

}
