﻿using LumenWorks.Framework.IO.Csv;
using PRISM.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
//using System.Data.Objects.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;



namespace PRISM.Controllers
{

    //[Authorize(Roles = "Superuser , Admin, Registered")]

    public class HandOverController : PrismBaseController
    {
        private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
        //
        // GET: /HandOver/

        [SessionExpireFilter]
        public ActionResult Index()
        {
            var unitMaster = int.Parse(this.Session["unitmaster"].ToString());
            string[] roles = Roles.GetRolesForUser();
           
            if (roles.Contains("Admin") && unitMaster == 0)
            {

                return View("Admin/Index");
            }
            else if (roles.Contains("Registered"))
            {
                return View("Index");
            }
            else
            {

                return RedirectToAction("personalpaybook", "Account");
            }


        }

        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public JsonResult AutoCompleteTransfertoUnit(string s)
        {
            var result = (from r in db.VIEW_POLICE_UNIT where r.UNIT_NAME.ToLower().Contains(s.ToLower()) select new { r.UNIT_NAME });
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [SessionExpireFilter]
        public JsonResult AutoCompTransPerson(string s)
        {
            var sq = s.ToUpper().Trim();
            int i = int.TryParse(s, out i) ? i : 0;
            var res = db.VIEW_AUTH.Where(c => c.PERSON_CODE.Equals(sq)).Select(x => new { x.PERSON_CODE, x.NAME, x.UNIT_NAME }).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);

        }

        [SessionExpireFilter]
        [ValidateAntiForgeryToken]
        public JsonResult Persondetailview(string s)
        {
            int i = int.TryParse(s, out i) ? i : 0;
            var res = db.VIEW_AUTH.Find(i);
            return Json(res, JsonRequestBehavior.AllowGet);

        }
        
        #region "Csv Upload To TBL_CSV_UPLOAD"
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ToCsv(TBL_ASSETLEDGER asset, HttpPostedFileBase uploadFile, FormCollection val)
        {


            StringBuilder strValidations = new StringBuilder(string.Empty);
            try
            {
                if (uploadFile.ContentLength > 0)
                {
                    NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
                    // List<db.TBL_CSV_UPLOAD> listOfRecords = new List<db.TBL_CSV_UPLOAD>();
                    int count = db.TBL_ASSETLEDGER.Where(x => x.CSV_FILE_NAME == uploadFile.FileName).Count();
                    if (count > 0)
                    {
                        ModelState.AddModelError("File", "File Already Uploaded Please Choose Another !!!");
                        return View();
                    }
                    string v = asset.ToString();
                    string ENTRYBYid = User.Identity.Name;
                    string ENTRYUNITid = this.Session["UnitCode"].ToString();
                    var EntryForunitid = val["hddSelectedUnit"];

                    string filePath = Path.Combine(HttpContext.Server.MapPath("../Uploads"),
                    Path.GetFileName(uploadFile.FileName));
                    uploadFile.SaveAs(filePath);
                    DataSet ds = new DataSet();

                    //A 32-bit provider which enables the use of

                    string ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1;\"";

                    using (OleDbConnection conn = new System.Data.OleDb.OleDbConnection(ConnectionString))
                    {
                        conn.Open();
                        using (DataTable dtExcelSchema = conn.GetSchema("Tables"))
                        {
                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                            string query = "SELECT * FROM [" + sheetName + "]";
                            OleDbDataAdapter adapter = new OleDbDataAdapter(query, conn);
                            //DataSet ds = new DataSet();
                            adapter.Fill(ds, "Items");

                            TBL_CSV_UPLOAD ledgerList;
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow item in ds.Tables[0].Rows)
                                    {
                                        try
                                        {

                                            ledgerList = new TBL_CSV_UPLOAD
                                            {
                                                PK_ALE_ID = Guid.NewGuid().ToString(),
                                                FK_ASSET_ID = item["PK_ASSET_ID"].ToString(),
                                                POSTINGDATE = Convert.ToDateTime(item["POSTING_DATE"]),
                                                ENTRYTYPE = Convert.ToInt32(item["ENTRY_TYPE"]),
                                                ENTRYFOR = EntryForunitid,
                                                DESCRIPTION = item["DESCRIPTION"].ToString(),
                                                QUANTITY = item["QUANTITY"].ToString(),
                                                ITEMCATEGORYCODE = Convert.ToString(item["ITEMCATRGORYCODE"]),
                                                COMMENTS = item["COMMENTS"].ToString(),
                                                FK_VENDOR_ID = item["VENDOR_ID"].ToString(),
                                                ENTRY_DATE = Convert.ToDateTime(item["ENTRY_DATE"]),
                                                PATCH_ID = ENTRYBYid,
                                                FILE_NAME = uploadFile.FileName.ToString(),
                                                ACTIVE_FLAG = 1,
                                            };


                                            db.Configuration.ValidateOnSaveEnabled = false;
                                            db.TBL_CSV_UPLOAD.Add(ledgerList);
                                            db.SaveChanges();

                                        }


                                        catch (Exception ex)
                                        {
                                            ModelState.AddModelError("Error", ex.Message);
                                        }
                                    }
                                    ModelState.AddModelError("", "Successfully Uploaded");

                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
            }
            return View();
        }
        #endregion



        #region "View Csv Loaded Records"

        public ActionResult GetCsvRecords()
        {
            NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();

            var list = db.TBL_CSV_UPLOAD.GroupBy(x => x.FILE_NAME).Select(y => y.First()).Distinct();
            string ENTRYBYid = User.Identity.Name;
            using (NepalPoliceInventoryEntities ent = new NepalPoliceInventoryEntities())
            {

                List<TBL_CSV_UPLOAD> upload = new List<TBL_CSV_UPLOAD>();
                upload = GetAllCsvRecords(ENTRYBYid);
                return View("CsvRecords", upload);
            }
        }
        #endregion

        #region "Save To TBL_ASSET_LEDGER"
        [HttpPost]
        public ActionResult CsvRecords(FormCollection val)
        {
            NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
            // string v = itr.SelectedItemId.ToString();
            string v = val["hddSelectedUnit"];
            string ENTRYBYid = User.Identity.Name;
            string ENTRYUNITid = this.Session["UnitCode"].ToString();

            // var unitPrice = db.TBL_ASSETLEDGER.Where(x => x.FK_ASSET_ID == v).Select(y => y.UNITPRICE).First();
            // var EntryForunitid = val["hddSelectedUnit"];         
            // var unitPrice = db.TBL_ASSETLEDGER.Where(x => x.FK_ASSET_ID == v).Select(y => y.UNITPRICE).First();
            //var postDate = itr.ENTRYDATE;



            using (NepalPoliceInventoryEntities ent = new NepalPoliceInventoryEntities())
            {
                List<TBL_CSV_UPLOAD> upload = new List<TBL_CSV_UPLOAD>();
                upload = GetAllCsvRecords(ENTRYBYid);
                TBL_ASSETLEDGER tra;
                foreach (var item in upload)
                {
                    var unitPrice = db.TBL_ASSETLEDGER.Where(x => x.FK_ASSET_ID == item.FK_ASSET_ID).Select(y => y.UNITPRICE).First();
                    int count = db.TBL_ASSETLEDGER.Where(x => x.CSV_FILE_NAME == item.FILE_NAME).Count();

                    try
                    {
                        tra = new TBL_ASSETLEDGER
                        {
                            PK_ALE_ID = Guid.NewGuid().ToString(),
                            FK_ASSET_ID = item.FK_ASSET_ID, //new Guid(val["ItemsList"]),
                            POSTINGDATE = item.POSTINGDATE,
                            ENTRYTYPE = 0, /*Entry type 0 for Entry From PHQ*/
                            TRANSACTIONTYPE = 1, /*Transaction type type 1 for handover to unit*/
                            QUANTITY = Convert.ToInt32(item.QUANTITY) * -1,
                            INVOICEDQUANTITY = Convert.ToDecimal(item.QUANTITY),
                            ITEMCATEGORYCODE = Convert.ToInt32(item.ITEMCATEGORYCODE),
                            DESCRIPTION = item.DESCRIPTION,
                            VAT = 13,
                            UNITPRICE = unitPrice,
                            TOTALEXPENSES = Convert.ToDecimal(unitPrice * Convert.ToDecimal(item.QUANTITY)).ToString(),
                            PRICEINCLUDEVAT = 1,
                            WAIVED = 0,
                            ITEMTRACKINGCODE = getITEMTRACKINGCODE(item.FK_ASSET_ID),
                            EXTRAEXPENSES = 0,
                            COMMENTS = item.COMMENTS,
                            ENTRYDATE = item.ENTRY_DATE,
                            ENTRYBY = ENTRYBYid,
                            ENTRYUNIT = ENTRYUNITid,
                            OPEN = 0,
                            SelectedVendorId = item.FK_VENDOR_ID,
                            ENTRYFOR = item.ENTRYFOR,
                            //SelectedItemId = v,
                            FK_VENDORID = "2efcfe7f-8242-42a9-930b-3015b5e0f6e9",
                            CSV_FILE_NAME = item.FILE_NAME
                        };

                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.TBL_ASSETLEDGER.Add(tra);
                        db.TBL_CSV_UPLOAD.RemoveRange(db.TBL_CSV_UPLOAD);
                        db.SaveChanges();


                    }

                    catch (Exception e)
                    {

                        var ex = e;
                        ModelState.AddModelError("Error", e);
                    }


                }
                ViewData["StatusMessage"] = "Records Saved Successfully";

                return View();


            }
        }



        #endregion

        #region "Get Csv Records"
        [NonAction]
        public List<TBL_CSV_UPLOAD> GetAllCsvRecords(string EntryBy)
        {

            PRISM.Repository<TBL_CSV_UPLOAD> csvResult = new Repository<TBL_CSV_UPLOAD>();

            return csvResult.FindBy(x => x.ACTIVE_FLAG == 1 && x.PATCH_ID == EntryBy);
        }

        #endregion

        #region "Handover To Unit Post Action"

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult ToUnit(TBL_ASSETLEDGER itr, FormCollection val)
        {
            TBL_ASSETLEDGER tra;

            string v = itr.SelectedItemId.ToString();
            string ENTRYBYid = User.Identity.Name;
            string ENTRYUNITid = this.Session["UnitCode"].ToString();
            var EntryForunitid = val["hddSelectedUnit"];


            var unitMaster = int.Parse(this.Session["unitmaster"].ToString());
            string[] roles = Roles.GetRolesForUser();
            /*code added by Subash */
            var unitPrice = db.TBL_ASSETLEDGER.Where(x => x.FK_ASSET_ID == v).Select(y => y.UNITPRICE).First();
            var postDate = itr.ENTRYDATE;


            if (roles.Contains("Admin") && unitMaster == 0)
            {
                tra = new TBL_ASSETLEDGER
                {
                    PK_ALE_ID = Guid.NewGuid().ToString(),
                    FK_ASSET_ID = v, //new Guid(val["ItemsList"]),
                    POSTINGDATE = postDate,
                    ENTRYTYPE = 0, /*Entry type 0 for Entry From PHQ*/
                    TRANSACTIONTYPE = 1, /*Transaction type type 1 for handover to unit*/
                    QUANTITY = itr.QUANTITY * -1,
                    INVOICEDQUANTITY = itr.QUANTITY,
                    ITEMCATEGORYCODE = itr.ITEMCATEGORYCODE,
                    BOOKPAGEID = itr.BOOKPAGEID,
                    TOTALEXPENSES = itr.TOTALEXPENSES,
                    DESCRIPTION = itr.DESCRIPTION,
                    VAT = 13,
                    UNITPRICE = unitPrice,
                    PRICEINCLUDEVAT = 1,
                    WAIVED = 0,
                    ITEMTRACKINGCODE = getITEMTRACKINGCODE(v),
                    EXTRAEXPENSES = 0,
                    COMMENTS = itr.COMMENTS,
                    ENTRYDATE = itr.ENTRYDATE,
                    ENTRYBY = ENTRYBYid,
                    ENTRYUNIT = ENTRYUNITid,
                    OPEN = 1,
                    SelectedVendorId = itr.SelectedVendorId = "2efcfe7f-8242-42a9-930b-3015b5e0f6e9",
                    ENTRYFOR = EntryForunitid,
                    SelectedItemId = itr.SelectedItemId,
                    FK_VENDORID = "2efcfe7f-8242-42a9-930b-3015b5e0f6e9"


                };

            }
            else if (roles.Contains("Admin") && unitMaster != 0)
            {
                tra = new TBL_ASSETLEDGER
                {
                    PK_ALE_ID = Guid.NewGuid().ToString(),
                    FK_ASSET_ID = v,//val["ItemsList"],
                    POSTINGDATE = postDate,
                    ENTRYTYPE = 1, /*Entry type 1 for Entry From unit*/
                    TRANSACTIONTYPE = 0, /*Transaction type type 1 for handover to Unit*/
                    ENTRYFOR = EntryForunitid,//val["UnitList"],
                    QUANTITY = itr.QUANTITY * -1,
                    INVOICEDQUANTITY = itr.QUANTITY,
                    ITEMCATEGORYCODE = itr.ITEMCATEGORYCODE,
                    BOOKPAGEID = itr.BOOKPAGEID,
                    TOTALEXPENSES = itr.TOTALEXPENSES,
                    DESCRIPTION = itr.DESCRIPTION,
                    VAT = 13,

                    UNITPRICE = unitPrice,
                    PRICEINCLUDEVAT = 1,
                    WAIVED = 0,
                    ITEMTRACKINGCODE = getITEMTRACKINGCODE(v),
                    EXTRAEXPENSES = 0,
                    COMMENTS = itr.COMMENTS,
                    ENTRYDATE = itr.ENTRYDATE,
                    ENTRYBY = ENTRYBYid,
                    ENTRYUNIT = ENTRYUNITid,
                    OPEN = 1,

                    SelectedVendorId = itr.SelectedVendorId = "2efcfe7f-8242-42a9-930b-3015b5e0f6e9",
                    SelectedItemId = itr.SelectedItemId,
                    FK_VENDORID = "2efcfe7f-8242-42a9-930b-3015b5e0f6e9"

                };
            }
            else
            {
                return RedirectToAction("personalpaybook", "Account");
            }
            try
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                db.TBL_ASSETLEDGER.Add(tra);
                db.SaveChanges();
                updateDatabaseFor(v);
                return RedirectToAction("ToUnit");

            }
            catch (Exception e)
            {
                var ex = e;

            }

            return View();
        }
        #endregion

        #region "Handover To Unit Get"


        [SessionExpireFilter]
        public ActionResult ToUnit()
        {
            SetViewBagsForUnit();
            return View();
        }
        #endregion


        #region "To Csv"
        [SessionExpireFilter]
        public ActionResult ToCsv()
        {
            SetViewBagsForUnit();
            return View();
        }
        #endregion


        public void SetViewBagsForUnit()
        {
            Session["IsWeaponReturn"] = false;
            ViewBag.UnitList = GetUnitFromSession();
            ViewBag.ItemsList = db.TBL_ASSET;
            ViewBag.WAitems = new SelectList(getReportWAitems(), string.Empty);
        }


        [SessionExpireFilter]
        public JsonResult AutoComplete(string term)
        {
            var data = Session["LowerUnits"] as List<Unit>;
            //var filteredUnits = db.VIEW_POLICE_UNIT.Where(unit => unit.UNIT_NAME.StartsWith(term.ToUpper())).Select(unit => new { unit.ID, unit.UNIT_NAME });
            var filteredUnits = data.Where(unit => unit.UnitName.StartsWith(term.ToUpper())).Select(unit => new { ID = unit.UnitId, UNIT_NAME = unit.UnitName }).ToList();
            JsonResult result = Json(filteredUnits, JsonRequestBehavior.AllowGet);
            return result;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult ToPerson(TBL_ASSETLEDGER pb, FormCollection val)
        {
            TBL_ASSETLEDGER pbk;
            var unitMaster = int.Parse(this.Session["unitmaster"].ToString());
            string[] roles = Roles.GetRolesForUser();
            string v = pb.SelectedItemId.ToString(); //val["ItemsList"];
            string ENTRYBYid = User.Identity.Name;
            string ENTRYUNITid = this.Session["UnitCode"].ToString();
            var unitPrice = db.TBL_ASSETLEDGER.Where(x => x.FK_ASSET_ID == v).Select(y => y.UNITPRICE).First();

            switch (ENTRYUNITid)
            {
                //case 1:
                case "PHQ":
                    {
                        pb.ENTRYTYPE = 1;
                        pb.TRANSACTIONTYPE = 1;
                        break;
                    }
                default:
                    {
                        pb.ENTRYTYPE = 2;
                        pb.TRANSACTIONTYPE = 2;
                        break;
                    }
            }

            pbk = new TBL_ASSETLEDGER
            {
                PK_ALE_ID = Guid.NewGuid().ToString(),
                FK_ASSET_ID = v,
                ENTRYFOR = val["hddSelectedUnit"],//int.Parse(val["PersonList"]),
                ENTRYTYPE = pb.ENTRYTYPE,
                TRANSACTIONTYPE = pb.TRANSACTIONTYPE,
                QUANTITY = pb.QUANTITY * -1,
                INVOICEDQUANTITY = pb.QUANTITY,
                POSTINGDATE = pb.ENTRYDATE,
                TOTALEXPENSES = pb.TOTALEXPENSES,
                DESCRIPTION = pb.DESCRIPTION,
                BOOKPAGEID = pb.BOOKPAGEID,
                ITEMCATEGORYCODE = pb.ITEMCATEGORYCODE,
                ITEMTRACKINGCODE = getITEMTRACKINGCODE(v),
                COMMENTS = pb.COMMENTS,
                PRICEINCLUDEVAT = 1,
                ENTRYDATE = pb.ENTRYDATE,
                ENTRYBY = ENTRYBYid,
                ENTRYUNIT = ENTRYUNITid,
                VAT = 13,
                EXTRAEXPENSES = 0,
                WAIVED = 0,
                OPEN = 1,
                SelectedItemId = "dummy",
                SelectedVendorId = "dummy",
                FK_VENDORID = pb.FK_VENDORID,
                UNITPRICE = decimal.Parse(pb.TOTALEXPENSES) / pb.QUANTITY
            };




            try
            {
                db.TBL_ASSETLEDGER.Add(pbk);
                db.SaveChanges();
                updateDatabaseFor(v);
                return RedirectToAction("ToPerson");

            }
            catch (Exception ee)
            {
                var exc = ee;
            }
            return View();
        }

        [SessionExpireFilter]
        public ActionResult ToPerson()
        {

            ViewBag.ItemsList = db.TBL_ASSET;

            return View();
        }

        public TBL_ASSETLEDGER dummyLegder
        {
            get
            {
                return new TBL_ASSETLEDGER
                {
                    POSTINGDATE = DateTime.Now,
                    SelectedVendorId = "dummy",
                    INVOICEDQUANTITY = 0,
                    VAT = 0,
                    UNITPRICE = 0,
                    ENTRYFOR = "dummy"
                };
            }
        }

        public List<Unit> GetUnitFromSession()
        {
            var _loggedInUnit = this.Session["UnitCode"].ToString();
            List<Unit> data = null;
            if (Session["LowerUnits"] == null)
            {


                data = db.Database.SqlQuery<Unit>(@"select  DISTINCT PU.ID UnitId, PU.UNIT_NAME UnitName FROM VIEW_POLICE_UNIT PU JOIN VIEW_AUTH VA ON PU.UNIT_NAME = VA.UNIT_NAME JOIN
                                                                        TBL_USER_DETAILS UD ON VA.PERSON_CODE = UD.PERSON_CODE 
                                                                        JOIN 
                                                                        TBL_IROLES R ON
                                                                        UD.FK_ROLEID = R.PK_ROLEID
                                                                        WHERE PU.ID <> '" + _loggedInUnit + @"' AND R.ROLENAME IN ('Admin', 'Superuser')").ToList<Unit>();
                Session["LowerUnits"] = data;
            }
            else
                data = Session["LowerUnits"] as List<Unit>;
            return data;
        }

        [SessionExpireFilter]
        public PartialViewResult SingleHandOver(string formId)
        {
            SetViewBagsForUnit();
            ViewBag.PersonList = db.VIEW_AUTH;
            return PartialView("_SingleHandOver", dummyLegder);
        }

        [SessionExpireFilter]
        public ActionResult MultipleHandOver()
        {
            SetViewBagsForUnit();
            ViewBag.PersonList = db.VIEW_AUTH;
            return View(new List<TBL_ASSETLEDGER>() { dummyLegder }.AsEnumerable());
        }

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult MultipleHandOver(IEnumerable<TBL_ASSETLEDGER> assets)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var unitMaster = int.Parse(this.Session["unitmaster"].ToString());
                    if (Roles.GetRolesForUser().Contains("Admin"))
                    {
                        foreach (var assetLedger in assets)
                        {
                            AssetsToUnitOrPerson(assetLedger, unitMaster);
                        }
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        return RedirectToAction("MultipleHandOver");
                    }
                    else
                    {
                        return RedirectToAction("personalpaybook", "Account");
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }

            }
            SetViewBagsForUnit();
            ViewBag.PersonList = db.VIEW_AUTH;
            return View(assets);
        }

        public void AssetsToUnitOrPerson(TBL_ASSETLEDGER itr, int unitMaster)
        {
            int transactionType = 0;
            int entryType = 0;
            if (itr.UnitOrPerson.Equals("UNIT"))
            {
                transactionType = unitMaster == 0 ? 1 : 0;
                entryType = unitMaster == 0 ? 0 : 1;
            }
            else if (itr.UnitOrPerson.Equals("PERSON"))
            {
                transactionType = entryType = unitMaster == 0 ? 1 : 2;
            }


            TBL_ASSETLEDGER tra = new TBL_ASSETLEDGER
            {
                PK_ALE_ID = Guid.NewGuid().ToString(),
                FK_ASSET_ID = itr.SelectedItemId, //new Guid(val["ItemsList"]),
                POSTINGDATE = DateTime.Now,
                ENTRYTYPE = entryType,
                /*Entry type 0 for Entry From PHQ*/
                TRANSACTIONTYPE = transactionType,
                /*Transaction type type 1 for handover to unit*/
                QUANTITY = itr.QUANTITY * -1,
                INVOICEDQUANTITY = itr.QUANTITY,
                ITEMCATEGORYCODE = itr.ITEMCATEGORYCODE,
                BOOKPAGEID = itr.BOOKPAGEID,
                TOTALEXPENSES = itr.TOTALEXPENSES,
                DESCRIPTION = itr.DESCRIPTION,
                VAT = 13,
                UNITPRICE = itr.UNITPRICE = 3000,
                PRICEINCLUDEVAT = 1,
                WAIVED = 0,
                ITEMTRACKINGCODE = getITEMTRACKINGCODE(itr.SelectedItemId),
                EXTRAEXPENSES = 0,
                COMMENTS = itr.COMMENTS,
                ENTRYDATE = DateTime.Now,
                ENTRYBY = User.Identity.Name,
                ENTRYUNIT = Session["UnitCode"].ToString(),
                OPEN = 1,
                SelectedVendorId = itr.SelectedVendorId = "dummy",
                ENTRYFOR = unitMaster == 0 ? itr.ENTRYFOR : null,
                SelectedItemId = itr.SelectedItemId
            };
            db.TBL_ASSETLEDGER.Add(tra);
            updateDatabaseFor(itr.SelectedItemId, false);
        }

        [SessionExpireFilter]
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public string getITEMTRACKINGCODE(string id)
        {
            var FK_ASSET_ID_PK_ALE_ID = (from al in db.TBL_ASSETLEDGER where al.OPEN == 1 && al.FK_ASSET_ID == id select new { AId = al.PK_ALE_ID, al.PK_ALE_ID, FK_ASSET_ID = al.FK_ASSET_ID, POSTINGDATE = al.POSTINGDATE, ENTRYTYPE = al.ENTRYTYPE, ENTRYFOR = al.ENTRYFOR, DESCRIPTION = al.DESCRIPTION, QUANTITY = al.QUANTITY, RemainingQUANTITY = al.REMAININGQUANTITY, INVOICEDQUANTITY = al.INVOICEDQUANTITY, Adj = al.ADJ, AdjType = al.ADJTYPE, TRANSACTIONTYPE = al.TRANSACTIONTYPE, al.VAT, PRICEINCLUDEVAT = al.PRICEINCLUDEVAT, UNITPRICE = al.UNITPRICE, ITEMCATEGORYCODE = al.ITEMCATEGORYCODE, ITEMTRACKINGCODE = al.ITEMTRACKINGCODE, open = al.OPEN, vendorid = al.FK_VENDORID, EXTRAEXPENSES = al.EXTRAEXPENSES, TOTALEXPENSES = al.TOTALEXPENSES, COMMENTS = al.COMMENTS, ENTRYBY = al.ENTRYBY, ENTRYDATE = al.ENTRYDATE, editdate = al.EDITDATE, lasteditdat = al.LASTEDITDAT, WAIVED = al.WAIVED, WAIVEDReason = al.WAIVEDREASON }).First();
            return (FK_ASSET_ID_PK_ALE_ID.PK_ALE_ID).ToString();
        }
        /// <summary>
        /// Update the Database for the line entry in the TBL_ASSETLEDGER entry of the first entry which is non negative
        /// this is most probably entered by the PHQ
        /// </summary>
        /// <param name="id"></param>
        public void updateDatabaseFor(string id, bool SaveImmediately = true)
        {
            var getPK_ALE_ID = getITEMTRACKINGCODE(id);
            var getItemCode = id;
            var xs = (from a in
                          (
                              (from TBL_ASSETLEDGER in db.TBL_ASSETLEDGER
                               where
                                 TBL_ASSETLEDGER.ITEMTRACKINGCODE == getPK_ALE_ID &&
                                 TBL_ASSETLEDGER.QUANTITY < 0
                               group TBL_ASSETLEDGER by new
                               {
                                   TBL_ASSETLEDGER.FK_ASSET_ID
                               } into g
                               select new
                               {
                                   g.Key.FK_ASSET_ID,
                                   actualqty = (decimal?)g.Sum(p => p.QUANTITY)
                               }))
                      join b in
                          (
                              (from TBL_ASSETLEDGER in db.TBL_ASSETLEDGER
                               where
                                 TBL_ASSETLEDGER.FK_ASSET_ID == id &&
                                 TBL_ASSETLEDGER.PK_ALE_ID == getPK_ALE_ID
                               select new
                               {
                                   item = TBL_ASSETLEDGER.FK_ASSET_ID,
                                   TBL_ASSETLEDGER.QUANTITY
                               })) on new { FK_ASSET_ID = a.FK_ASSET_ID } equals new { FK_ASSET_ID = b.item }
                      select new
                      {
                          a.FK_ASSET_ID,
                          a.actualqty,
                          b.item,
                          b.QUANTITY
                      }).ToList();
            var Remainingqty = xs.Select(ss => ss.actualqty).ToList();
            var aQUANTITY = xs.Select(ss => ss.QUANTITY).ToList();
            try
            {
                var PK_ALE_IDdger = (from q in db.TBL_ASSETLEDGER where q.PK_ALE_ID == getPK_ALE_ID select q).Single();
                PK_ALE_IDdger.REMAININGQUANTITY = (decimal)Remainingqty[0];
                if (aQUANTITY[0] < Remainingqty[0])
                {
                    PK_ALE_IDdger.OPEN = 0;
                }

                if (SaveImmediately)
                {
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                var ex = e;
            }
        }
        /// <summary>
        /// Return Data if the Selected item is ok to Distribute to the particular person
        /// based on the TBL_CONSUMPTION_PERIOD table
        /// </summary>
        /// <param name="FK_ASSET_ID"></param>
        /// <param name="personid"></param>
        /// <returns></returns>


        public JsonResult GetRecordById(string id)
        {

            // string query =
            var consumptionPeriod = (from a in db.VIEW_AUTH
                                     where
                                         a.PERSON_CODE == id
                                     //orderby a.ENTRYTIME descending
                                     select new
                                     {

                                         a.NAME
                                     }).FirstOrDefault();
            if (consumptionPeriod != null)
            {

                var d = Json(consumptionPeriod, JsonRequestBehavior.AllowGet);
                return d;
            }
            else
            {

                var d = Json("", JsonRequestBehavior.AllowGet);
                return d;

            }




        }



        public JsonResult getIfEligible(string itemid, string personid)
        {
            List<PRISM.Models.ConsumptionStatus> lst = new List<Models.ConsumptionStatus>();
            decimal allowedQty = 0;
            var consumptionPeriod = (from a in db.TBL_CONSUMPTION_PERIOD
                                     where
                                         a.FK_ITEMID == itemid
                                     orderby a.ENTRYTIME descending
                                     select new
                                     {
                                         FK_ItemId = a.FK_ITEMID,
                                         a.ALLOWEDQTY,
                                         a.CONSUMPTIONPERIOD,
                                         a.CONSUMPTIONTIME
                                     }).FirstOrDefault();
            if (consumptionPeriod != null)
            {
                var consumptionStartDate = ConsumptionRule.GetConsumptionStartDate((int)consumptionPeriod.CONSUMPTIONPERIOD, consumptionPeriod.CONSUMPTIONTIME);

                var consumedAsset = (from a in db.TBL_ASSETLEDGER
                                     join c in
                                         (
                                         (from x in db.VIEW_AUTH
                                          select new
                                          {
                                              x.PERSON_CODE,
                                              x.NAME,
                                              x.UNIT_NAME,
                                              x.ADDRESS
                                          })) on new { entryfor = a.ENTRYFOR } equals new { entryfor = c.PERSON_CODE }

                                     where
                                     ((a.ENTRYTYPE == 1 && a.TRANSACTIONTYPE == 1) || (a.ENTRYTYPE == 2 && a.TRANSACTIONTYPE == 2))
                                     && a.ENTRYFOR == personid && a.FK_ASSET_ID == itemid && a.POSTINGDATE >= consumptionStartDate
                                     select new
                                     {
                                         a.FK_ASSET_ID,
                                         a.INVOICEDQUANTITY
                                     }).ToList();

                decimal consumedQuantity = Convert.ToDecimal(consumedAsset.Sum(a => a.INVOICEDQUANTITY));
                allowedQty = Convert.ToDecimal(consumptionPeriod.ALLOWEDQTY) - consumedQuantity;
                if (allowedQty > 0)
                {
                    lst.Add(new Models.ConsumptionStatus { status = "ok", data = "You Are Eligible To Recieve this Item", maxallqty = allowedQty.ToString("n") });
                    var d = Json(lst, JsonRequestBehavior.AllowGet);
                    return d;
                }
                else
                {
                    allowedQty = 0;
                    lst.Add(new Models.ConsumptionStatus { status = "error", data = "You Are not Eligible To Recieve this Item", maxallqty = allowedQty.ToString("n") });
                    var d = Json(lst, JsonRequestBehavior.AllowGet);
                    return d;
                }
            }
            else
            {
                var _itmNam = db.TBL_ASSET.Where(s => s.PK_ASSET_ID == itemid).ToList();
                var message = "{Status:0,data:" + _itmNam[0].ITEMNAME + " has not been registered for consumption for personnel}";
                lst.Add(new Models.ConsumptionStatus { status = "0", data = _itmNam[0].ITEMNAME + " has not been registered for consumption for personnel", maxallqty = allowedQty.ToString("n") });
                var d = Json(lst, JsonRequestBehavior.AllowGet);
                return d;
            }
        }


        /// <summary>
        /// Gets the Unit Price and the Available QUANTITY for the Specified item with FK_ASSET_ID
        /// </summary>
        /// <param name="FK_ASSET_ID"></param>
        /// <returns></returns>
        /// 

        public JsonResult getUpNqty(string itemid, bool ignoreConsumptionRule)
        {


            var unitId = this.Session["UnitCode"].ToString();
            string query =
        @"Select ItemName, StockQuantity-HandedQuantity as RemainingQuantity, nvl(UnitPrice,0) UnitPrice, Vendor from (
SELECT FK_ASSET_ID AS ItemName,
(select nvl(Sum(ABS(quantity)),0) from tbl_assetledger where entryfor = '" + unitId + @"' and waived = 0 and open=1 and fk_asset_id=TA.FK_ASSET_ID) AS StockQuantity,
(select nvl(Sum(ABS(quantity)),0) deducted from tbl_assetledger where entryfor != '" + unitId + "' and entryunit = '" + unitId + @"' and waived = 0 and open=1 and fk_asset_id=TA.FK_ASSET_ID) AS HandedQuantity,
UNITPRICE        AS UnitPrice,
FK_VENDORID      AS Vendor
FROM TBL_ASSET A LEFT JOIN TBL_ASSETLEDGER TA ON A.PK_ASSET_ID=TA.FK_ASSET_ID 
AND A.PK_ASSET_ID = '" + itemid + @"'
AND TA.OPEN           = 1
AND TA.WAIVED        <> 1
) where ROWNUM = 1 AND ITEMNAME IS NOT NULL";
            var _upriceNqty =
               db.Database.SqlQuery<Models.AssetsQuantity>(query).FirstOrDefault();
            if (!ignoreConsumptionRule)
            {
                var _ConsumptionPeriod = (from a in db.TBL_CONSUMPTION_PERIOD
                                          where
                                              a.FK_ITEMID == itemid
                                          orderby a.ENTRYTIME descending
                                          select new
                                          {
                                              FK_ItemId = a.FK_ITEMID,
                                              a.ALLOWEDQTY,
                                              a.CONSUMPTIONPERIOD,
                                              a.CONSUMPTIONTIME
                                          }).FirstOrDefault();

                if (_upriceNqty != null)
                {
                    _upriceNqty.Quantity =
                        (int)(_ConsumptionPeriod != null && _upriceNqty.RemainingQuantity > _ConsumptionPeriod.ALLOWEDQTY
                                   ? _ConsumptionPeriod.ALLOWEDQTY
                                   : _upriceNqty.RemainingQuantity);
                }
            }
            else
            {
                _upriceNqty.Quantity = _upriceNqty.RemainingQuantity;
            }

            var d = Json(_upriceNqty, JsonRequestBehavior.AllowGet);

            return d;
        }

        public List<string> getReportWAitems()
        {
            List<string> st = new List<string>();
            st.Add("Asset");
            st.Add("Weapons");
            return st;
        }

        public JsonResult GetItemList(string Itype)
        {
            using (NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities())
            {
                if (Itype == "Asset")
                {
                    var query = from x in db.TBL_ASSET
                                select new
                                {
                                    id = x.PK_ASSET_ID,
                                    display = x.ITEMNAME

                                };
                    return Json(query.ToList(), JsonRequestBehavior.AllowGet);
                }
                else if (Itype == "Weapons")
                {
                    var query = from x in db.TBL_WEAPON_DETAIL
                                select new
                                {
                                    id = x.PK_WEAPONS_ID,
                                    display = x.WEAPON_NAME
                                };
                    return Json(query.ToList(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Null", JsonRequestBehavior.AllowGet);
                }
            }

        }
    }
}
