﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PRISM.Models;
using Newtonsoft;
using System.Web.Script.Serialization;
using System.Net.Http;
using System.Text;
using PRISM.Filters;
namespace PRISM.Controllers
{

    [Authorize(Roles="Admin")]
    [SessionExpireFilter]
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class HomeController : PrismBaseController
    {
        private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();


        /// <summary>
        /// Localization(for nepali or english)
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public ActionResult ChangeCulture(string lang)
        {
            var langcookie = new HttpCookie("lang", lang) { HttpOnly = true };
            Response.AppendCookie(langcookie);
            return RedirectToAction("Index", "Home", new { culture = lang });
        }

        [SessionExpireFilter]
        public ActionResult Index()
        {
            ViewBag.sessonset1 = this.Session["person"];
            ViewBag.sessonset2 = this.Session["UnitCode"];
            ViewBag.sessonset3 = this.Session["unitmaster"];
            return View();
        }

        public ActionResult Test()
        {


            var treeView = (from e1 in db.VIEW_POLICE_UNIT
                                     where e1.MASTER == "NEP" && e1.ID == "PHQ"
                                     select new OrgHierarchy()
                                     {
                                         UnitCode = e1.ID,
                                         UnitName = e1.UNIT_NAME
                                     }).AsEnumerable();
            //BuildChildNode(treeView);
            return View(treeView);
        
        }
        int level = 0;
        private void BuildChildNode(OrgHierarchy rootNode)
        {
            if (level < 2)
            {
                level++;

                if (rootNode != null)
                {
                    if (rootNode.ChildNodes == null)
                        rootNode.ChildNodes = new List<OrgHierarchy>();
                    List<OrgHierarchy> chidNode = (from e1 in db.VIEW_POLICE_UNIT
                                                   where e1.MASTER == rootNode.UnitCode
                                                   select new OrgHierarchy()
                                                   {
                                                       UnitCode = e1.ID,
                                                       UnitName = e1.UNIT_NAME
                                                   }).ToList<OrgHierarchy>();
                    //if (chidNode.Count > 0)

                    foreach (var childRootNode in chidNode)
                    {
                        rootNode.ChildNodes.Add(childRootNode);

                        BuildChildNode(childRootNode);


                    }

                    level--;

                }
            }
            }
        

        public PartialViewResult ChildNode()
        {
            return PartialView();
        }


        public JsonResult PopulateUnit(string masterCode)
        {
            var treeView = (from e1 in db.VIEW_POLICE_UNIT
                            where e1.MASTER == masterCode //&& e1.ID == "PHQ"
                            select new OrgHierarchy()
                            {
                                UnitCode = e1.ID,
                                UnitName = e1.UNIT_NAME
                            }).AsEnumerable();
            //BuildChildNode(treeView);
            
            return Json(treeView, JsonRequestBehavior.AllowGet);
        }
       

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult ChangeLanguage(string lang)
        {
            new SiteLanguages().SetLanguage(lang);
            return RedirectToAction("Index", "Home");
        }
    }
}
