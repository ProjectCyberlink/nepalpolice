﻿using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;


namespace PRISM.Controllers
{
    public class CsvController : Controller
    {
        //
        // GET: /Csv/
        public ActionResult UploadCsv()
        {
            HandOverController hand = new HandOverController();
            hand.SetViewBagsForUnit();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadCSV(HttpPostedFileBase upload, FormCollection val)
        {

            if (ModelState.IsValid)
            {

                if (upload != null && upload.ContentLength > 0)
                {

                    if (upload.FileName.EndsWith(".csv"))
                    {
                        NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();

                        Stream stream = upload.InputStream;
                        TempData["stream"] = stream;
                        DataTable csvTable = new DataTable();
                        using (CsvReader csvReader =
                            new CsvReader(new StreamReader(stream), true))
                        {
                            csvTable.Load(csvReader);
                        }
                        TBL_CSV_UPLOAD ledgerList;
                        foreach (var item in csvTable.Rows)
                        {


                            ledgerList = new    TBL_CSV_UPLOAD
                            {
                                PK_ALE_ID = Guid.NewGuid().ToString(),
                                FK_ASSET_ID = csvTable.Rows[0]["PK_ASSET_ID"].ToString(),
                                POSTINGDATE = Convert.ToDateTime(csvTable.Rows[2]["POSTING_DATE"]),
                                ENTRYTYPE = Convert.ToInt32(csvTable.Rows[3]["ENTRY_TYPE"]),
                                ENTRYFOR = csvTable.Rows[4]["ENTRY_FOR"].ToString(),
                                DESCRIPTION = csvTable.Rows[5]["DESCRIPTION"].ToString(),
                                QUANTITY = csvTable.Rows[6]["QUANTITY"].ToString(),
                                ITEMCATEGORYCODE = Convert.ToString(csvTable.Rows[7]["ITEMCATRGORYCODE"]),
                                COMMENTS = csvTable.Rows[7]["COMMENTS"].ToString(),
                                FK_VENDOR_ID = csvTable.Rows[8]["VENDOR_ID"].ToString(),
                                ENTRY_DATE = Convert.ToDateTime(csvTable.Rows[9]["ENTRY_DATE"]),
                               
                                
                            };
                            db.Configuration.ValidateOnSaveEnabled = false;
                            db.TBL_CSV_UPLOAD.Add(ledgerList);
                            db.SaveChanges();
                            return RedirectToAction("ToUnit");
                        }                        

                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        return View();
                    }

                }
                else
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
            }
            return View();
        }
        
    }
}

