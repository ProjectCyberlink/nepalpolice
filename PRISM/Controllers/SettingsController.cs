﻿using PRISM.Filters;
using PRISM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRISM.Controllers
{

    [Authorize]
    [SessionExpireFilter]
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class SettingsController : PrismBaseController
    {
        private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
        //
        // GET: /Settings/

        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        public PartialViewResult VendorSetup()
        {

            return PartialView("iVendorSetup");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult AddVendor(TBL_VENDOR vnd)
        {
            vnd.PK_VENDORID = Guid.NewGuid().ToString();
            vnd.BLOCKED = 0;
            try
            {
                db.TBL_VENDOR.Add(vnd);
                db.SaveChanges();
                return View("Index");

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return View();

            }
            catch (Exception e)
            {

                var ex = e;
                return View();
            }
        }

        [SessionExpireFilter]
        public ActionResult ViewVendor()
        {
            var dat = new SettingsDbHelper();
            var data = dat.getVendorDetail();
            return PartialView("iViewVendor", data);
        }

        [SessionExpireFilter]
        public ActionResult DetailVendor(string id)
        {
            var qid = id;
            var data = (from x in db.TBL_VENDOR
                        where x.PK_VENDORID == qid
                        select new
                        {
                            x.VENDORNAME,
                            x.ADDRESS,
                            x.ADDRESS2,
                            x.CONTACTPERSON,
                            x.PHONENO,
                            x.PHONENO1,
                            x.EMAIL,
                            x.FAXNO,
                            x.TERRETORY,
                            x.COUNTRY,
                            x.CURRENCYCODE,
                            x.VATREGNO,
                            x.BLOCKED

                        }).AsEnumerable().Select(xs => new PRISM.Models.spVendor
                        {
                            VendorName = xs.VENDORNAME,
                            Address = (xs.ADDRESS == null ? "" : xs.ADDRESS),
                            Address2 = (xs.ADDRESS2 == null ? "" : xs.ADDRESS2),
                            ContactPerson = (xs.CONTACTPERSON == null ? "" : xs.CONTACTPERSON),
                            PhoneNo = (xs.PHONENO == null ? "" : xs.PHONENO),
                            PhoneNo1 = (xs.PHONENO1 == null ? "" : xs.PHONENO1),
                            Email = (xs.EMAIL == null ? "" : xs.EMAIL),
                            FaxNo = (xs.FAXNO == null ? "" : xs.FAXNO),
                            Terretory = (xs.TERRETORY == null ? "" : xs.TERRETORY),
                            Country = (xs.COUNTRY == null ? "" : xs.COUNTRY),
                            CurrencyCod = (xs.CURRENCYCODE == null ? "" : xs.CURRENCYCODE),
                            VatRegistrationNo = (xs.VATREGNO == null ? "" : xs.VATREGNO),
                            isActice = (xs.BLOCKED == 0 ? "Yes" : "No")
                        });
            var rdata = data.SingleOrDefault();
            return PartialView("iVendorDetail", rdata);
        }

        [SessionExpireFilter]
        public ActionResult ConsumptionPeriod()
        {
            var query = (from a in db.TBL_ASSET
                         join b in db.TBL_CONSUMPTION_PERIOD on new { guid = a.PK_ASSET_ID } equals new { guid = b.FK_ITEMID } into b_join
                         from b in b_join.DefaultIfEmpty()
                         where
                           b.FK_ITEMID == null
                         select new
                         {
                             a.ITEMNAME,
                             guid = a.PK_ASSET_ID
                         }).ToList();
            ViewBag.ConsumptionItem = new SelectList(query, "guid", "ITEMNAME");
            ViewBag.TimeFrams = new SelectList(getDateTypes(), string.Empty);
            return PartialView("iConsumptionPeriod");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult AddConsumptionPeriod(PRISM.TBL_CONSUMPTION_PERIOD p, FormCollection val)
        {
            int entrybyid = int.Parse(User.Identity.Name.ToString());
            //p.FK_ItemId = new Guid(val["ConsumptionItem"]);
            //p.ConsumptionTime = val["TimeFrams"];
            p.PK_DATEID = Guid.NewGuid().ToString();
            p.ENTRYBY = entrybyid;
            p.ENTRYTIME = DateTime.Now.Date;
            try
            {
                db.TBL_CONSUMPTION_PERIOD.Add(p);
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                var ex = e;
                return View();
            }

        }

        [SessionExpireFilter]
        public ActionResult ViewConsumption()
        {
            var dat = new SettingsDbHelper();
            var data = dat.GetConsumptionPeriodDetail();
            return PartialView("iViewConsumption", data);
        }

        [SessionExpireFilter]
        public ActionResult DetailConsuptionP(string id)
        {
            var cid = new Guid(id);
            //var data = (from xi in db.tbl_ConsumptionPeriod
            //            join xj in db.Assets on new { itemid = xi.FK_ItemId } equals new { itemid = xj.guid }
            //            where xi.PK_DateId == cid
            //            select new
            //            {

            //            });
            var query = (from a in db.TBL_CONSUMPTION_PERIOD
                         join b in db.TBL_ASSET on new { FK_ItemId = a.FK_ITEMID } equals new { FK_ItemId = b.PK_ASSET_ID }
                         where
                           a.PK_DATEID == id
                         select new
                         {
                             a.PK_DATEID,
                             a.FK_ITEMID,
                             a.ALLOWEDQTY,
                             a.CONSUMPTIONPERIOD,
                             a.CONSUMPTIONTIME,
                             a.ENTRYBY,
                             a.ENTRYTIME,
                             a.LASTEDITDATE,
                             b.PK_ASSET_ID,
                             b.BOOKPAGEID,
                             b.ITEMNAME,
                             b.SPECIFICATION,
                             b.FK_UOM_ID,
                             b.ITEM_DESC,
                             b.ITEMCATEGORYCODE,
                             b.ENTRYDATE,
                             Column1 = b.ENTRYBY
                         }
                       ).AsEnumerable().Select(xs => new PRISM.Models.spConsumption
                       {
                           Dateid = xs.PK_DATEID,
                           FK_ASSET_ID = xs.FK_ITEMID,
                           ITEMNAME = xs.ITEMNAME,
                           allowedQty = xs.ALLOWEDQTY.ToString(),
                           consumpPeriod = xs.CONSUMPTIONPERIOD,
                           ConsumpTime = xs.CONSUMPTIONTIME,
                           itemDesc = xs.ITEM_DESC,
                       });
            var data = query.SingleOrDefault();
            ViewBag.TimeFrams = new SelectList(getDateTypes(), string.Empty);
            return PartialView("iDetailConsuptionP", data);
        }

        [SessionExpireFilter]
        public ActionResult UnitOfMeasure()
        {

            return PartialView("iUnitOfMeasure");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult AddUnitOfMeasure(PRISM.TBL_MEASURINGUNITS_MASTER ms)
        {
            ms.PK_UOM_ID = Guid.NewGuid().ToString();
            ms.ENTRYID = int.Parse(User.Identity.Name.ToString());
            ms.CREATEDDATE = DateTime.Now;
            try
            {
                db.TBL_MEASURINGUNITS_MASTER.Add(ms);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {

                var ex = e;
                return View();
            }

        }

        [SessionExpireFilter]
        public ActionResult ViewUnitOfMeasure()
        {
            var dat = new SettingsDbHelper();
            var data = dat.getMeasuringUnitDetail();
            return PartialView("iViewUnitOfMeasure", data);
        }

        public ActionResult EditUnitOfMeasure(string s)
        {
            var qid = new Guid(s);
            var q = db.TBL_MEASURINGUNITS_MASTER.Where(x => x.PK_UOM_ID == s).FirstOrDefault();
            return PartialView("iEditUnitOfMeasure", q);
        }

        public ActionResult EditVendor(string id)
        {
            var qid = new Guid(id);
            var q = db.TBL_VENDOR.Where(x => x.PK_VENDORID == id).FirstOrDefault();
            return PartialView("iEditVendor", q);
        }
        public ActionResult updateVendor(TBL_VENDOR ven)
        {
            var upd8 = db.TBL_VENDOR.Where(x => x.PK_VENDORID == ven.PK_VENDORID).FirstOrDefault();
            upd8.VENDORNAME = ven.VENDORNAME;
            upd8.ADDRESS = ven.ADDRESS;
            upd8.ADDRESS2 = ven.ADDRESS2;
            upd8.CONTACTPERSON = ven.CONTACTPERSON;
            upd8.PHONENO = ven.PHONENO;
            upd8.PHONENO1 = ven.PHONENO1;
            upd8.EMAIL = ven.EMAIL;
            upd8.FAXNO = ven.FAXNO;
            upd8.TERRETORY = ven.TERRETORY;
            upd8.COUNTRY = ven.COUNTRY;
            upd8.CURRENCYCODE = ven.CURRENCYCODE;
            upd8.BLOCKED = ven.BLOCKED;
            upd8.VATREGNO = ven.VATREGNO;
            db.Configuration.ValidateOnSaveEnabled = false;
            db.SaveChanges();
            return View("Index");
        }
        public ActionResult updateUnitOfMeasure(TBL_MEASURINGUNITS_MASTER mes)
        {
            var upd8 = db.TBL_MEASURINGUNITS_MASTER.Where(x => x.PK_UOM_ID == mes.PK_UOM_ID).FirstOrDefault();
            upd8.UNITNAME = mes.UNITNAME;
            upd8.UNITSHORT = mes.UNITSHORT;
            upd8.DESCRIPTION = mes.DESCRIPTION;
            db.Configuration.ValidateOnSaveEnabled = false;
            db.SaveChanges();
            return View("Index");
        }
        public ActionResult UpdateConsumptionP(spConsumption con)
        {
            var upd8 = db.TBL_CONSUMPTION_PERIOD.Where(x => x.PK_DATEID == con.Dateid).FirstOrDefault();
            upd8.CONSUMPTIONPERIOD = con.consumpPeriod;
            upd8.CONSUMPTIONTIME = con.ConsumpTime;
            upd8.ALLOWEDQTY = int.Parse(con.allowedQty);
            db.Configuration.ValidateOnSaveEnabled = false;
            db.SaveChanges();
            return View("Index");
        }

        public List<string> getDateTypes()
        {

            List<string> dt = new List<string>();
            dt.Add("Year");
            dt.Add("Month");
            dt.Add("Day");
            return dt;
        }

    }
}
