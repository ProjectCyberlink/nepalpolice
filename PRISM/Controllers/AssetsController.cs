﻿//using Microsoft.Reporting.WebForms;

using System.IO;
using Newtonsoft.Json;
using PRISM.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using PRISM.Models;

namespace PRISM.Controllers
{

    /// <summary>
    /// Assets Controller
    /// 
    /// Entry Type   Transaction Type   comments
    /// 0               0               new entry by PHQ
    /// 0               1               PHQ to Unit(this will new new entry for Unit)
    /// 1               1               PHQ to Personnel
    /// 1               0               Unit to Unit
    /// 2               2               Unit to Personnel
    /// </summary>

    [Authorize(Roles = "Admin")]
    [SessionExpireFilter]
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class AssetsController : PrismBaseController
    {
        private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
        //
        // GET: /TBL_ASSET/

        [SessionExpireFilter]
        public ActionResult Index(string itemName)
        {
            var unitMaster = int.Parse(this.Session["unitmaster"].ToString());
            string[] roles = Roles.GetRolesForUser();
            //if (roles.Contains("Admin") && unitMaster == 0)
            //{
            //    var invDat = getdbitems();
            //    return View(invDat);
            //}
            //else if (roles.Contains("Admin") && unitMaster != 0)
            //{
            //    var unitId = this.Session["UnitCode"].ToString();
            //    if (getcountforUnit(unitId))
            //    {
            //        var invData = getdbitemsFor(unitId);
            //        return View(invData);
            //    }
            //    else
            //    {
            //        return View();
            //    }
            //    //fix this

            //}
            //else
            //{
            //    return RedirectToAction("personalpaybook", "Account");
            //}
            if (PRISM.RoleRightHelper.HasUserRight("AddAssetMaster") || PRISM.RoleRightHelper.HasUserRight("AddAssetItem"))
            {
                var unitId = this.Session["UnitCode"].ToString();

                var invData = getdbitemsFor(unitId, itemName);
                return View(invData);
            }
            else
            {
                //return View();
                return RedirectToAction("personalpaybook", "Account");
            }

        }

        [SessionExpireFilter]
        public PartialViewResult CreateMaster()
        {
            //ViewBag.unitOMes = new SelectList(db.measuringUnits_master, "PK_UoM_ID", "unitName");
            ViewBag.UOM = db.TBL_MEASURINGUNITS_MASTER; //new SelectList(db.measuringUnits_master, "PK_UoM_ID", "unitName");


            ViewBag.HeadMessage = PRISM.Resource.MasterHeader; //"Create New Master for Assets";
            return PartialView("iCreateMaster");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult CreateM001(TBL_ASSET ast, FormCollection val)
        {
            //var unitofm = new Guid(val["unitOMes"]).ToString();
            ///need of injection is highly felt
            string ENTRYUNITid = this.Session["UnitCode"].ToString();
            TBL_ASSET astt = new TBL_ASSET
            {
                PK_ASSET_ID = Guid.NewGuid().ToString(),
                ITEM_DESC = ast.ITEM_DESC,
                BOOKPAGEID = ast.BOOKPAGEID,
                ITEMCATEGORYCODE = ast.ITEMCATEGORYCODE,
                ITEMNAME = ast.ITEMNAME,
                SPECIFICATION = ast.SPECIFICATION,
                FK_UOM_ID = ast.SelectedUnitId,//unitofm,
                ENTRYDATE = ast.ENTRYDATE,
                PURCHASEDDATE = ast.PURCHASEDDATE,
                ENTRYBY = int.Parse(User.Identity.Name),
                SelectedUnitId = ast.SelectedUnitId
            };
            try
            {
                db.TBL_ASSET.Add(astt);
                db.SaveChanges();
                return RedirectToAction("Index", getdbitems(ENTRYUNITid));
            }
            catch (Exception ex)
            {
                var e = ex;
                return View("Index", getdbitems(ENTRYUNITid));
            }
        }

        [SessionExpireFilter]
        public ActionResult MasterList()
        {
            //ViewBag.unitOMes = new SelectList(db.measuringUnits_master, "PK_UoM_ID", "unitName");
            ViewBag.UOM = db.TBL_MEASURINGUNITS_MASTER; //new SelectList(db.measuringUnits_master, "PK_UoM_ID", "unitName");


            ViewBag.HeadMessage = PRISM.Resource.MasterList; //"Create New Master for Assets";
            return View(db.TBL_ASSET.OrderByDescending(a=>a.ENTRYDATE).ToList());
        }

        [SessionExpireFilter]
        public PartialViewResult EditMaster(string Id)
        {
            //ViewBag.unitOMes = new SelectList(db.measuringUnits_master, "PK_UoM_ID", "unitName");
            ViewBag.UOM = db.TBL_MEASURINGUNITS_MASTER; //new SelectList(db.measuringUnits_master, "PK_UoM_ID", "unitName");


            ViewBag.HeadMessage = PRISM.Resource.MasterHeader; //"Create New Master for Assets";

            return PartialView("iEditMaster", db.TBL_ASSET.First(a => a.PK_ASSET_ID == Id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult EditMaster(TBL_ASSET ast, FormCollection val)
        {
            //var unitofm = new Guid(val["unitOMes"]).ToString();
            ///need of injection is highly felt
            TBL_ASSET astt = db.TBL_ASSET.Find(ast.PK_ASSET_ID);
                astt.ITEM_DESC = ast.ITEM_DESC;
                astt.BOOKPAGEID = ast.BOOKPAGEID;
                astt.ITEMCATEGORYCODE = ast.ITEMCATEGORYCODE;
                astt.ITEMNAME = ast.ITEMNAME;
                astt.SPECIFICATION = ast.SPECIFICATION;
                astt.FK_UOM_ID = ast.FK_UOM_ID;//unitofm,
                astt.SelectedUnitId = ast.SelectedUnitId;
            
            try
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                var e = ex;
            }
            return RedirectToAction("MasterList");
        }

        public JsonResult DoesItemNameExists(string ItemName, string PK_ASSET_ID)
        {
            var doesExists = db.TBL_ASSET.Any(item => item.ITEMNAME == ItemName && (string.IsNullOrEmpty(PK_ASSET_ID) || item.PK_ASSET_ID != PK_ASSET_ID));
            return Json(!doesExists, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public PartialViewResult CreateALE()
        {
            //ViewBag.AssetHead = new SelectList(db.Assets, "Guid", "ItemName");
            ViewBag.Assets = db.TBL_ASSET;
            ViewBag.Vendors = db.TBL_VENDOR.Where(x => x.BLOCKED == 0);
            //ViewBag.vendorColxn = new SelectList(db.tbl_Vendor.Where(x => x.Blocked == 0), "PK_VendorId", "VendorName");
            ViewBag.HeadMessage = PRISM.Resource.AddStockHeader; //"Register New Item In Inventory";
            return PartialView("iCreateALE");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult CreateM002(TBL_ASSETLEDGER asl, FormCollection value)
        {
            string ENTRYBYid = User.Identity.Name;
            //Guid iguid = new Guid(value["TBL_ASSETHead"]);
            //Guid _vendorId = new Guid(value["vendorColxn"]);
            string unitid = this.Session["UnitCode"].ToString();
            string ENTRYUNITid = this.Session["UnitCode"].ToString();
            TBL_ASSETLEDGER alsr = new TBL_ASSETLEDGER
            {
                PK_ALE_ID = Guid.NewGuid().ToString(),
                FK_ASSET_ID = asl.SelectedItemId, //iguid,
                POSTINGDATE = asl.POSTINGDATE,// DateTime.Now,
                ENTRYTYPE = 0,
                ENTRYFOR = unitid,
                DESCRIPTION = asl.DESCRIPTION,
                QUANTITY = asl.QUANTITY,
                INVOICEDQUANTITY = asl.INVOICEDQUANTITY,
                TRANSACTIONTYPE = 0,
                VAT = asl.VAT,
                PRICEINCLUDEVAT = 1,
                UNITPRICE = asl.UNITPRICE,
                ITEMCATEGORYCODE = asl.ITEMCATEGORYCODE,
                OPEN = 1,
                EXTRAEXPENSES = asl.EXTRAEXPENSES,
                TOTALEXPENSES = asl.TOTALEXPENSES,
                COMMENTS = asl.COMMENTS,
                ENTRYBY = ENTRYBYid,
                ENTRYUNIT = ENTRYUNITid,
                ENTRYDATE = DateTime.Now,
                LASTEDITDAT = DateTime.Now,
                WAIVED = 0,
                FK_VENDORID = asl.SelectedVendorId,
                SelectedVendorId = asl.SelectedVendorId,
                SelectedItemId = asl.SelectedItemId
            };
            try
            {
                db.TBL_ASSETLEDGER.Add(alsr);
                db.SaveChanges();
                return View("Index", getdbitems(ENTRYUNITid));
            }
            catch (Exception e)
            {
                var ex = e;
                return View("Index", getdbitems(ENTRYUNITid));
            }
        }
        #region SameerEdit
        /// <summary>
        /// Multiple Data Entry By Sameer
        /// </summary>
        /// <param name="disposing"></param>
        /// 
        [SessionExpireFilter]
        public PartialViewResult SingleStock(string formId)
        {
            ViewBag.Assets = db.TBL_ASSET;
            ViewBag.Vendors = db.TBL_VENDOR.Where(x => x.BLOCKED == 0);
            string unitid = this.Session["UnitCode"].ToString();
            return PartialView("_SingleStock", new TBL_ASSETLEDGER() { ENTRYFOR = unitid });
        }

        [SessionExpireFilter]
        public ActionResult MultipleStocks()
        {
            ViewBag.Assets = db.TBL_ASSET;
            ViewBag.Vendors = db.TBL_VENDOR.Where(x => x.BLOCKED == 0);
            string unitid = this.Session["UnitCode"].ToString();
            return View(new List<TBL_ASSETLEDGER>() { new TBL_ASSETLEDGER() { ENTRYFOR = unitid } }.AsEnumerable());
        }

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult MultipleStocks(IEnumerable<TBL_ASSETLEDGER> assets)
        {
            if (ModelState.IsValid)
            {
                string ENTRYBYid = User.Identity.Name;
                //Guid iguid = new Guid(value["TBL_ASSETHead"]);
                //Guid _vendorId = new Guid(value["vendorColxn"]);
                string unitid = this.Session["UnitCode"].ToString();
                string ENTRYUNITid = this.Session["UnitCode"].ToString();
                foreach (var asl in assets)
                {
                    TBL_ASSETLEDGER alsr = new TBL_ASSETLEDGER
                    {
                        PK_ALE_ID = Guid.NewGuid().ToString(),
                        FK_ASSET_ID = asl.SelectedItemId, //iguid,
                        POSTINGDATE = DateTime.Now,
                        ENTRYTYPE = 0,
                        ENTRYFOR = unitid,
                        DESCRIPTION = asl.DESCRIPTION,
                        QUANTITY = asl.QUANTITY,
                        INVOICEDQUANTITY = asl.INVOICEDQUANTITY,
                        TRANSACTIONTYPE = 0,
                        VAT = asl.VAT,
                        PRICEINCLUDEVAT = 1,
                        UNITPRICE = asl.UNITPRICE,
                        ITEMCATEGORYCODE = asl.ITEMCATEGORYCODE,
                        OPEN = 1,
                        EXTRAEXPENSES = asl.EXTRAEXPENSES,
                        TOTALEXPENSES = asl.TOTALEXPENSES,
                        COMMENTS = asl.COMMENTS,
                        ENTRYBY = ENTRYBYid,
                        ENTRYUNIT = ENTRYUNITid,
                        ENTRYDATE = DateTime.Now,
                        LASTEDITDAT = DateTime.Now,
                        WAIVED = 0,
                        FK_VENDORID = asl.SelectedVendorId,
                        SelectedVendorId = asl.SelectedVendorId,
                        SelectedItemId = asl.SelectedItemId
                    };
                    db.TBL_ASSETLEDGER.Add(alsr);
                }
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    var ex = e;
                }
                return RedirectToAction("Index", getdbitems(ENTRYUNITid));
            }
            ViewBag.Assets = db.TBL_ASSET;
            ViewBag.Vendors = db.TBL_VENDOR.Where(x => x.BLOCKED == 0);
            return View(assets);
        }
        #endregion
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [SessionExpireFilter]
        public ActionResult ItemDetails(string id)
        {
            var _person = this.Session["person"].ToString();
            string _unitcod = this.Session["UnitCode"].ToString();
            var unitMaster = this.Session["unitmaster"].ToString();
            string[] roles = Roles.GetRolesForUser();

            var idata = (from alx in db.TBL_ASSETLEDGER
                         join amx in db.TBL_ASSET on new { itid = alx.FK_ASSET_ID } equals new { itid = amx.PK_ASSET_ID }
                         join umx in db.TBL_MEASURINGUNITS_MASTER on new { muid = amx.FK_UOM_ID } equals new { muid = umx.PK_UOM_ID }
                         join pex in db.VIEW_AUTH on new { pid = alx.ENTRYBY } equals new { pid = pex.PERSON_CODE }
                         where
                         alx.FK_ASSET_ID == id
                         && (alx.ENTRYUNIT == _unitcod
                        || alx.ENTRYFOR == _unitcod)

                         select new
                         {
                             id = alx.PK_ALE_ID,
                             Name = amx.ITEMNAME,
                             PageID = amx.BOOKPAGEID,
                             Description = amx.ITEM_DESC,
                             Spec = amx.SPECIFICATION,
                             purchasedate = amx.PURCHASEDDATE,
                             UoM = umx.UNITNAME,
                             EType = alx.ENTRYTYPE,
                             //EType = (alx.ENTRYTYPE == null ? " " : (alx.ENTRYTYPE == 0 ? "New Entry" : "Transferred")),
                             TType = alx.TRANSACTIONTYPE,
                             Posted_Date = alx.POSTINGDATE,
                             QTY = (alx.ENTRYUNIT == _unitcod ? alx.QUANTITY : (alx.ENTRYFOR == _unitcod ? Math.Abs(alx.QUANTITY) : alx.QUANTITY)),
                             InvQty = alx.INVOICEDQUANTITY,
                             VAT = alx.VAT,
                             IncVAT = alx.PRICEINCLUDEVAT,
                             UnitPrice = alx.UNITPRICE,
                             Code = alx.ITEMCATEGORYCODE,
                             Open = alx.OPEN,
                             ExtraExp = alx.EXTRAEXPENSES,
                             Expense = alx.TOTALEXPENSES,
                             //Vendor = vix.VENDORNAME,
                             waive = alx.WAIVED,
                             waiveReason = alx.WAIVEDREASON,
                             EntryFor = alx.ENTRYFOR,
                             EntryBy = alx.ENTRYBY,
                             EntryByN = pex.NAME,
                             EntryFrom = alx.ENTRYUNIT,
                             comment = alx.COMMENTS
                             
                         }
).ToList();



            if (idata == null)
            {
                return HttpNotFound();
            }
            ViewBag.AssetList = idata;
            //return View(idata);
            return View("AssetDetail");
        }


        [HttpPost]
        public virtual ActionResult UpdateAsset()
        {
            try
            {
                var resolveRequest = HttpContext.Request;
                resolveRequest.InputStream.Seek(0, System.IO.SeekOrigin.Begin);
                string jsons = new StreamReader(resolveRequest.InputStream).ReadToEnd();
                if (jsons != null)
                {
                    var ToSave = JsonConvert.DeserializeObject<AssetToJson>(jsons);
                    var AssetToUpdate = db.TBL_ASSETLEDGER.Find(ToSave.id);
                    AssetToUpdate.OPEN = decimal.Parse("1");
                    AssetToUpdate.WAIVED = int.Parse(ToSave.waive);
                    AssetToUpdate.WAIVEDREASON = ToSave.waiveReason;
                    AssetToUpdate.COMMENTS = ToSave.comment;
                    AssetToUpdate.LASTEDITDAT = DateTime.Now;                   
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return View("AssetDetail");
        }
        [SessionExpireFilter]
        public ActionResult ViewMasterLine(string vtype)
        {
            if (vtype == "Master")
            {
                var queryresult = db.TBL_ASSET.ToList();
                return View("Edit/AssetMaster", queryresult);
            }
            else if (vtype == "Line")
            {
                var queryresult = db.TBL_ASSETLEDGER.ToList();
                return View("Edit/AssetLine", queryresult);
            }
            else
            {
                return View();
            }
        }

        [SessionExpireFilter]
        public PartialViewResult iDetail(string id)
        {
           // int _ENTRYFOR = 0, _transType = 0;
            //var condition = (from asl in db.TBL_ASSETLEDGER where asl.PK_ALE_ID == id select new { asl.ENTRYTYPE, asl.TRANSACTIONTYPE }).ToList().Take(1);
            //var condition = (from asl in db.TBL_ASSETLEDGER where asl.PK_ALE_ID == id select new { asl.ENTRYTYPE, asl.TRANSACTIONTYPE }).ToList();
            //foreach (var item in condition)
            //{
            //    _ENTRYFOR = int.Parse (item.ENTRYTYPE.ToString());
            //    _transType = int.Parse(item.TRANSACTIONTYPE.ToString());
            //}


            //if ((_ENTRYFOR == 1 && _transType == 1) || (_ENTRYFOR == 2 && _transType == 2))
            //{
            //        var data = (from a in db.TBL_ASSETLEDGER
            //                    join b in db.TBL_ASSET on new { FK_ASSET_ID = a.FK_ASSET_ID } equals new { FK_ASSET_ID = b.PK_ASSET_ID }
            //                    join c in db.TBL_MEASURINGUNITS_MASTER on new { UOM = b.FK_UOM_ID } equals new { UOM = c.PK_UOM_ID }
            //                    join d in
            //                        (
            //                            (from Auth in db.VIEW_AUTH
            //                             select new
            //                             {
            //                                 Auth.PERSON_CODE,
            //                                 Auth.NAME
            //                             })) on new { ENTRYFOR = a.ENTRYFOR } equals new { ENTRYFOR =  d.PERSON_CODE.ToString() }
            //                    where
            //                      a.PK_ALE_ID == id
            //                    select new
            //                    {
            //                        a.PK_ALE_ID,
            //                        FK_ASSET_ID = a.FK_ASSET_ID,
            //                        a.POSTINGDATE,
            //                        a.ENTRYTYPE,
            //                        a.DESCRIPTION,
            //                        a.QUANTITY,
            //                        a.INVOICEDQUANTITY,
            //                        a.VAT,
            //                        a.PRICEINCLUDEVAT,
            //                        a.UNITPRICE,
            //                        a.ITEMCATEGORYCODE,
            //                        a.ITEMTRACKINGCODE,
            //                        a.EXTRAEXPENSES,
            //                        a.TOTALEXPENSES,
            //                        a.COMMENTS,
            //                        a.WAIVED,
            //                        a.WAIVEDREASON,
            //                        b.BOOKPAGEID,
            //                        b.ITEMNAME,
            //                        b.SPECIFICATION,
            //                        b.ITEM_DESC,
            //                        c.UNITSHORT,
            //                        d.NAME,
            //                        d.PERSON_CODE
            //                    }).AsEnumerable().Select

            //                        (x => new PRISM.Models.spTBL_ASSETDetails
            //                        {
            //                            ALE = x.PK_ALE_ID.ToString(),
            //                            FK_ASSET_ID = x.FK_ASSET_ID.ToString(),
            //                            POSTINGDATE = x.POSTINGDATE.ToString(),
            //                            ENTRYTYPE = (x.ENTRYTYPE == null ? " " : (x.ENTRYTYPE == 0 ? "New Entry" : "Transferred")),
            //                            DESCRIPTION = x.DESCRIPTION,
            //                            QUANTITY = x.QUANTITY.ToString(),
            //                            INVOICEDQUANTITY = x.INVOICEDQUANTITY.ToString(),
            //                            VAT = x.VAT.ToString(),
            //                            PRICEINCLUDEVAT = (x.PRICEINCLUDEVAT == 0 ? "NO" : "YES"),
            //                            UNITPRICE = x.UNITPRICE.ToString(),
            //                            ITEMCATEGORYCODE = (x.ITEMCATEGORYCODE == 52 ? "less than year" : "more than a year"),
            //                            ITEMTRACKINGCODE = (x.ITEMTRACKINGCODE == null ? " " : (x.ITEMTRACKINGCODE.ToString())),
            //                            EXTRAEXPENSES = x.EXTRAEXPENSES.ToString(),
            //                            TOTALEXPENSES = x.TOTALEXPENSES.ToString(),
            //                            COMMENTS = (x.COMMENTS == null ? " " : (x.COMMENTS)),
            //                            BOOKPAGEID = x.BOOKPAGEID.ToString(),
            //                            ITEMNAME = x.ITEMNAME,
            //                            SPECIFICATION = x.SPECIFICATION,
            //                            item_Desc = x.ITEM_DESC,
            //                            UNITSHORT = x.UNITSHORT,
            //                            PERSON_CODE = x.PERSON_CODE.ToString(),
            //                            WAIVED = x.WAIVED.ToString(),
            //                            WAIVEDReason = (x.WAIVEDREASON == null ? " " : (x.COMMENTS)),
            //                            UNIT_NAME = x.NAME
            //                        }).ToList();
            //        return PartialView("iItemDetail", data.SingleOrDefault());
            //    //}

            //}
            //else
            //{
            //    /*Review This 07*/
            //    var data = (
            //        from a in db.TBL_ASSETLEDGER
            //        join b in db.TBL_ASSET on new { FK_ASSET_ID = a.FK_ASSET_ID } equals new { FK_ASSET_ID = b.PK_ASSET_ID }
            //        join c in db.TBL_MEASURINGUNITS_MASTER on new { UOM = b.FK_UOM_ID } equals new { UOM = c.PK_UOM_ID }
            //        join d in db.VIEW_AUTH on new { ENTRYFOR = a.ENTRYFOR.ToString() } equals new { ENTRYFOR = d.ID }
            //        where
            //          a.PK_ALE_ID == id
            //        select new
            //        {
            //            a.PK_ALE_ID,
            //            FK_ASSET_ID = a.FK_ASSET_ID,
            //            a.POSTINGDATE,
            //            a.ENTRYTYPE,
            //            a.DESCRIPTION,
            //            a.QUANTITY,
            //            a.INVOICEDQUANTITY,
            //            a.VAT,
            //            a.PRICEINCLUDEVAT,
            //            a.UNITPRICE,
            //            a.ITEMCATEGORYCODE,
            //            a.ITEMTRACKINGCODE,
            //            a.EXTRAEXPENSES,
            //            a.TOTALEXPENSES,
            //            a.COMMENTS,
            //            b.BOOKPAGEID,
            //            b.ITEMNAME,
            //            b.SPECIFICATION,
            //            b.ITEM_DESC,
            //            c.UNITSHORT,
            //            a.WAIVED,
            //            a.WAIVEDREASON,
            //            d.UNIT_NAME,
            //            d.PERSON_CODE
            //        }).AsEnumerable().Select

            //    (x => new PRISM.Models.spTBL_ASSETDetails
            //{
            //    ALE = x.PK_ALE_ID,
            //    FK_ASSET_ID = x.FK_ASSET_ID,
            //    POSTINGDATE = x.POSTINGDATE.ToString(),
            //    ENTRYTYPE = (x.ENTRYTYPE == null ? " " : (x.ENTRYTYPE == 0 ? "New Entry" : "Transferred")),
            //    DESCRIPTION = x.DESCRIPTION,
            //    QUANTITY = x.QUANTITY.ToString(),
            //    INVOICEDQUANTITY = x.INVOICEDQUANTITY.ToString(),
            //    VAT = x.VAT.ToString(),
            //    PRICEINCLUDEVAT = (x.PRICEINCLUDEVAT == 0 ? "NO" : "YES"),
            //    UNITPRICE = x.UNITPRICE.ToString(),
            //    ITEMCATEGORYCODE = (x.ITEMCATEGORYCODE == 52 ? "less than year" : "more than a year"),
            //    ITEMTRACKINGCODE = (x.ITEMTRACKINGCODE == null ? " " : (x.ITEMTRACKINGCODE.ToString())),
            //    EXTRAEXPENSES = x.EXTRAEXPENSES.ToString(),
            //    TOTALEXPENSES = x.TOTALEXPENSES,
            //    COMMENTS = (x.COMMENTS == null ? " " : (x.COMMENTS)),
            //    BOOKPAGEID = x.BOOKPAGEID.ToString(),
            //    ITEMNAME = x.ITEMNAME,
            //    SPECIFICATION = x.SPECIFICATION,
            //    item_Desc = x.ITEM_DESC,
            //    UNITSHORT = x.UNITSHORT,
            //    PERSON_CODE = x.PERSON_CODE,
            //    WAIVED = x.WAIVED.ToString(),
            //    WAIVEDReason = (x.WAIVEDREASON == null ? " " : (x.COMMENTS)),
            //    UNIT_NAME = x.UNIT_NAME
            //}).ToList();

            //    return PartialView("iItemDetail", data.SingleOrDefault());
            //}

            //ViewBag.data2 = (from x in db.TBL_ASSET where x.guid == new Guid(data.FK_ASSET_ID) select x).Single();
            Models.spAssetsDetails spAD = new Models.spAssetsDetails();
            var _unitcod = this.Session["UnitCode"].ToString();
            var unitMaster = this.Session["unitmaster"].ToString();
            var dat = new DbHelper();
            var dt = dat.GetItemSpecificDetail(id, _unitcod, unitMaster); ;
            return PartialView("iItemDetail", dt);

        }

        public IEnumerable<PRISM.Models.spOnHand> getdbitems(string ENTRYUNITid)
        {
            IEnumerable<PRISM.Models.spOnHand> lst = (
            from a in db.TBL_ASSET
            join b in
                (
                    (from al in db.TBL_ASSETLEDGER.Where(x=>x.ENTRYUNIT==ENTRYUNITid)
                     group al by new
                     {
                         al.FK_ASSET_ID,
                     } into g
                     select new
                     {
                         g.Key.FK_ASSET_ID,
                         qty = (decimal?)g.Sum(p => p.QUANTITY)
                     })) on new { guid = a.PK_ASSET_ID } equals new { guid = b.FK_ASSET_ID }
                     //join c in db.TBL_MEASURINGUNITS_MASTER on new { UOM = a.FK_UOM_ID } equals new { UOM = c.PK_UOM_ID }
            select new
            {
                a.PK_ASSET_ID,
                a.ITEMNAME,
                b.qty,
                a.SPECIFICATION,
                a.ITEM_DESC,
                a.PURCHASEDDATE
            }).AsEnumerable().Select(n => new PRISM.Models.spOnHand
            {
                ale = n.PK_ASSET_ID.ToString(),
                ITEMNAME = n.ITEMNAME,
                qty = decimal.Parse(n.qty.ToString()),
                SPECIFICATION = n.SPECIFICATION,
                item_Desc = n.ITEM_DESC,
                // = n.PURCHASEDDATE
            }).ToList();

            return lst;
        }
        public IEnumerable<PRISM.Models.spOnHand> getdbitemsFor(string id, string itemNameFilter)
        {
            string _entryId = id;
            string query = @"select ast.pk_asset_id ale, ast.ITEMNAME, ast.SPECIFICATION, ast.ITEM_DESC item_desc, nvl(stock.on_hand,0) qty,ast.PURCHASEDDATE FROM
tbl_asset ast
left join
(select a.fk_asset_id, (a.added - nvl(b.deducted,0)) as on_hand from
(select fk_asset_id, Sum(ABS(quantity)) added from tbl_assetledger where entryfor = '" + _entryId + @"' and waived = 0 and open=1 group by fk_asset_id) a
left join
(select fk_asset_id, Sum(ABS(quantity)) deducted from tbl_assetledger where entryfor != '" + _entryId + "' and entryunit = '" + _entryId + @"' and waived = 0 and open=1 group by fk_asset_id) b on
a.fk_asset_id = b.fk_asset_id) stock
on ast.pk_asset_id = stock.fk_asset_id";
            if (!string.IsNullOrWhiteSpace(itemNameFilter))
            {
                query += " where upper(ast.ITEMNAME) like '%" + itemNameFilter.ToUpper() + "%'";
            }
            IEnumerable<spOnHand> lst = Enumerable.Empty<spOnHand>();
            if (_entryId!="PHQ")
            {
                 lst = db.Database.SqlQuery<PRISM.Models.spOnHand>(query).ToList<PRISM.Models.spOnHand>().Where(x => x.qty != 0);
            }
            else
            {
                lst = db.Database.SqlQuery<PRISM.Models.spOnHand>(query).ToList<PRISM.Models.spOnHand>();
            }

            return lst;
        }

        //public ActionResult TBL_ASSETReport()
        //{
        //    var data = from a in db.TBL_POLICE_UNIT 
        //               join b in db.TBL_STATES 
        //               on new { STATECODE = (Guid)a.STATECODE } equals new { STATECODE = b.STATEGUID } 
        //               join c in db.TBL_REGION 
        //               on new { sRegion = b.STATEREGION } equals new { sRegion = c.REGIONGUID} 
        //               select new { a.PK_POLICEUNIT_ID, UnitName = (a.UNIT_NAME + "," + b.STATE + "," + c.REGIONNAME) };
        //    var reoprtTypes = getReportType();
        //    ViewBag.TBL_ASSETHead = new SelectList(db.TBL_ASSET, "Guid", "ITEMNAME");
        //    ViewBag.ForUnit = new SelectList(data, "Id", "UnitName");
        //    ViewBag.RType = new SelectList(reoprtTypes, string.Empty);
        //    return View();
        //}

        /* [HttpPost]
         [ValidateAntiForgeryToken]
         [SessionExpireFilter]
         public PartialViewResult ENTRYFORmReport(FormCollection val)
         {
             var forUnit = val["ForUnit"];
             var TBL_ASSETHead = val["TBL_ASSETHead"];
             var fromDate = (val["fromDate"]).Replace(".", "/");
             var toDate = (val["toDate"]).Replace(".", "/");

             ViewBag.ftype = BaseLXIVHelper.base64Encode(forUnit);
             ViewBag.atype = BaseLXIVHelper.base64Encode(TBL_ASSETHead);
             if (val["RType"] == "दाखिला प्रतिबेदन")
             {
                 var data = (from xs in db.ENTRYFORms select xs).AsEnumerable();
                 return PartialView("Reports/iENTRYFORmReport", data);
             } if (val["RType"] == "खप्ने जिन्सी समान को प्रतिबेदन")
             {
                 throw new NotImplementedException();
             } if (val["RType"] == "जिन्सी निरिक्छेन")
             {
                 throw new NotImplementedException();
                 //    var data = (from xs in db.ENTRYFORms select xs).AsEnumerable();
                 //    return PartialView("Reports/iENTRYFORmReport", data);
             } if (val["RType"] == "खर्च भएर जाने जिन्सी समान को खाता")
             {
                 //var data = (from xs in db.ENTRYFORms select xs).AsEnumerable();
                 //return PartialView("Reports/iENTRYFORmReport", data);
                 throw new NotImplementedException();
             }
             if (val["RType"] == "हस्तान्तरण")
             {
                 var ffdate = Convert.ToDateTime(fromDate
                     //, "yyyy-MM-dd",
                     //System.Globalization.CultureInfo.InvariantCulture
                                 );
                 var ttdate = Convert.ToDateTime(toDate);
                 //              , "yyyy-MM-dd",System.Globalization.CultureInfo.InvariantCulture);
                 var datafor = int.Parse(forUnit);
                 var data = (from xs in db.handoverForms
                             //where xs.POSTINGDATE >= ffdate && xs.POSTINGDATE <= ttdate
                             //&& xs.ENTRYFOR == datafor
                             select xs).AsEnumerable();
                 return PartialView("Reports/iHandOverFormReport", data);
             }

             //remove this for default
             return PartialView("Reports/iENTRYFORmReport");
         } */

        public List<string> getReportType()
        {
            List<string> st = new List<string>();
            st.Add("दाखिला प्रतिबेदन");
            //st.Add("खप्ने जिन्सी समान को प्रतिबेदन");
            //st.Add("जिन्सी निरिक्छेन");
            st.Add("हस्तान्तरण");
            //st.Add("निसर्ग/मिनाह फारम");
            st.Add("खर्च भएर जाने जिन्सी समान को खाता");
            return st;
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]

        /*public FileResult ShowOnExcel(string Type, string ftype, string atype)
        {
            LocalReport Lr = new LocalReport();
            ReportDataSource repDs = new ReportDataSource();
            //var rptType = BaseLXIVHelper.base64Decode(ftype);
            //change this param
            var _ftypes = int.Parse(BaseLXIVHelper.base64Decode(ftype));
            var _atypes = new Guid(BaseLXIVHelper.base64Decode(atype));
            //string query =""
            //repDs.Value = (from xs in db.ENTRYFORms select xs).ToList();
            if (Type == "Dakhila")
            {
                Lr.ReportPath = Server.MapPath("~/Reports/EntryFromReport.rdlc");
                repDs.Name = "ENTRYFORmReport";
                repDs.Value = (from a in db.ENTRYFORms join b in ((from ef in db.ENTRYFORms where ef.QUANTITY > 0 select new { ef.ALE, ef.ITEMNAME, ef.QUANTITY })) on a.ALE equals b.ALE select new { a.BOOKPAGEID, a.ITEMCATEGORYCODE, ALE = (Guid?)a.ALE, a.ITEMNAME, a.SPECIFICATION, a.UNITSHORT, b.QUANTITY, a.VAT, a.UNITPRICE, a.EXTRAEXPENSES, a.TOTALEXPENSES, a.COMMENTS, a.ENTRYFOR, a.ENTRYDATE }).ToList();
            }
            if (Type == "Hastantaran")
            {
                Lr.ReportPath = Server.MapPath("~/Reports/HandoverRpt.rdlc");
                repDs.Name = "HandOverRptData";
                //repDs.Value = (from a in db.handoverForms 
                //where a.ENTRYFOR == _ftypes && a.FK_ASSET_ID == _atypes
                //                 select a).ToList();
                repDs.Value = (from a in db.handoverForms select new { a.BOOKPAGEID, a.ITEMCATEGORYCODE, a.ITEMNAME, a.SPECIFICATION, a.Qty, a.EXTRAEXPENSES, a.TOTALEXPENSES, a.POSTINGDATE }).ToList();
            }

            //change this param
            Lr.DataSources.Add(repDs);

            string reportType = "Excel";
            string mimeType;
            string encoding;
            string fileNameExtension = "xlsx";
            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = Lr.Render(reportType, null, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
            Response.AddHeader("content-disposition", "attachement;filename=" + Type + "_" + DateTime.Now.Year + "." + fileNameExtension);

            return File(renderedBytes, fileNameExtension);
        } */
        public bool getcountforUnit(string id)
        {
            var _id = id;
            var testdata = (
                //from x in db.TBL_ASSETLEDGER
                //                where
                //                  x.ENTRYTYPE == 1 &&
                //                  (x.TRANSACTIONTYPE == 1 ||
                //                  x.TRANSACTIONTYPE == 2)
                //                group x by new
                //                {
                //                    x.FK_ASSET_ID
                //                } into g
                //                select new
                //                {
                //                    Transferred = (Decimal?)g.Sum(p => p.QUANTITY),
                //                    FK_ASSET_ID = (Guid?)g.Key.FK_ASSET_ID
                //                }
            from x in db.TBL_ASSETLEDGER
            where
            x.ENTRYFOR == _id &&
              ((x.ENTRYTYPE == 0 &&
              x.TRANSACTIONTYPE == 1) ||
             (x.ENTRYTYPE == 1 && x.TRANSACTIONTYPE == 0))
            group x by new
            {
                x.FK_ASSET_ID
            } into g
            select new
            {
                Transferred = (Decimal?)g.Sum(p => p.QUANTITY),
                FK_ASSET_ID = g.Key.FK_ASSET_ID
            }).Count();

            if (testdata != null && testdata != 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}