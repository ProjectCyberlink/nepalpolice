﻿using PRISM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRISM.Controllers
{
    [SessionExpireFilter]
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class ChangePasswordController : PrismBaseController
    {
        
        private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult Index(ChangePassword model, FormCollection collection)
        {
            string oldPassword = collection["OldPassword"];
            string newPassword = collection["NewPassword"];
            string confirmPassword = collection["ConfirmPassword"];
            string personCode=Session["PersonCode"].ToString();

            if (newPassword != confirmPassword)
            {
                ModelState.AddModelError("", "Your new password doesn't match with confirm password.");
                return View(model);
            }
           var dbUpdate= db.TBL_USER_DETAILS.Where(user=>user.PERSON_CODE==personCode).SingleOrDefault();
           try
           {
               dbUpdate.PASSWORD = newPassword;
               db.SaveChanges();
               ViewData["SuccessMessage"] = "Password successfully changed.";
           }
           catch (Exception ex)
           {
               //ViewData["ErrorMessage"] = "Error occurred when updating password.";
               ModelState.AddModelError("", "Error occurred when updating password.");
           }
           
            return View();
        }

        [SessionExpireFilter]
        public JsonResult DoesOldPasswordMatch(string OldPassword)
        {
            string personCode= Session["PersonCode"].ToString(),
                query = @"select 
                                                                    b.USER_ID, 
                                                                    b.IS_ACTIVE, 
                                                                    b.PERSON_CODE,
                                                                    b.PASSWORD,    
                                                                    b.IS_ACTIVE,
                                                                    b.ACTIVATED_ON,
                                                                    b.LAST_LOGGED_ON,
                                                                    b.LAST_UPDATED_ON,
                                                                    b.LAST_UPDATED_BY,
                                                                    b.FK_ROLEID 
                                                                    from 
                                view_auth a left join tbl_user_details b on a.person_code=b.person_code where ((b.PASSWORD='" + OldPassword + "' and IS_ACTIVE=1) or ('" + personCode + "'='" + OldPassword + "' and IS_ACTIVE=0))";
           
            List<TBL_USER_DETAILS> userDetails = db.Database.SqlQuery<TBL_USER_DETAILS>(query).ToList();
            //var doesExists = db.TBL_USER_DETAILS.Where((user=> user.PASSWORD == OldPassword && user.IS_ACTIVE==1) || (usr=>usr.Person_code==OldPassword && usr.IS_ACTIVE==0) );
            var doesMatch = userDetails.Count() > 0 ? true : false;
            return Json(doesMatch, JsonRequestBehavior.AllowGet);
        }

    }
}
