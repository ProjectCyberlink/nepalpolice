﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;

namespace PRISM.Controllers
{
    [SessionExpireFilter]
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class ReportsController : Controller
    {
        //
        // GET: /Reports/

        public PartialViewResult Report(FormCollection val)
        {
            ViewBag._Type = val["ReportType"];
            ViewBag._For = val["ReportFor"];
            ViewBag._WAitems = val["WAitems"];
            ViewBag._Date1 = val["StartDate"];
            ViewBag._Date2 = val["EndDate"];
            ViewBag._unitOrPerson = val["unitorperson"];
            ViewBag._items = val["Items"];
            string entry_unit_org = this.Session["UnitCode"].ToString();
            string ENTRYUNITid = this.Session["PersonCode"].ToString();
            string orgName = "Police HQ";
            bool dateIsValid = true;
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.DataSources.Clear();
            using (var db = new PRISM.NepalPoliceInventoryEntities())
            {
                if (val["WAitems"] == "Asset")
                {
                    if (val["ReportType"] == "दाखिला प्रतिबेदन(४६)")
                    {

                        IQueryable<PRISM.ASSET_REPORTTYPE_46> reportData = null;
                        if (entry_unit_org == "PHQ")
                        {
                            reportData = db.ASSET_REPORTTYPE_46.Where(x => ((x.TRANSACTIONTYPE == 0) && (x.ENTRYTYPE == 0))).OrderByDescending(x => x.ENTRYDATE);

                        }
                        else
                        {
                            reportData = db.ASSET_REPORTTYPE_46.Where(x => ((x.TRANSACTIONTYPE == 0) && (x.ENTRYTYPE == 0) && (x.ENTRYFOR == entry_unit_org))).OrderByDescending(x => x.ENTRYDATE);
                        }

                        foreach (var item in reportData)
                        {
                            orgName = db.ORGHs.Where(x => x.ORG_STRUCTURE_CODE == item.ENTRYFOR).Select(y => y.ORG_STRUCTURE_NAME).First();
                            item.ENTRYFOR = orgName;
                            
                        }
                        reportViewer.LocalReport.ReportPath = Server.MapPath("~/rdlc/AssetReport_type46.rdlc");
                        ReportDataSource rds = new ReportDataSource("AssetReportType46", reportData.ToList());
                        reportViewer.LocalReport.DataSources.Add(rds);
                    }
                    else if (val["ReportType"] == "हस्तान्तरण(४८)")
                    {
                        //IQueryable<PRISM.ASSET_REPORTTYPE_48> reportData = null;
                        IQueryable<PRISM.TBL_ASSETLEDGER> reportData = null;
                        if (val["ReportFor"] == "Unit")
                        {
                            if (entry_unit_org == "PHQ")
                            {
                                reportData = db.TBL_ASSETLEDGER.Where(
                                                                    x =>
                                                                    (x.TRANSACTIONTYPE == 0 && x.ENTRYTYPE == 1) ||
                                                                    (x.TRANSACTIONTYPE == 1 && x.ENTRYTYPE == 0)).OrderByDescending(x => x.POSTINGDATE);
                            }
                            else
                            {
                                reportData = db.TBL_ASSETLEDGER.Where(
                                                                    x =>
                                                                    (x.TRANSACTIONTYPE == 0 && x.ENTRYTYPE == 1 && x.ENTRYFOR == entry_unit_org) ||
                                                                    (x.TRANSACTIONTYPE == 1 && x.ENTRYTYPE == 0 && x.ENTRYFOR == entry_unit_org)).OrderByDescending(x => x.POSTINGDATE);
                            }
                            foreach (var item in reportData)
                            {

                                orgName = getOrgStringCode(item.ENTRYFOR);
                                //orgName = db.ORGHs.Where(x => x.ORG_STRUCTURE_CODE == item.ENTRYFOR).Select(y => y.ORG_STRUCTURE_NAME).First();
                                string ItemName = getItemName(item.FK_ASSET_ID);
                                item.ENTRYFOR = orgName;
                                //item.Items.ItemName = ItemName;
                                //string orgPersonCode = "";                                
                            }

                        }
                        else if (val["ReportFor"] == "Personnel")
                        {
                            if (entry_unit_org == "PHQ")

                            {
                                reportData =
                                db.TBL_ASSETLEDGER.Where(
                                    x =>
                                    (x.TRANSACTIONTYPE == 1 && x.ENTRYTYPE == 1 && x.ENTRYFOR == entry_unit_org) ||
                                    (x.TRANSACTIONTYPE == 2 && x.ENTRYTYPE == 2 && x.ENTRYFOR == entry_unit_org)).OrderByDescending(x => x.POSTINGDATE);
                            }

                            else
                            {
                                reportData =
                                db.TBL_ASSETLEDGER.Where(
                                    x =>
                                    (x.TRANSACTIONTYPE == 1 && x.ENTRYTYPE == 1) ||
                                    (x.TRANSACTIONTYPE == 2 && x.ENTRYTYPE == 2)).OrderByDescending(x => x.POSTINGDATE);
                            }


                            foreach (var item in reportData)
                            {
                                orgName = getOrgStringCode(item.ENTRYFOR);

                                //orgName = db.VIEW_AUTH.Where(x => x.PERSON_CODE == item.ENTRYFOR).Select(y => y.NAME).First();
                                item.ENTRYFOR = orgName;
                                //item.Items.ItemName = db.TBL_ASSET.Where(x => x.PK_ASSET_ID == item.FK_ASSET_ID).Select(x => x.ITEMNAME).FirstOrDefault();


                            }
                        }
                        reportViewer.LocalReport.ReportPath = Server.MapPath("~/rdlc/AssetReportType48.rdlc");
                        ReportDataSource rds = new ReportDataSource("AssetReportType48", reportData.ToList());
                        reportViewer.LocalReport.DataSources.Add(rds);
                    }
                    else if (val["ReportType"] == "जिन्सि निरिक्षण(४९)")
                    {
                        IQueryable<PRISM.ASSET_REPORTTYPE_49> reportData = null;
                        if (val["ReportFor"] == "Unit")
                        {

                            if (entry_unit_org == "PHQ")
                            {
                                reportData =
                                    db.ASSET_REPORTTYPE_49.Where(
                                        x =>
                                        (x.TRANSACTIONTYPE == 0 && x.ENTRYTYPE == 1) ||
                                        (x.TRANSACTIONTYPE == 1 && x.ENTRYTYPE == 0)).OrderByDescending(x => x.POSTINGDATE);
                            }
                            else
                            {
                                reportData =
                                    db.ASSET_REPORTTYPE_49.Where(
                                        x =>
                                        (x.TRANSACTIONTYPE == 0 && x.ENTRYTYPE == 1 && x.ENTRYFOR == entry_unit_org) ||
                                        (x.TRANSACTIONTYPE == 1 && x.ENTRYTYPE == 0 && x.ENTRYFOR == entry_unit_org)).OrderByDescending(x => x.POSTINGDATE);
                            }
                        }
                        else if (val["ReportFor"] == "Personnel")
                        {
                            if (entry_unit_org == "PHQ")
                            {
                                reportData =
                                    db.ASSET_REPORTTYPE_49.Where(
                                        x =>
                                        (x.TRANSACTIONTYPE == 1 && x.ENTRYTYPE == 1) ||
                                        (x.TRANSACTIONTYPE == 2 && x.ENTRYTYPE == 2)).OrderByDescending(x => x.POSTINGDATE);
                            }
                            else
                            {


                                reportData =
                                    db.ASSET_REPORTTYPE_49.Where(
                                        x =>
                                        (x.TRANSACTIONTYPE == 1 && x.ENTRYTYPE == 1 && x.ENTRYFOR == entry_unit_org) ||
                                        (x.TRANSACTIONTYPE == 2 && x.ENTRYTYPE == 2 && x.ENTRYFOR == entry_unit_org)).OrderByDescending(x => x.POSTINGDATE);

                            }
                        }
                        reportViewer.LocalReport.ReportPath = Server.MapPath("~/rdlc/AssetReportType49.rdlc");
                        ReportDataSource rds = new ReportDataSource("AssetReportType49", reportData.ToList());
                        reportViewer.LocalReport.DataSources.Add(rds);
                    }

                }
                else if (val["WAitems"] == "Weapons")
                {
                    if (val["ReportType"] == "दाखिला प्रतिबेदन(४६)")
                    {
                        IQueryable<PRISM.WEAPON_REPORTTYPE_46> reportData;
                        reportData = db.WEAPON_REPORTTYPE_46.Where(x => x.ENTRYTYPE == 1).OrderByDescending(x => x.DISTRIBUTEDDATE);
                        reportViewer.LocalReport.ReportPath = Server.MapPath("~/rdlc/Weapon_Report_type46.rdlc");
                        ReportDataSource rds = new ReportDataSource("WeaponReport", reportData.ToList());
                        reportViewer.LocalReport.DataSources.Add(rds);
                    }
                    else if (val["ReportType"] == "हस्तान्तरण(४८)")
                    {
                        IQueryable<PRISM.WEAPON_REPORTTYPE_46> reportData = null;
                        if (val["ReportFor"] == "Unit")
                        {
                           // reportData = db.WEAPON_REPORTTYPE_46.Where(x => (x.ENTRYTYPE == 2 || x.ENTRYTYPE == 4)).OrderByDescending(x => x.DISTRIBUTEDDATE);
                            reportData = db.WEAPON_REPORTTYPE_46.Where(x => (x.ENTRYTYPE == 2 || x.ENTRYTYPE == 4)).OrderByDescending(x => x.DISTRIBUTEDDATE);
                        }
                        else if (val["ReportFor"] == "Personnel")
                        {
                            reportData = db.WEAPON_REPORTTYPE_46.Where(x => (x.ENTRYTYPE == 3 || x.ENTRYTYPE == 5)).OrderByDescending(x => x.DISTRIBUTEDDATE);
                        }
                        reportViewer.LocalReport.ReportPath = Server.MapPath("~/rdlc/Weapon_Report_type48.rdlc");
                        ReportDataSource rds = new ReportDataSource("WeaponReport", reportData.ToList());
                        reportViewer.LocalReport.DataSources.Add(rds);
                    }
                    else if (val["ReportType"] == "जिन्सि निरिक्षण(४९)")
                    {
                        IQueryable<PRISM.WEAPON_REPORTTYPE_49> reportData = null;
                        if (val["ReportFor"] == "Unit")
                        {
                            reportData = db.WEAPON_REPORTTYPE_49.Where(x => (x.ENTRYTYPE == 2 || x.ENTRYTYPE == 4)).OrderByDescending(x => x.DISTRIBUTEDDATE);
                        }
                        else if (val["ReportFor"] == "Personnel")
                        {
                            reportData = db.WEAPON_REPORTTYPE_49.Where(x => (x.ENTRYTYPE == 3 || x.ENTRYTYPE == 5)).OrderByDescending(x => x.DISTRIBUTEDDATE);
                        }
                        reportViewer.LocalReport.ReportPath = Server.MapPath("~/rdlc/WeaponReport_Type_49.rdlc");
                        ReportDataSource rds = new ReportDataSource("WeaponReport49", reportData.ToList());
                        reportViewer.LocalReport.DataSources.Add(rds);
                    }
                }
                if (val["ReportType"] == "जिन्सि निसर्ग | मिनाहा फारम(५०)")
                {
                    IQueryable<PRISM.ASSET_REPORT_50> reportData = null;
                    reportData = db.ASSET_REPORT_50.Where(x => (x.WAIVED == 1)).OrderByDescending(x => x.POSTINGDATE);
                    if (!string.IsNullOrEmpty(val["StartDate"]))
                    {
                        DateTime startDate;
                        if (DateTime.TryParse(val["StartDate"], out startDate))
                            reportData = reportData.Where(x => x.POSTINGDATE >= startDate).OrderByDescending(x => x.POSTINGDATE);
                        else
                            dateIsValid = false;
                    }
                    if (!string.IsNullOrEmpty(val["EndDate"]))
                    {
                        DateTime endDate;
                        if (DateTime.TryParse(val["EndDate"], out endDate))
                        {
                            endDate = endDate.AddDays(1);
                            reportData = reportData.Where(x => x.POSTINGDATE <= endDate).OrderByDescending(x => x.POSTINGDATE);
                        }
                    }
                    else
                        dateIsValid = false;

                    if (dateIsValid)
                    {
                        reportViewer.LocalReport.ReportPath = Server.MapPath("~/rdlc/AssetReportType50.rdlc");
                        ReportDataSource rds = new ReportDataSource("AssetReportType50", reportData.ToList());
                        reportViewer.LocalReport.DataSources.Add(rds);
                    }
                }

                else if (val["ReportType"] == "जिन्सी मौज्दातको विवरण(५७)")
                {
                    IQueryable<PRISM.ASSET_REPORT_50> reportData = null;
                    reportData = db.ASSET_REPORT_50;
                    if (!string.IsNullOrEmpty(val["StartDate"]))
                    {
                        DateTime startDate;
                        if (DateTime.TryParse(val["StartDate"], out startDate))
                            reportData = reportData.Where(x => x.POSTINGDATE >= startDate).OrderByDescending(x => x.POSTINGDATE);
                        else
                            dateIsValid = false;
                    }
                    if (!string.IsNullOrEmpty(val["EndDate"]))
                    {
                        DateTime endDate;
                        if (DateTime.TryParse(val["EndDate"], out endDate))
                        {
                            endDate = endDate.AddDays(1);
                            reportData = reportData.Where(x => x.POSTINGDATE <= endDate).OrderByDescending(x => x.POSTINGDATE);
                        }
                    }
                    if (dateIsValid)
                    {
                        reportViewer.LocalReport.ReportPath = Server.MapPath("~/rdlc/AssetReportType57.rdlc");
                        ReportDataSource rds = new ReportDataSource("AssetReportType50", reportData.ToList());
                        reportViewer.LocalReport.DataSources.Add(rds);
                    }
                }

            }
            if (dateIsValid)
            {
                reportViewer.LocalReport.Refresh();
                ViewBag.ReportViewer = reportViewer;
                ViewBag.Message = null;
            }
            else
            {
                ViewBag.Message = "Invalid Date";
            }
            return PartialView();
        }

        public ActionResult Index()
        {
            ViewBag.ReportType = new SelectList(getReportType(), string.Empty);
            ViewBag.ReportFor = new SelectList(getReportsFor(), string.Empty);
            ViewBag.WAitems = new SelectList(getReportWAitems(), string.Empty);

            return View();
        }
        public ActionResult ReportPreProcessor(FormCollection val)
        {
            ViewBag._Type = val["ReportType"];
            ViewBag._For = val["ReportFor"];
            ViewBag._WAitems = val["WAitems"];
            ViewBag._Date1 = val["Date1"];
            ViewBag._Date2 = val["Date2"];
            ViewBag._unitOrPerson = val["unitorperson"];
            ViewBag._items = val["Items"];
            return View("ReportsHost");
        }

        public List<string> getReportType()
        {
            List<string> st = new List<string>();
            st.Add("दाखिला प्रतिबेदन(४६)");
            st.Add("हस्तान्तरण(४८)");
            //st.Add("खर्च भएर जाने जिन्सी समान को खाता(52)");
            st.Add("जिन्सि निरिक्षण(४९)");
            st.Add("जिन्सि निसर्ग | मिनाहा फारम(५०)");
            st.Add("जिन्सी मौज्दातको विवरण(५७)");
            return st;
        }
        public List<string> getReportsFor()
        {
            List<string> st = new List<string>();
            st.Add("Personnel");
            st.Add("Unit");
            return st;
        }
        public List<string> getReportWAitems()
        {
            List<string> st = new List<string>();
            st.Add("Asset");
            st.Add("Weapons");
            return st;
        }
        //public JsonResult GetReportType(string ReportType)
        //{
        //    return Json("Null", JsonRequestBehavior.AllowGet);
        //}
        public JsonResult Getfortype(string fortype)
        {
            using (NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities())
            {
                if (fortype == "Personnel")
                {
                    var query = from x in db.VIEW_AUTH
                                select new
                                {
                                    id = x.PERSON_CODE,
                                    display = x.NAME
                                };
                    return Json(query, JsonRequestBehavior.AllowGet);
                }
                else if (fortype == "Unit")
                {
                    var query = from x in db.VIEW_POLICE_UNIT
                                select new
                                {
                                    x.ID,
                                    display = x.UNIT_NAME
                                };

                    return Json(query.ToList(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Null", JsonRequestBehavior.AllowGet);
                }
            }
        }


        public string getOrgStringCode(string entryFor)
        {
            NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
            int count = db.ORGHs.Where(x => x.ORG_STRUCTURE_CODE == entryFor).Count();
            string orgPersonCode = "";
            if (count > 0)
            {
                orgPersonCode = db.ORGHs.Where(x => x.ORG_STRUCTURE_CODE == entryFor).Select(x => x.ORG_STRUCTURE_NAME).FirstOrDefault();
            }
            else
            {
                orgPersonCode = "Undifined";
            }
            return orgPersonCode;

        }
        public string getItemName(string itemId)
        {
            NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
            int count = db.TBL_ASSET.Where(x => x.PK_ASSET_ID == itemId).Count();
            string itemName = "";
            if (count>0)
            {
                itemName = db.TBL_ASSET.Where(x => x.PK_ASSET_ID == itemId).Select(x => x.ITEMNAME).FirstOrDefault();
            }
            else
            {
                itemName = "Undifined";
            }
            return itemName;
        }

        public JsonResult GetItemList(string Itype)
        {
            using (NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities())
            {
                if (Itype == "Asset")
                {
                    var query = from x in db.TBL_ASSET
                                select new
                                {
                                    id = x.PK_ASSET_ID,
                                    display = x.ITEMNAME

                                };
                    return Json(query.ToList(), JsonRequestBehavior.AllowGet);
                }
                else if (Itype == "Weapons")
                {
                    var query = from x in db.TBL_WEAPON_DETAIL
                                select new
                                {
                                    id = x.PK_WEAPONS_ID,
                                    display = x.WEAPON_NAME
                                };
                    return Json(query.ToList(), JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Null", JsonRequestBehavior.AllowGet);
                }
            }

        }
    }
}
