﻿using PRISM.Filters;
using PRISM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRISM.Controllers
{
    [SessionExpireFilter]
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class UsersController : Controller
    {
        
        private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
        //
        // GET: /Users/
        [SessionExpireFilter]
        public ActionResult Index()
        {
            var query = UserList();
            return View(query);
        }
        [SessionExpireFilter]
        public ActionResult DisplaySearchUsers(FormCollection val)
        {

            var param = val["userIdentity"].ToString();
            var query = SearchUserList(param);

            return PartialView("iDisplaySearchUsers", query);
        }

        [SessionExpireFilter]
        public IEnumerable<PRISM.Models.LoginModel2> UserList()
        {
            var result = db.Database.SqlQuery<PRISM.Models.LoginModel2>(@"SELECT tbl_user_details.PERSON_CODE as UserId,
  tbl_user_details.PASSWORD as Passwd,
  CASE tbl_user_details.IS_ACTIVE WHEN '1' THEN 'Active' ELSE 'InActive' End as Status,
  TO_CHAR(tbl_user_details.FK_ROLEID) as userroleid ,
View_auth.RANK_NAME Rank,
  view_auth.NAME as FullName,
  view_auth.ID unitid,
  view_auth.UNIT_NAME unitname,
  view_auth.MASTER as UnitUpperId,
  tbl_iroles.ROLENAME as UserRole
FROM tbl_user_details
LEFT JOIN view_auth
ON tbl_user_details.PERSON_CODE = view_auth.PERSON_CODE
INNER JOIN tbl_iroles
ON tbl_user_details.FK_ROLEID = tbl_iroles.PK_ROLEID
where 
tbl_user_details.FK_ROLEID <> '4'").ToList<PRISM.Models.LoginModel2>();
            return result;
        }

        [SessionExpireFilter]
        public IEnumerable<PRISM.Models.LoginModel2> SearchUserList(string s)
        {

            var result = db.Database.SqlQuery<PRISM.Models.LoginModel2>(@"SELECT TBL_USER_DETAILS.FK_ROLEID,
TBL_IROLES.Rolename UserRole,
  View_auth.PERSON_CODE as UserId,
  View_auth.RANK_NAME Rank,
  View_auth.NAME as FullName,
  View_auth.ID as UnitId,
  View_auth.UNIT_NAME as UnitName,
  TBL_USER_DETAILS.PASSWORD as Passwd,
  CASE TBL_USER_DETAILS.IS_ACTIVE WHEN '1' THEN 'Active' ELSE 'Inactive' END as Status
FROM View_auth
LEFT JOIN TBL_USER_DETAILS
ON TBL_USER_DETAILS.PERSON_CODE = View_auth.PERSON_CODE
Left Join
TBL_IROLES
on TBL_USER_DETAILS.FK_ROLEID = TBL_IROLES.PK_ROLEID
where View_auth.NAME like '" + s + @"%' or View_auth.Person_code like '" + s + @"%' or View_auth.RANK_NAME like '" + s + @"%' or view_auth.id like '" + s + @"%' or View_auth.UNIT_NAME like '" + s + @"%'").ToList<PRISM.Models.LoginModel2>();

            return result;
        }

        [SessionExpireFilter]
        public ActionResult ShowUserDetail(string user)
        {
            var param = decimal.Parse(user);

            #region InsertIfDoesntExist
            if (db.TBL_USER_DETAILS.Where(x => x.PERSON_CODE == user).ToList().Count() < 1)
            {
                TBL_USER_DETAILS tb = new TBL_USER_DETAILS();
                tb.USER_ID = param;
                tb.PERSON_CODE = user;
                tb.PASSWORD = user;
                tb.IS_ACTIVE = "1";
                tb.ACTIVATED_ON = DateTime.Now.Date;
                tb.LAST_LOGGED_ON = DateTime.Now.Date;
                tb.LAST_LOGGED_ON = DateTime.Now.Date;
                tb.LAST_UPDATED_BY = User.Identity.Name;
                tb.FK_ROLEID = 4;
                try
                {
                    db.TBL_USER_DETAILS.Add(tb);
                    db.SaveChanges();
                }
                catch (Exception Ex)
                {

                    var e = Ex;
                }
            }
            #endregion


            var access = db.TBL_IROLES.Select(m => new { m.PK_ROLEID, m.ROLENAME }).ToList();

            LoginModel2 result = new LoginModel2();
            result = (from xsa in db.VIEW_AUTH
                      join
                      xsb in db.TBL_USER_DETAILS
                      on new { ss = xsa.PERSON_CODE } equals new { ss = xsb.PERSON_CODE }
                      join xsc in db.TBL_IROLES
                      on new { st = (Decimal)xsb.FK_ROLEID } equals new { st = xsc.PK_ROLEID }
                      where
                      xsb.USER_ID == param
                      select new
                      {
                          xsb.USER_ID,
                          xsa.PERSON_CODE,
                          xsa.RANK_NAME,
                          xsa.NAME,
                          xsa.ID,
                          xsa.ADDRESS,
                          xsa.UNIT_NAME,
                          xsb.ACTIVATED_ON,
                          xsb.LAST_LOGGED_ON,
                          xsb.FK_ROLEID,
                          xsc.ROLENAME
                          ,
                          xsb.IS_ACTIVE
                      }).AsEnumerable().Select(x => new LoginModel2
                      {
                          Usr = x.USER_ID,
                          UserId = x.PERSON_CODE,
                          Rank = x.RANK_NAME,
                          FullName = x.NAME,
                          UnitId = x.ID,
                          Address = x.ADDRESS,
                          UnitName = x.UNIT_NAME,
                          ActiveSince = x.ACTIVATED_ON.HasValue?x.ACTIVATED_ON.Value.ToString("yyyy-MM-dd"):"",
                          LastActivityOn = x.LAST_LOGGED_ON.HasValue ? x.LAST_LOGGED_ON.Value.ToString("yyyy-MM-dd") : "",
                          UserRole = x.ROLENAME,
                          UserRoleId = ((Decimal?)x.FK_ROLEID).ToString(),
                          statusbool = x.IS_ACTIVE == "1" ? true : false
                      }).SingleOrDefault();
            //ViewBag.selected = result.UserRole;
            ViewBag.AccessRoles = new SelectList(access, "PK_ROLEID", "ROLENAME");
            return View("iShowUserDetail", result);
        }

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult SaveUserResult(LoginModel2 v)
        {
            var list = db.TBL_USER_DETAILS.Find(v.Usr);
            if (v.statusbool != null)
            {
                list.IS_ACTIVE = v.statusbool == true ? "1" : "0";
            }

            if (v.UserRoleId != null)
            {
                list.FK_ROLEID = decimal.Parse(v.UserRoleId);
            }
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public JsonResult ResetPassword(string pfor, string pwa, string pwb)
        {
            var parm = decimal.Parse(pfor);
            var query = db.TBL_USER_DETAILS.Where(x => ((x.PERSON_CODE == pfor) || (x.USER_ID == parm))).SingleOrDefault();
            try
            {
                query.PASSWORD = pwa;
                db.SaveChanges();
                return Json(JsonResponseFactory.SuccessResponse());
            }
            catch (Exception es)
            {

                var ex = es;
                return Json(JsonResponseFactory.ErrorResponse("Error"));
            }


        }

    }
}
