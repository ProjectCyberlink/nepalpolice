﻿using PRISM.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRISM.Controllers
{
    
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class TreeViewController : Controller
    {
        private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();

        #region Actions
        public PartialViewResult Treeview(bool IsWeaponReturn=false)
        {            

            return PartialView();
        }
         
        [AcceptVerbs(HttpVerbs.Post)]
        [SessionExpireFilter]
        public ActionResult FileSystemInfos(string root)
        {
            bool isWeaponReturn=false;
            if(Session["IsWeaponReturn"]!=null)
                isWeaponReturn = bool.Parse(Session["IsWeaponReturn"].ToString()); ;
            IEnumerable<TreeViewNode> children = null;
            if (!isWeaponReturn)
            {
                string unitId = (root == "source" || root == "") ? Session["UnitCode"].ToString() : root;
                children = GetUnitInfo(unitId, isWeaponReturn);
            }
            else
            {
                string query = @"SELECT 
                                    rownum R_NUM,
                                    ID,
                                    UNIT_NAME,
                                    UNIT_NAME_NP,
                                    MASTER,
                                    RID
                                    FROM
                                       view_police_unit
                                    CONNECT BY
                                        ID = PRIOR MASTER
                                    START WITH
                                       ID = '" + Session["UnitCode"].ToString() + "' order by R_NUM desc";
                var data = db.Database.SqlQuery<VIEW_POLICE_UNIT>(query);
                TempData["ParentUnits"] = data;
                var parent = data.FirstOrDefault().ID;
                string unitId = (root == "source" || root == "") ? parent : root;
                children = GetUnitInfo(unitId, isWeaponReturn);
            }            
            return Json(children);
            
        }        

        public List<TreeViewNode> GetUnitInfo(string UnitId, bool IsweaponReturn)
        {
            List<TreeViewNode> treeView=null;
            if (!IsweaponReturn)
                treeView = (from e1 in db.VIEW_POLICE_UNIT
                            where e1.MASTER == UnitId //&& e1.ID == "PHQ"
                            select new TreeViewNode()
                            {
                                text = e1.UNIT_NAME,
                                id = e1.ID,
                                expanded = false,
                                hasChildren = true
                            }).ToList<TreeViewNode>();
            else
            {
                IEnumerable<VIEW_POLICE_UNIT> data = TempData["ParentUnits"] as IEnumerable<VIEW_POLICE_UNIT>;
                treeView = (from e1 in data
                            where e1.MASTER == UnitId 
                            select new TreeViewNode()
                            {
                                text = e1.UNIT_NAME,
                                id = e1.ID,
                                expanded = false,
                                hasChildren = true
                            }).ToList<TreeViewNode>();

            }
            

            return treeView;
        }
        #endregion

    }
}
