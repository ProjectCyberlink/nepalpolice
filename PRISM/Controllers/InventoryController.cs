﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using PRISM.Models;
namespace PRISM.Controllers
{
    [Authorize(Roles = "Admin")]
    [SessionExpireFilter]
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class InventoryController : Controller
    {
        //    private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();

        //    //
        //    // GET: /Inventory/

        //    public ActionResult Create()
        //    {
        //        return View();
        //    }

        //    [HttpPost]
        //    [ValidateAntiForgeryToken]
        //    public ActionResult Create(Item item)
        //    {
        //        PRISM.Item items = new Item
        //        {
        //            guid = Guid.NewGuid(),
        //            ENTRYDATE = DateTime.Now,
        //            ITEMNAME = item.ITEMNAME,
        //            bookpgid = item.bookpgid,
        //            SPECIFICATION = item.SPECIFICATION,
        //            unit = item.unit,
        //            QUANTITY = item.QUANTITY,
        //            costPerUnit = item.costPerUnit,
        //            taxPU = item.taxPU,
        //            EXTRAEXPENSES = item.EXTRAEXPENSES,
        //            COMMENTS = item.COMMENTS,
        //            purchaseOrderNo = item.purchaseOrderNo,
        //            //entryid = WebSecurity.CurrentUserId,
        //            //entryname = WebSecurity.CurrentUserName,
        //            entryid = int.Parse(User.Identity.Name),
        //            entryname = User.Identity.Name,
        //            Total = item.Total
        //        };

        //        try
        //        {
        //            db.Items.Add(items);
        //            db.SaveChanges();
        //            return RedirectToAction("Index");
        //        }
        //        catch (Exception e) { var exc = e; }

        //        return View(item);
        //    }

        //    public ActionResult Delete(int id = 0)
        //    {
        //        Item item = db.Items.Find(id);
        //        if (item == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(item);
        //    }

        //    [HttpPost, ActionName("Delete")]
        //    [ValidateAntiForgeryToken]
        //    public ActionResult DeleteConfirmed(int id)
        //    {
        //        Item item = db.Items.Find(id);
        //        db.Items.Remove(item);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    public ActionResult Details(int id = 0)
        //    {
        //        Item item = db.Items.Find(id);
        //        if (item == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(item);
        //    }

        ////[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        //    public PartialViewResult DetailOSP(int id = 0)
        //    {
        //        ViewBag.HeadMessage = "Detail For";
        //        Item item = db.Items.Find(id);
        //        return PartialView("_partialDetail",item);
        //    }

        //    public PartialViewResult CreateOSP()
        //    {
        //        ViewBag.HeadMessage = "Create New Inventory Item";
        //        return PartialView("iCreate");
        //    }

        //    public ActionResult Edit(int id = 0)
        //    {
        //        Item item = db.Items.Find(id);
        //        if (item == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(item);
        //    }

        //    [HttpPost]
        //    [ValidateAntiForgeryToken]
        //    public ActionResult Edit(Item item)
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            db.Entry(item).State = EntityState.Modified;
        //            db.SaveChanges();
        //            return RedirectToAction("Index");
        //        }
        //        return View(item);
        //    }

        //    public ActionResult Index()
        //    {

        //        ////////var res = (from a in db.Items join b in ( (from ITRANSACTIONS in db.ITRANSACTIONS group ITRANSACTIONS by new { ITRANSACTIONS.ITEM_GUID } into g select new { g.Key.ITEM_GUID, qty = (int?)g.Sum(p => p.QUANTITY) })) on new { guid = a.guid } equals new { guid = b.ITEM_GUID } into b_join from b in b_join.DefaultIfEmpty() select new { a.bookpgid, a.ITEMNAME, a.SPECIFICATION, a.classification, a.unit, onHand = ((System.Double?)a.QUANTITY - b.qty ?? (System.Double?)0), a.costPerUnit, a.taxPU, a.purchaseOrderNo, a.ENTRYDATE, a.Total }).ToList();
        //        ////////var res = (from a in db.Items join b in db.ITRANSACTIONS on new { guid = (Guid)a.guid } equals new { guid = b.ITEM_GUID } into b_join from b in b_join.DefaultIfEmpty() select new { bookpgid = a.bookpgid, ITEMNAME = a.ITEMNAME, SPECIFICATION = a.SPECIFICATION, unit = a.unit, QUANTITY = (a.QUANTITY - b.QUANTITY), costPerUnit = a.costPerUnit, taxPU = a.taxPU, ENTRYDATE = a.ENTRYDATE, purchaseOrderNo = a.purchaseOrderNo, Total = a.Total }).ToList();
        //        var resl = (from a in db.Items
        //                    join b in
        //                        (
        //                            ((from ITRANSACTIONS in db.ITRANSACTIONS
        //                              group ITRANSACTIONS by new
        //                              {
        //                                  ITRANSACTIONS.ITEM_GUID
        //                              } into g
        //                              select new
        //                              {
        //                                  g.Key.ITEM_GUID,
        //                                  qty = (double?)g.Sum(p => p.QUANTITY)
        //                              }).Distinct())) on new { guid = a.guid } equals new { guid = b.ITEM_GUID } into b_join
        //                    from b in b_join.DefaultIfEmpty()
        //                    select new
        //                    {
        //                        /*guid = (Guid?)a.guid,
        //                         a.EXTRAEXPENSES,
        //                        a.COMMENTS,*/
        //                        a.id,
        //                        a.bookpgid,
        //                        a.ITEMNAME,
        //                        a.SPECIFICATION,
        //                        a.classification,
        //                        a.unit,
        //                        a.QUANTITY,
        //                        //qty = (double?)b.qty,
        //                        b.qty,
        //                        a.costPerUnit,
        //                        a.taxPU,
        //                        a.purchaseOrderNo,
        //                        a.ENTRYDATE,
        //                        a.entryid,
        //                        a.entryname,
        //                        a.Total
        //                    }).AsEnumerable().Select(c => new spItems
        //                    {id=c.id,
        //                        bookpgid = c.bookpgid,
        //                        ITEMNAME = c.ITEMNAME,
        //                        SPECIFICATION = c.SPECIFICATION,
        //                        classification = c.classification,
        //                        unit = c.unit,
        //                        //onHand = (c.QUANTITY - c.qty),
        //                        onHand = ((System.Double?)c.QUANTITY - ((Double?)c.qty ?? (Double?)0) ?? (System.Double?)0),
        //                        costPerUnit = c.costPerUnit,
        //                        taxPU = c.taxPU,
        //                        purchaseOrderNo = c.purchaseOrderNo,
        //                        ENTRYDATE = c.ENTRYDATE,
        //                        Total = c.Total
        //                    }).ToList();

        //         var findat = this.Json(resl);
        //        //(a.QUANTITY-b.QUANTITY),
        //        var dat = db.Items.ToList();

        //        //return View(db.Items.ToList());
        //        return View(resl);
        //    }


        //    protected override void Dispose(bool disposing)
        //    {
        //        db.Dispose();
        //        base.Dispose(disposing);
        //    }
        //}
    }
}