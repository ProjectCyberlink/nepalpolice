﻿using PRISM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRISM.Controllers
{
    [SessionExpireFilter]
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class PersonnelController : Controller
    {
        //
        // GET: /Personnel/
        private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
        public ActionResult Index()
        {
            PersonnelSearch search = new PersonnelSearch();            
            GetRanks(search);                       
            return PartialView("iPersonnelSearch",search);
        }

        
        
        public ActionResult Search(string name, string rank)
        {
            PersonnelSearch search = new PersonnelSearch();
            search.Name = name;
            search.Rank = rank;
            GetRanks(search);

            string query = "select * from VIEW_AUTH ";
            string whereClause=string.Empty;
            if (!string.IsNullOrEmpty(name))
            {
                whereClause += " and UPPER(NAME) like UPPER('" + name + "%')";
            }
            if (!string.IsNullOrEmpty(rank))
            {
                whereClause += " and UPPER(RANK_NAME) = UPPER('" + rank + "')";
            }

            
            if (!string.IsNullOrEmpty(whereClause))
                whereClause = " WHERE " + whereClause.Substring(4);
            query = query + whereClause;

            List<VIEW_AUTH> userDetails = db.Database.SqlQuery<VIEW_AUTH>(query).ToList();
            ViewData["UserDetails"] = userDetails;
            return Json(userDetails,JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<string> Ranks)
        {            
            var selectList = new List<SelectListItem>();
            foreach (var rank in Ranks)
            {
                selectList.Add(new SelectListItem
                {
                    Value = rank,
                    Text = rank
                });
            }

            return selectList;
        }

        private IEnumerable<SelectListItem> GetRanks(PersonnelSearch search)
        {
            if (search == null)
                search = new PersonnelSearch();

            if (Session["RankList"] == null)
            {
                IEnumerable<string> Ranks = db.VIEW_AUTH.Select(user => user.RANK_NAME).Distinct();
                search.Ranks = GetSelectListItems(Ranks);
                Session["RankList"] = search.Ranks;
            }
            else
            {
                var ranks = Session["RankList"];
                search.Ranks = Session["RankList"] as IEnumerable<SelectListItem>;
            }
            return search.Ranks;
        }
    }
}
