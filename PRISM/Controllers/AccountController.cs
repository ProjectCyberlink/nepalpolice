﻿



using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using PRISM.Filters;
using PRISM.Models;

namespace PRISM.Controllers
{
    [Authorize]
    //[InitializeSimpleMembership]
    public class AccountController : PrismBaseController
    {

        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
           
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            //if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            //{
            //    return RedirectToLocal(returnUrl);
            //}

            //// If we got this far, something failed, redisplay form
            //ModelState.AddModelError("", "The user name or password provided is incorrect.");
            //return View(model);
            if (ModelState.IsValid)
            {
                using (NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities())
                {
                    var udi = model.UserName;
                    //var objusr = db.VIEW_AUTH.FirstOrDefault(x => x.PERSON_CODE == udi && x.PASSWORD == model.Password);
                  
                    try
                     {                        
                         List<TBL_USER_DETAILS> userDetails= db.Database.SqlQuery<TBL_USER_DETAILS>(@"select 
                                                                    b.USER_ID, 
                                                                    b.IS_ACTIVE, 
                                                                    b.PERSON_CODE,
                                                                    b.PASSWORD,    
                                                                    b.IS_ACTIVE,
                                                                    b.ACTIVATED_ON,
                                                                    b.LAST_LOGGED_ON,
                                                                    b.LAST_UPDATED_ON,
                                                                    b.LAST_UPDATED_BY,
                                                                    b.FK_ROLEID 
                                                                    from 
                                view_auth a left join tbl_user_details b on a.person_code=b.person_code where a.person_code='" + udi + "' and ((b.PASSWORD='" + model.Password + "' and IS_ACTIVE=1) or ('" + udi + "'='" + model.Password + "' and (IS_ACTIVE=0 or IS_ACTIVE is null)))").ToList();
//(b.PASSWORD=" + model.Password + " or (" + udi + "=" + model.Password + " and IS_ACTIVE=1))").ToList();

                         if (userDetails != null && userDetails.Count > 0)
                         {
                             if ( userDetails[0] == null || userDetails[0].USER_ID == 0)
                             {
                                 TBL_USER_DETAILS tb = new TBL_USER_DETAILS();
                                 tb.USER_ID = decimal.Parse(model.UserName);
                                 tb.PERSON_CODE = model.UserName;
                                 tb.PASSWORD = model.UserName;
                                 tb.IS_ACTIVE = "1";
                                 tb.ACTIVATED_ON = DateTime.Now.Date;
                                 tb.LAST_LOGGED_ON = DateTime.Now.Date;
                                 tb.LAST_LOGGED_ON = DateTime.Now.Date;
                                 tb.LAST_UPDATED_BY =model.UserName;
                                 tb.FK_ROLEID = 4;
                                 try
                                 {
                                     db.TBL_USER_DETAILS.Add(tb);
                                     db.SaveChanges();
                                 }
                                 catch (Exception Ex)
                                 {
                                     
                                     var e = Ex;
                                 }
                             }
                             else
                             {
                                 if (userDetails[0].IS_ACTIVE == "1")
                                 {
                                     List<UserRoles> roles = db.Database.SqlQuery<UserRoles>(@"
                                                                select 
	                                                                ud.FK_ROLeID RoleId, 
	                                                                ro.ROLENAME RoleName 
                                                                from 
	                                                                tbl_user_details ud, tbl_iroles ro
                                                                where 
	                                                                
	                                                                ro.pk_roleId=ud.fk_roleid and 
	                                                                person_code =" + udi).ToList();
                                     if (roles != null && roles.Count > 0)
                                     {
                                         //UserRoles userRole = roles.Where(role => role.RoleId == roles.Min(c => c.RoleId)).FirstOrDefault();
                                         string userroles = string.Join(",", roles.Select(c => c.RoleId.ToString()).ToArray());
                                         Session["UserRole"] = userroles;

                                     }
                                     else
                                         Session["UserRole"] = "4";

                                 }
                                 else
                                     Session["UserRole"] = "4";

                                 List<string> userRights = db.Database.SqlQuery<string>(@"select RIGHT_NAME from tbl_Rights r, tbl_RoleRightAssociation rr where r.PK_RIGHTID=rr.FK_RIGHT_ID and rr.FK_ROLE_ID in (4," + Session["UserRole"] + ")").ToList();
                                 Session["UserRights"] = userRights;

                                 #region success
                                 FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                                 if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                                 {
                                     return Redirect(returnUrl);
                                 }
                                 else
                                 {
                                     var objusr = db.VIEW_AUTH.FirstOrDefault(x => x.PERSON_CODE == udi);
                                     this.Session["person"] = objusr.RANK_NAME + " " + objusr.NAME;
                                     this.Session["PersonCode"] = udi;
                                     this.Session["UnitCode"] = objusr.ID;
                                     this.Session["unitmaster"] = objusr.MASTER == "PHQ" ? 0 : 1;  /*For this to work we need to designate PHQ BHANDAR SHAKHA as master we need to set objusr.Master =='PHO' */
                                     return RedirectToAction("RedirectToDefault");
                                 }
                                 #endregion
                             }
                         }
                         else
                             ModelState.AddModelError("", "The user name or password provided is incorrect.");

                     }
                    
                    catch (Exception ex)
                    {
                        var exp = ex;
                        
                    }                  

                }

            }
            return View(model);
        }

        public ActionResult LogOfff()
        {

            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");

        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            //WebSecurity.Logout();
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        //
        // POST: /Account/Disassociate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // Only disassociate the account if the currently logged in user is the owner
            if (ownerAccount == User.Identity.Name)
            {
                // Use a transaction to prevent the user from deleting their last login credential
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
        public ActionResult personalpaybook()
        {
            var ENTRYFOR = User.Identity.Name;
            //var query = (
            //from a in db.TBL_ASSETLEDGER
            //join b in db.TBL_ASSET on new { FK_ASSET_ID = a.FK_ASSET_ID } equals new { FK_ASSET_ID = b.guid }
            //join c in db.TBL_MEASURINGUNITS_MASTER on new { UOM = (Guid)b.UOM } equals new { UOM = c.guid }
            //join d in db.VIEW_AUTH on new { ENTRYFOR = (Int32)a.ENTRYFOR } equals new { ENTRYFOR = d.PERSON_CODE }
            //join e in
            //    (
            //        (from Au in db.VIEW_AUTH
            //         select new
            //         {    Au.ADDRESS,
            //             Au.PERSON_CODE,
            //             Au.NAME,
            //             Au.Id
            //         })) on new { ENTRYBY = a.ENTRYBY } equals new { ENTRYBY = (Int32)e.PERSON_CODE }
            //where
            //  a.ENTRYTYPE == 1 || a.ENTRYTYPE == 2 &&
            //  a.TRANSACTIONTYPE == 1 || a.TRANSACTIONTYPE == 2 &&
            //  a.ENTRYFOR == ENTRYFOR
            //select new
            //{
            //    a.ALE,
            //    FK_ASSET_ID = (Guid?)a.FK_ASSET_ID,
            //    b.ITEMNAME,
            //    a.POSTINGDATE,
            //    ENTRYFOR = (Int32?)a.ENTRYFOR,
            //    a.DESCRIPTION,
            //    Qty = (Double?)Math.Abs((double)a.QUANTITY),
            //    a.ITEMCATEGORYCODE,
            //    a.open,
            //    a.EXTRAEXPENSES,
            //    a.TOTALEXPENSES,
            //    a.COMMENTS,
            //    a.WAIVED,
            //    a.WAIVEDReason,
            //    ENTRYFOR = d.NAME,
            //    ENTRYBY = e.NAME,
            //    Unit = e.Id,
            //    Address = e.ADDRESS
            //}).AsEnumerable().Select(xs => new PRISM.Models.paybookModel
            //             {
            //                 pbITEMNAME = xs.ITEMNAME,
            //                 pbPOSTINGDATE = (xs.POSTINGDATE).ToString(),
            //                 pbDesc = xs.DESCRIPTION,
            //                 pbQUANTITY = decimal.Parse(xs.Qty.ToString()),
            //                 pbitemCatCode = int.Parse(xs.ITEMCATEGORYCODE.ToString()),
            //                 pbCOMMENTS = xs.COMMENTS,
            //                 pbhandedby = xs.ENTRYBY,
            //                 pbAddress = xs.Address,
            //             }).ToList();

            IEnumerable<PRISM.Models.paybookModel> query = (from a in db.TBL_ASSETLEDGER
                                                           join b in db.TBL_ASSET
                                                           on new { itemid = a.FK_ASSET_ID } equals new { itemid = b.PK_ASSET_ID }
                                                           join c in
                                                               (
                                                               (from x in db.VIEW_AUTH
                                                                select new
                                                                {
                                                                    x.PERSON_CODE,
                                                                    x.NAME,
                                                                    x.UNIT_NAME,
                                                                    x.ADDRESS
                                                                })) on new { entryfor = a.ENTRYFOR } equals new { entryfor = c.PERSON_CODE }
                                                           join d in
                                                               (
                                                               (from y in db.VIEW_AUTH
                                                                select new
                                                                {
                                                                    y.PERSON_CODE,
                                                                    y.NAME,
                                                                    y.UNIT_NAME,
                                                                    y.ADDRESS
                                                                })) on new { entryby = a.ENTRYBY } equals new { entryby = d.PERSON_CODE }

                                                           where
                                                             a.ENTRYFOR == ENTRYFOR &&
                                                           ((a.ENTRYTYPE == 1 && a.TRANSACTIONTYPE == 1) || (a.ENTRYTYPE == 2 && a.TRANSACTIONTYPE == 2))
                                                           orderby a.POSTINGDATE descending
                                                           select new
                                                           {
                                                               b.ITEMNAME,
                                                               entbyn = c.NAME,
                                                               entbyu = d.UNIT_NAME,
                                                               a.ENTRYFOR,
                                                               a.DESCRIPTION,
                                                               a.QUANTITY,
                                                               a.COMMENTS,
                                                               ent4 = d.NAME,
                                                               ent4n = d.UNIT_NAME,
                                                               ent4a = d.ADDRESS,

                                                               a.POSTINGDATE
                                                           }).AsEnumerable().Select(xs => new PRISM.Models.paybookModel
                                                           {
                                                               pbITEMNAME = xs.ITEMNAME,
                                                               pbPOSTINGDATE = xs.POSTINGDATE,
                                                               pbDesc = xs.DESCRIPTION,
                                                               pbQUANTITY = Math.Abs(decimal.Parse(xs.QUANTITY.ToString())),
                                                               pbENTRYFOR = xs.entbyn, // =int.Parse(xs.ItemCategoryCode.ToString()),
                                                               pbCOMMENTS = xs.COMMENTS,
                                                               pbhandedby = xs.ent4,
                                                               pbAddress = xs.ent4n + "," + xs.ent4a,
                                                           }).ToList().Take(100);
            return View(query);
        }

        public ActionResult RedirectToDefault()
        {

            string[] roles = Roles.GetRolesForUser();
            if (this.Session["UserRole"].ToString() != "4")
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("personalpaybook", "Account");
            }
        }

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("", String.Format("Unable to create local account. An account with the name \"{0}\" may already exist.", User.Identity.Name));
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            if (!result.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }

            if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false))
            {
                return RedirectToLocal(returnUrl);
            }

            if (User.Identity.IsAuthenticated)
            {
                // If the current user is logged in add the new account
                OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, User.Identity.Name);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // User is new, ask for their desired membership name
                string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
                ViewBag.ReturnUrl = returnUrl;
                return View("ExternalLoginConfirmation", new RegisterExternalLoginModel { UserName = result.UserName, ExternalLoginData = loginData });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model, string returnUrl)
        {
            string provider = null;
            string providerUserId = null;

            if (User.Identity.IsAuthenticated || !OAuthWebSecurity.TryDeserializeProviderUserId(model.ExternalLoginData, out provider, out providerUserId))
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Insert a new user into the database
                using (UsersContext db = new UsersContext())
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
                    // Check if user already exists
                    if (user == null)
                    {
                        // Insert name into the profile table
                        db.UserProfiles.Add(new UserProfile { UserName = model.UserName });
                        db.SaveChanges();

                        OAuthWebSecurity.CreateOrUpdateAccount(provider, providerUserId, model.UserName);
                        OAuthWebSecurity.Login(provider, providerUserId, createPersistentCookie: false);

                        return RedirectToLocal(returnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError("UserName", "User name already exists. Please enter a different user name.");
                    }
                }
            }

            ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(provider).DisplayName;
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // GET: /Account/ExternalLoginFailure

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult ExternalLoginsList(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
        }

        [ChildActionOnly]
        public ActionResult RemoveExternalLogins()
        {
            ICollection<OAuthAccount> accounts = OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name);
            List<ExternalLogin> externalLogins = new List<ExternalLogin>();
            foreach (OAuthAccount account in accounts)
            {
                AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(account.Provider);

                externalLogins.Add(new ExternalLogin
                {
                    Provider = account.Provider,
                    ProviderDisplayName = clientData.DisplayName,
                    ProviderUserId = account.ProviderUserId,
                });
            }

            ViewBag.ShowRemoveButton = externalLogins.Count > 1 || OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            return PartialView("_RemoveExternalLoginsPartial", externalLogins);
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
