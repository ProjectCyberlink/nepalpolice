﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace PRISM.Controllers
{
    [Authorize]
    [SessionExpireFilter]
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class MeasureOfUnitController : Controller
    {
        private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();

        //
        // GET: /MeasureOfUnit/

        public ActionResult Index()
        {
            return View(db.TBL_MEASURINGUNITS_MASTER.ToList());
        }

        //
        // GET: /MeasureOfUnit/Details/5

        public ActionResult Details(int id = 0)
        {
            TBL_MEASURINGUNITS_MASTER TBL_MEASURINGUNITS_MASTER = db.TBL_MEASURINGUNITS_MASTER.Find(id);
            if (TBL_MEASURINGUNITS_MASTER == null)
            {
                return HttpNotFound();
            }
            return View(TBL_MEASURINGUNITS_MASTER);
        }

        //
        // GET: /MeasureOfUnit/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /MeasureOfUnit/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TBL_MEASURINGUNITS_MASTER TBL_MEASURINGUNITS_MASTER)
        {
            PRISM.TBL_MEASURINGUNITS_MASTER mms = new TBL_MEASURINGUNITS_MASTER
            {
                PK_UOM_ID = Guid.NewGuid().ToString(),
                CREATEDDATE = DateTime.Now,
                ENTRYID = WebSecurity.CurrentUserId,
                UNITNAME = TBL_MEASURINGUNITS_MASTER.UNITNAME,
                UNITSHORT = TBL_MEASURINGUNITS_MASTER.UNITSHORT,
                DESCRIPTION = TBL_MEASURINGUNITS_MASTER.DESCRIPTION,
            };
            try
            {
                db.TBL_MEASURINGUNITS_MASTER.Add(mms);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex) { var exp = ex; }

            return View(TBL_MEASURINGUNITS_MASTER);
        }

        //
        // GET: /MeasureOfUnit/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TBL_MEASURINGUNITS_MASTER TBL_MEASURINGUNITS_MASTER = db.TBL_MEASURINGUNITS_MASTER.Find(id);
            if (TBL_MEASURINGUNITS_MASTER == null)
            {
                return HttpNotFound();
            }
            return View(TBL_MEASURINGUNITS_MASTER);
        }

        //
        // POST: /MeasureOfUnit/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TBL_MEASURINGUNITS_MASTER TBL_MEASURINGUNITS_MASTER)
        {
            if (ModelState.IsValid)
            {
                db.Entry(TBL_MEASURINGUNITS_MASTER).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(TBL_MEASURINGUNITS_MASTER);
        }

        //
        // GET: /MeasureOfUnit/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TBL_MEASURINGUNITS_MASTER TBL_MEASURINGUNITS_MASTER = db.TBL_MEASURINGUNITS_MASTER.Find(id);
            if (TBL_MEASURINGUNITS_MASTER == null)
            {
                return HttpNotFound();
            }
            return View(TBL_MEASURINGUNITS_MASTER);
        }

        //
        // POST: /MeasureOfUnit/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TBL_MEASURINGUNITS_MASTER TBL_MEASURINGUNITS_MASTER = db.TBL_MEASURINGUNITS_MASTER.Find(id);
            db.TBL_MEASURINGUNITS_MASTER.Remove(TBL_MEASURINGUNITS_MASTER);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}