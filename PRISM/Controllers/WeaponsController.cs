﻿using PRISM.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
//using System.Data.Objects.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PRISM.Controllers
{
    [Authorize(Roles = "Admin")]
    [SessionExpireFilter]
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class WeaponsController : PrismBaseController
    {
        private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
        string _DistributedTo; /**Distribution to referes to the unit or person to whom weapons is distributed. When transferring/distributing weapon, this variable is not used.**/
        /****define entry type starts****/
        /****
         * if PHQ -> PHQ  1
         * if PHQ -> Unit 2
         * if PHQ -> Person 3
         * if Unit -> Unit 4
         * if Unit -> Person 5
         * **/
        byte _entryType;
        byte _entryType2;
        /****define entry type ends****/
        //
        // GET: /Weapons/
        byte _entryType3;
        public void setVal()
        {
            
            _DistributedTo = this.Session["UnitCode"].ToString();
            _entryType = (_DistributedTo == "PHQ") ? Convert.ToByte(1) : Convert.ToByte(2);
            _entryType2 = (_DistributedTo == "PHQ") ? Convert.ToByte(6) : Convert.ToByte(4);
            _entryType3 = (_DistributedTo == "PHQ") ? Convert.ToByte(1) : Convert.ToByte(7);

            //_entryType = (_DistributedTo == 1) ? Convert.ToByte(1) : Convert.ToByte(2);
            //_entryType2 = (_DistributedTo == 1) ? Convert.ToByte(6) : Convert.ToByte(4);
            //_entryType3 = (_DistributedTo == 1) ? Convert.ToByte(1) : Convert.ToByte(7);
        }

        [SessionExpireFilter]
        public ActionResult Index()
        {
            //    return View(dat);
            string[] roles = Roles.GetRolesForUser();
            if (roles.Contains("Admin"))
            {
                /*Replace this with Dbcode*/
                //var data = (from a in db.tbl_wepons_head join b in ((from tb in db.tbl_wepons_line where tb.wltype == 0 && tb.wlactive == 1 group tb by new { tb.whguid } into g select new { whguid = (Guid?)g.Key.whguid, qty = (Int32?)g.Sum(p => p.wlQUANTITY) })) on new { whguid = a.whguid } equals new { whguid = (Guid)b.whguid } join c in db.TBL_MEASURINGUNITS_MASTER on new { whunit = a.whunit } equals new { whunit = c.guid } select new { a.whid, whguid = (Guid?)a.whguid, a.whname, a.whtype, a.whSPECIFICATION, c.UNITSHORT, a.whpurchasedate, b.qty, a.whENTRYDATE, a.whENTRYBY, a.whCOMMENTS }).AsEnumerable().Select(xs => new PRISM.Models.spWeaponOnhand { whguid = xs.whguid.ToString(), whname = xs.whname, whtype = xs.whtype, whSPECIFICATION = xs.whSPECIFICATION, UNITSHORT = xs.UNITSHORT, qty = xs.qty.ToString(), }).ToList();

                //TempData["list"] = getUnitSpecific();
                return RedirectToAction("WeaponsMaster");
            }
            if (roles.Contains("Registered"))
            {
                var weaponList = GetAvailableWeaponInUnit(Session["UnitCode"].ToString());
                TempData["list"] = weaponList;
                return RedirectToAction("Index", weaponList);
            }
            else
            {
                return RedirectToAction("personalpaybook", "Account");
            }

        }

        public List<PRISM.Models.spWeaponOnhand> GetAvailableWeaponInUnit(string unitCode)
        {

            string query =
                string.Format(
                    @"SELECT mi.PK_MASTER_INV_ID whguid,mi.ITEMNAME whtype, mi.SPECIFICATION whSPECIFICATION, 
                                sum(case when wdist.DISTRIBUTIONTO='{0}' then 1 
                                when wdist.DISTRIBUTIONBY='{0}' then -1 else 0 end) qty from TBL_MASTER_INV mi inner join                                         TBL_WEAPON_DETAIL wd on mi.PK_MASTER_INV_ID = wd.FK_MASTER_INV 
                                left join TBL_WEAPON_DISTRIBUTION wdist on wdist.FK_WEAPON_DETAIL = wd.PK_WEAPONS_ID  and 
                                (wdist.DISTRIBUTIONBY='{0}' or wdist.DISTRIBUTIONTO='{0}') and wdist.status<> 'Inactive' 
                                group by mi.PK_MASTER_INV_ID,mi.ITEMNAME, mi.SPECIFICATION", unitCode);
            return db.Database.SqlQuery<PRISM.Models.spWeaponOnhand>(query).Where(w => w.qty > 0).ToList();
        }

        public List<PRISM.Models.spWeaponOnhand> GetAvailableWeaponInUnitForAdmin(string unitCode)
        {

            string query =
                string.Format(
                    @"SELECT mi.PK_MASTER_INV_ID whguid,mi.ITEMNAME whtype, mi.SPECIFICATION whSPECIFICATION, 
                                sum(case when wdist.DISTRIBUTIONTO='{0}' then 1 
                                when wdist.DISTRIBUTIONBY='{0}' then -1 else 0 end) qty from TBL_MASTER_INV mi left join                                         TBL_WEAPON_DETAIL wd on mi.PK_MASTER_INV_ID = wd.FK_MASTER_INV 
                                left join TBL_WEAPON_DISTRIBUTION wdist on wdist.FK_WEAPON_DETAIL = wd.PK_WEAPONS_ID  and 
                                wdist.status<> 'Inactive' group by mi.PK_MASTER_INV_ID,mi.ITEMNAME, mi.SPECIFICATION", unitCode);
            return db.Database.SqlQuery<PRISM.Models.spWeaponOnhand>(query).Where(w => w.qty >= 0).ToList();
        }

        //public List<PRISM.Models.spWeaponOnhand> getUnitSpecific()
        //{
        //    setVal();
        //    var data = (from mi in db.TBL_MASTER_INV
        //                join wd in
        //                    (
        //                        (from twd in db.TBL_WEAPON_DETAIL
        //                         select new
        //                         {
        //                             twd.PK_WEAPONS_ID,
        //                             twd.FK_MASTER_INV
        //                         })) on new { FK_MASTER_INV = mi.PK_MASTER_INV_ID } equals new { FK_MASTER_INV = wd.FK_MASTER_INV }
        //                join wdist in
        //                    (
        //                        (from twdt in db.TBL_WEAPON_DISTRIBUTION
        //                         where
        //                           twdt.DISTRIBUTIONTO == _DistributedTo &&
        //                           ((twdt.ENTRYTYPE == _entryType) || (twdt.ENTRYTYPE == _entryType2) || (twdt.ENTRYTYPE == _entryType3))
        //                           && twdt.STATUS == "Active"
        //                         group twdt by new
        //                         {
        //                             twdt.FK_WEAPON_DETAIL
        //                             //,twdt.STATUS
        //                         } into g
        //                         select new
        //                         {
        //                             FK_WEAPON_DETAIL = g.Key.FK_WEAPON_DETAIL,
        //                             //qty = g.Key.STATUS == "Active" ? g.Count() : 0
        //                             qty=g.Count()
        //                         })) on new { wd.PK_WEAPONS_ID } equals new { PK_WEAPONS_ID = wdist.FK_WEAPON_DETAIL }
        //                join um in
        //                    (
        //                        (from mu in db.TBL_MEASURINGUNITS_MASTER
        //                         select new
        //                         {
        //                             mu.UNITSHORT,
        //                             mu.PK_UOM_ID
        //                         })) on new { PK_UOM_ID = mi.FK_UOM_ID } equals new { PK_UOM_ID = um.PK_UOM_ID }
        //                group new { mi, um, wdist } by new
        //                {
        //                    mi.ITEMNAME,
        //                    mi.SPECIFICATION,
        //                    um.UNITSHORT,
        //                    mi.PK_MASTER_INV_ID
        //                } into g
        //                select new
        //                {
        //                    g.Key.PK_MASTER_INV_ID,
        //                    g.Key.ITEMNAME,
        //                    g.Key.SPECIFICATION,
        //                    QUANTITY = (System.Int64?)g.Sum(p => p.wdist.qty),
        //                    g.Key.UNITSHORT
        //                }).AsEnumerable().Select(xs => new PRISM.Models.spWeaponOnhand
        //                {
        //                    UNITSHORT = xs.UNITSHORT,
        //                    whtype = xs.ITEMNAME,
        //                    qty = xs.QUANTITY.ToString(),
        //                    whSPECIFICATION = xs.SPECIFICATION,
        //                    whguid = xs.PK_MASTER_INV_ID//.ToString()
        //                }).ToList();
        //    return data


            [SessionExpireFilter]
        public ActionResult WeaponsMaster()
        {
            var weaponList = GetAvailableWeaponInUnitForAdmin(Session["UnitCode"].ToString());
            TempData["list"] = weaponList;
            return View(weaponList);
        }
        [SessionExpireFilter]
        public PartialViewResult CreateOSP()
        {
            //var data = new SelectList(db.measuringUnits_master, "PK_UoM_ID", "unitName");
            ViewBag.UoM = db.TBL_MEASURINGUNITS_MASTER; //data;
            ViewBag.HeadMessage = PRISM.Resource.WeaponTypeHeader;//"Register New Weapons Type";
            return PartialView("iCreate");
        }

        [SessionExpireFilter]
        public PartialViewResult CreateOSP1()
        {
            //var data = new SelectList((db.tbl_Master_INV.Where(w => w.itemType == "Weapon")), "PK_Master_Inv_ID", "itemName");
            //var vendorData = new SelectList((db.tbl_Vendor.Where(w => w.Blocked == 0)), "PK_VendorId", "VendorName");
            ViewBag.WeaponType = db.TBL_MASTER_INV.Where(w => w.ITEMTYPE == "Weapon");
            ViewBag.VendorList = db.TBL_VENDOR.Where(w => w.BLOCKED == 0);
            ViewBag.HeadMessage = PRISM.Resource.AddWeaponHeader;

            return PartialView("iCreateWeapon");
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult FxReturnGoliByPerson(FormCollection value, TBL_WEAPON_DISTRIBUTION wdistribution)
        {
            ReturnGoliInsertUpdate(value, wdistribution);
            return RedirectToAction("FxReturnGoliByPerson");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Create(TBL_MASTER_INV wh, FormCollection value)
        {
            var _entryDate = DateTime.Parse(DateTime.Now.ToShortDateString());
            //var _UOM = new Guid(value["UoM"]);
            PRISM.TBL_MASTER_INV item = new TBL_MASTER_INV
            {
                PK_MASTER_INV_ID = Guid.NewGuid().ToString(),
                ITEMCATEGORYCODE = 47,
                ITEMNAME = wh.ITEMNAME,
                ITEMTYPE = "Weapon",
                SPECIFICATION = wh.SPECIFICATION,
                FK_UOM_ID = wh.FK_UOM_ID,//_UOM,
                UNIXTIMESTAMP = "",
                ENTRYDATE = _entryDate,
                ENTRYBY = int.Parse(User.Identity.Name),
                COMMENTS = wh.COMMENTS
            };
            try
            {
                db.TBL_MASTER_INV.Add(item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ee)
            {
                var exc = ee;
            }

            return View("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult Create1(TBL_WEAPON_DETAIL wl, FormCollection value)
        {
            setVal();
            /*Insert into weapon detail table starts*/
            var _PK_Weapon_detail = Guid.NewGuid();
            var _FK_Master_INV = wl.FK_MASTER_INV; //new Guid(value["WeaponType"]);
            wl.STATUS = "Active";
            wl.FK_MASTER_INV = _FK_Master_INV;
            wl.PK_WEAPONS_ID = _PK_Weapon_detail.ToString();
            wl.OWNER = "PHQ";
            var serialInDb = db.TBL_WEAPON_DETAIL.Where(wd => wd.SERIAL_NO == wl.SERIAL_NO).SingleOrDefault();
            if (serialInDb == null)
            {
                db.TBL_WEAPON_DETAIL.Add(wl);
                db.SaveChanges();
                /*Inser into weapon detail table completed */

                /*insert into purchase table start*/
                try
                {
                    var _entryUnit = this.Session["UnitCode"].ToString();

                    TBL_WEAPON_PURCHASE_INFO purchaseInfo = new TBL_WEAPON_PURCHASE_INFO//();
                    {
                        PK_PURCHASE_ID = Guid.NewGuid().ToString(),
                        FK_WEAPONDETAIL = _PK_Weapon_detail.ToString(),
                        PURCHASEDITEMTYPE = "Weapon",
                        PURCHASEDBY_FK_PERSONCODE = int.Parse(User.Identity.Name),
                        ENTRYUNIT = _entryUnit,
                        QUANTITY = 1,
                        UNITPRICE = wl.PurchaseInfo.UNITPRICE,
                        VAT = wl.PurchaseInfo.VAT,
                        EXTRAEXPENSES = wl.PurchaseInfo.EXTRAEXPENSES,
                        PURCHASEDDATE = wl.PurchaseInfo.PURCHASEDDATE,
                        ENTRYDATE = DateTime.Parse(DateTime.Now.ToShortDateString()),
                        FK_VENDORID = wl.PurchaseInfo.FK_VENDORID 
                    };

                    db.TBL_WEAPON_PURCHASE_INFO.Add(purchaseInfo);
                    db.SaveChanges();

                    var _ASSIGNEDBYTO = int.Parse(User.Identity.Name);
                    /***
                     *  In case of Weapon Distribution / Transferred, the value of ASSIGNEDBYTO is the person who distribute the weapon, especially the logged in user
                     *  In case of Weapon Return, the value of ASSIGNEDBYTO is the unitCode who recieves the returned weapon.
                    ***/
                    TBL_WEAPON_DISTRIBUTION wd = new TBL_WEAPON_DISTRIBUTION
                    {
                        PK_WDISTRIBUTION_ID = Guid.NewGuid().ToString(),
                        FK_WEAPON_DETAIL = _PK_Weapon_detail.ToString(),
                        DISTRIBUTIONBY = "PHQ",//1,
                        DISTRIBUTIONTO = "PHQ",//1,
                        DISTRIBUTEDDATE = wl.PurchaseInfo.PURCHASEDDATE,
						SerialNo=wl.SERIAL_NO,
                        ENTRYTYPE = 1,
                        ASSIGNEDBYTO = _ASSIGNEDBYTO,
                        STATUS = "Active",
                        ENTRYDATE = DateTime.Parse(DateTime.Now.ToShortDateString()),
                        COMMENTS = "",
                    };
                    db.TBL_WEAPON_DISTRIBUTION.Add(wd);
                    db.SaveChanges();
                }
                catch (Exception ex) { }
            }
            else
            {
                //throw serial number already exists
            }

            /*insert into purchase table completed*/
            return RedirectToAction("Index");
        }

        [SessionExpireFilter]
        public PartialViewResult EditMasterWeaponType(string id)
        {
            //var data = new SelectList(db.measuringUnits_master, "PK_UoM_ID", "unitName");
            ViewBag.UoM = db.TBL_MEASURINGUNITS_MASTER; //data;
            ViewBag.HeadMessage = PRISM.Resource.WeaponTypeHeader;//"Register New Weapons Type";
            return PartialView("iEditMasterWeaponType", db.TBL_MASTER_INV.Find(id));
        }

        [SessionExpireFilter]
        public ActionResult MasterWeaponTypeList()
        {
            //var data = new SelectList(db.measuringUnits_master, "PK_UoM_ID", "unitName");
            ViewBag.UoM = db.TBL_MEASURINGUNITS_MASTER; //data;
            ViewBag.HeadMessage = PRISM.Resource.WeaponTypeHeader;//"Register New Weapons Type";
            return View("MasterWeaponTypeList", db.TBL_MASTER_INV.OrderByDescending(w => w.ENTRYDATE));
        }

        [SessionExpireFilter]
        public PartialViewResult EditWeapon(string id)
        {
            ViewBag.WeaponType = db.TBL_MASTER_INV.Where(w => w.ITEMTYPE == "Weapon");
            ViewBag.VendorList = db.TBL_VENDOR.Where(w => w.BLOCKED == 0);
            var weapon = db.TBL_WEAPON_DETAIL.First(w => w.PK_WEAPONS_ID == id);
            weapon.PurchaseInfo = db.TBL_WEAPON_PURCHASE_INFO.First(p => p.FK_WEAPONDETAIL == id);
            return PartialView("iEditWeapon", weapon);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult EditMasterWeaponType(TBL_MASTER_INV wh, FormCollection value)
        {
            var weaponType = db.TBL_MASTER_INV.Find(wh.PK_MASTER_INV_ID);            
                weaponType.ITEMNAME = wh.ITEMNAME;
                weaponType.SPECIFICATION = wh.SPECIFICATION;
                weaponType.FK_UOM_ID = wh.FK_UOM_ID;
                weaponType.COMMENTS = wh.COMMENTS;
            
            try
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
                return RedirectToAction("MasterWeaponTypeList");
            }
            catch (Exception ee)
            {
                var exc = ee;
            }

            return View("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult EditWeapon(TBL_WEAPON_DETAIL weaponDetail)
        {
            var weapon = db.TBL_WEAPON_DETAIL.Find(weaponDetail.PK_WEAPONS_ID);
            var purchaseInfo = db.TBL_WEAPON_PURCHASE_INFO.Find(weaponDetail.PurchaseInfo.PK_PURCHASE_ID);
            weapon.WEAPON_NAME = weaponDetail.WEAPON_NAME;
            weapon.SERIAL_NO = weaponDetail.SERIAL_NO;
            weapon.FK_MASTER_INV = weaponDetail.FK_MASTER_INV;
            weapon.DESCRIPTION = weaponDetail.DESCRIPTION;
            weapon.BOOKPAGEID = weaponDetail.BOOKPAGEID;

            purchaseInfo.UNITPRICE = weaponDetail.PurchaseInfo.UNITPRICE;
            purchaseInfo.VAT = weaponDetail.PurchaseInfo.VAT;
            purchaseInfo.EXTRAEXPENSES = weaponDetail.PurchaseInfo.EXTRAEXPENSES;
            purchaseInfo.PURCHASEDDATE = weaponDetail.PurchaseInfo.PURCHASEDDATE;
            purchaseInfo.FK_VENDORID = weaponDetail.PurchaseInfo.FK_VENDORID;
            db.Configuration.ValidateOnSaveEnabled = false;
            db.SaveChanges();
            return RedirectToAction("WeaponList");
        }

        public ActionResult WeaponList() {
            var weapons = (from w in db.TBL_WEAPON_DETAIL
                           join p in db.TBL_WEAPON_PURCHASE_INFO on w.PK_WEAPONS_ID equals p.FK_WEAPONDETAIL
                           select new { 
                               w,p
                           }).AsEnumerable().Select(s =>
                           new PRISM.TBL_WEAPON_DETAIL
                           {
                               PK_WEAPONS_ID = s.w.PK_WEAPONS_ID,
                               WEAPON_NAME = s.w.WEAPON_NAME,
                               SERIAL_NO = s.w.SERIAL_NO,
                               DESCRIPTION = s.w.DESCRIPTION,
                               BOOKPAGEID = s.w.BOOKPAGEID,
                               PurchaseInfo = s.p
                           }).OrderByDescending(w => w.SERIAL_NO).ToList();
            return View(weapons);
        }

        public JsonResult DoesItemNameExists(string itemName, string PK_MASTER_INV_ID)
        {
            var doesExists = db.TBL_MASTER_INV.Any(item => item.ITEMNAME == itemName && (string.IsNullOrEmpty(PK_MASTER_INV_ID) || item.PK_MASTER_INV_ID != PK_MASTER_INV_ID));
            return Json(!doesExists, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DoesWeaponNameWithSerialExists(string Serial_No, string Weapon_Name, string PK_WEAPONS_ID)
        {
            var doesExists = db.TBL_WEAPON_DETAIL.Any(item => item.SERIAL_NO == Serial_No && item.WEAPON_NAME==Weapon_Name && (string.IsNullOrEmpty(PK_WEAPONS_ID) || item.PK_WEAPONS_ID != PK_WEAPONS_ID));
            return Json(!doesExists, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DoesSerialNumberExists(string Serial_No, string PK_WEAPONS_ID)
        {
            var doesExists = db.TBL_WEAPON_DETAIL.Any(item => item.SERIAL_NO == Serial_No && (string.IsNullOrEmpty(PK_WEAPONS_ID) || item.PK_WEAPONS_ID != PK_WEAPONS_ID));
            return Json(!doesExists, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DoesWeaponNameExists(string Weapon_Name, string PK_WEAPONS_ID)
        {
            var doesExists = db.TBL_WEAPON_DETAIL.Any(item => item.WEAPON_NAME == Weapon_Name && (string.IsNullOrEmpty(PK_WEAPONS_ID) || item.PK_WEAPONS_ID != PK_WEAPONS_ID));
            return Json(!doesExists, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        ///*Rehman Begin*/
        [SessionExpireFilter]
        public ActionResult WeaponToUnit()
        {
            Session["IsWeaponReturn"] = false;
            var _loggedInUnit = this.Session["UnitCode"].ToString();
            //var data = (from a in db.VIEW_POLICE_UNIT
            //            where a.ID != _loggedInUnit
            //            select new Unit() { UnitId = a.ID, UnitName = a.UNIT_NAME }).ToList<Unit>();
            List<Unit> data = null;
            if (Session["LowerUnits"] == null)
            {
                /* FIX: Applied Filter for Units Having Users with Admin and SuperUser Roles Only */
//                data = db.Database.SqlQuery<Unit>(@"select  DISTINCT PU.ID UnitId, PU.UNIT_NAME UnitName FROM VIEW_POLICE_UNIT PU JOIN VIEW_AUTH VA ON PU.UNIT_NAME = VA.UNIT_NAME JOIN
//                                                                        TBL_USER_DETAILS UD ON VA.PERSON_CODE = UD.PERSON_CODE 
//                                                                        JOIN 
//                                                                        TBL_IROLES R ON
//                                                                        UD.FK_ROLEID = R.PK_ROLEID
//                                                                        WHERE PU.ID <> '" + _loggedInUnit + @"' AND R.ROLENAME IN ('Admin', 'Superuser')  CONNECT BY PU.MASTER = PRIOR PU.ID START WITH PU.ID = '" + _loggedInUnit + "'").ToList<Unit>();
                /*data = db.Database.SqlQuery<Unit>(@"SELECT ID UnitId, UNIT_NAME UnitName
                                                       FROM view_police_unit where ID <> '" + _loggedInUnit + @"'
                                                       CONNECT BY  master = PRIOR id
                                                       start with ID = '" + _loggedInUnit + "' ").ToList<Unit>();*/
                data = db.Database.SqlQuery<Unit>(@"select  DISTINCT PU.ID UnitId, PU.UNIT_NAME UnitName FROM VIEW_POLICE_UNIT PU JOIN VIEW_AUTH VA ON PU.UNIT_NAME = VA.UNIT_NAME JOIN
                                                                        TBL_USER_DETAILS UD ON VA.PERSON_CODE = UD.PERSON_CODE 
                                                                        JOIN 
                                                                        TBL_IROLES R ON
                                                                        UD.FK_ROLEID = R.PK_ROLEID
                                                                        WHERE PU.ID <> '" + _loggedInUnit + @"' AND R.ROLENAME IN ('Admin', 'Superuser')").ToList<Unit>();
                Session["LowerUnits"] = data;
            }
            else
                data = Session["LowerUnits"] as List<Unit>;

            ViewBag.UnitList = new SelectList(data, "UnitId", "UnitName");
            var whWeaponType = returnWhWeaponType();
            ViewBag.WeaponTypeList = new SelectList(whWeaponType, "PK_MASTER_INV_ID", "ITEMNAME");            
            return View();
        }

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult WeaponToUnit(TBL_WEAPON_DISTRIBUTION weaponDist, FormCollection val)
        {
            var redirectTo = insertUpdateTblWeaponLine(weaponDist, val);
            return RedirectToAction(redirectTo);
        }

        public JsonResult AutoComplete(string term)
        {
            var data= Session["LowerUnits"] as List<Unit>;
            //var filteredItems = db.VIEW_POLICE_UNIT.Where(file => file.FileName.StartsWith(term)).Select(file => file.FileName);
            var filteredUnits = data.Where(unit => unit.UnitName.StartsWith(term.ToUpper())).Select(unit => new { ID = unit.UnitId, UNIT_NAME = unit.UnitName }).ToList();
            JsonResult result = Json(filteredUnits, JsonRequestBehavior.AllowGet);
            return result;
        }

        [SessionExpireFilter]
        public ActionResult WeaponToPerson()
        {
            //var _loggedInUser = User.Identity.Name;
            //var data = (from Auths in db.VIEW_AUTH.AsEnumerable()
            //            where Auths.PERSON_CODE != _loggedInUser
            //            select new
            //            {
            //                Auths.PERSON_CODE,
            //                PersonName = (Auths.NAME + ", " + Auths.PERSON_CODE + ", " + Auths.UNIT_NAME)
            //            }).ToList();
            //ViewBag.PersonList = new SelectList(data, "PERSON_CODE", "PersonName");
            var whWeaponType = returnWhWeaponType();
            ViewBag.WeaponTypeList = new SelectList(whWeaponType, "PK_MASTER_INV_ID", "ITEMNAME");
            /* ViewBag.WeaponDESCRIPTION = new SelectList(db.tbl_wepons_head, "whguid", "whSPECIFICATION");*/
            return View();
        }

        [SessionExpireFilter]
        public JsonResult AutoCompTransPerson(string s)
        {
            var sq = s.ToUpper().Trim();
            int i = int.TryParse(s, out i) ? i : 0;
            var res = db.VIEW_AUTH.Where(c => c.PERSON_CODE.Equals(sq)).Select(x => new { x.PERSON_CODE, x.NAME, x.UNIT_NAME }).ToList();

            return Json(res, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult WeaponToPerson(TBL_WEAPON_DISTRIBUTION weaponDist, FormCollection val)
        {
            var redirectTo = insertUpdateTblWeaponLine(weaponDist, val);
            return RedirectToAction(redirectTo);
        }

        private IEnumerable<object> returnWhWeaponType()
        {
            setVal();
            var _loggedInUnit = this.Session["UnitCode"].ToString();




            var whWeaponType = (from c1 in db.TBL_MASTER_INV
                                join c2 in db.TBL_WEAPON_DETAIL on c1.PK_MASTER_INV_ID equals c2.FK_MASTER_INV
                                where c2.OWNER == _loggedInUnit
                                select new
                                {
                                    c1.PK_MASTER_INV_ID,
                                    c1.ITEMNAME

                                })
                                .Distinct();
                                
                            
                                //.Select(x =>)
            //var sql = ((System.Data.Entity.Core.Objects.ObjectQuery)whWeaponType)
           // .ToTraceString();
           // string query = @"select * from  TBL_MASTER_INV";
          //  var whWeaponType = (from c in db.TBL_MASTER_INV
                           //     select c).ToList();
           // var whWeaponType =db.TBL_MASTER_INV.ToList();
           // var whWeaponType=
            //var whWeaponType = (from wd in db.TBL_MASTER_INV
            //              join wdis in
            //                  (
            //                      (from tbl_weapon_detail in db.tbl_weapon_detail
            //                       where
            //                         tbl_weapon_detail.ow == _DistributedTo // &&
            //                       //  TBL_WEAPON_DISTRIBUTION.STATUS == "Active" // &&
            //                       // (TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType2 || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType3)
            //                       select new
            //                       {
            //                           TBL_WEAPON_DISTRIBUTION.FK_WEAPON_DETAIL
            //                       })) on new { FK_WEAPON_DETAIL = wd.PK_WEAPONS_ID } equals new { FK_WEAPON_DETAIL = wdis.FK_WEAPON_DETAIL }
            //              where
            //                wd.FK_MASTER_INV == PK_MASTER_INV_ID
            //              select new
            //              {
            //                  wd.WEAPON_NAME,
            //                  wd.SERIAL_NO,
            //                  wd.PK_WEAPONS_ID
            //              });
            //var whWeaponType = db.TBL_MASTER_INV.ToList();
            //var whWeaponType = (from mi in db.TBL_MASTER_INV
            //                    join wd in
            //                        (
            //                            (from TBL_WEAPON_DETAIL in db.TBL_WEAPON_DETAIL
            //                             select new
            //                             {
            //                                 TBL_WEAPON_DETAIL.PK_WEAPONS_ID,
            //                                 TBL_WEAPON_DETAIL.FK_MASTER_INV
            //                             })) on new { FK_MASTER_INV = mi.PK_MASTER_INV_ID } equals new { FK_MASTER_INV = wd.FK_MASTER_INV }
            //                    join wdis in
            //                        (
            //                            (from TBL_WEAPON_DISTRIBUTION in db.TBL_WEAPON_DISTRIBUTION
            //                             where
            //                               TBL_WEAPON_DISTRIBUTION.DISTRIBUTIONTO == _DistributedTo &&
            //                               TBL_WEAPON_DISTRIBUTION.STATUS == "Active" &&
            //                             //  (TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType2 || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType3)
            //                              (TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == 1)// || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType2 || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType3)
            //                             select new
            //                             {
            //                                 TBL_WEAPON_DISTRIBUTION.FK_WEAPON_DETAIL
            //                             })) on new { FK_WEAPON_DETAIL = wd.PK_WEAPONS_ID } equals new { FK_WEAPON_DETAIL = wdis.FK_WEAPON_DETAIL }
            //                    group mi by new
            //                    {
            //                        mi.PK_MASTER_INV_ID,
            //                        mi.ITEMNAME
            //                    } into g
            //                    select new
            //                    {
            //                        g.Key.ITEMNAME,
            //                        PK_MASTER_INV_ID = g.Key.PK_MASTER_INV_ID
            //                    }).AsEnumerable();
            return whWeaponType;
        }




        public ActionResult FxReturnByPerson()
        {

            ViewBag.SubjectName = new SelectList(db.TBL_WEAPON_DETAIL, "WEAPON_NAME", "FK_MASTER_INV");




            return View();

        }


        public ActionResult FxReturnGoliByPerson()
        {

           // ViewBag.SubjectName = new SelectList(db.TBL_WEAPON_DETAIL, "WEAPON_NAME", "FK_MASTER_INV");

            var _distributionBy = this.Session["UnitCode"].ToString();
            var _loggedInUser = User.Identity.Name;
            var personList = (from wdist in db.TBL_WEAPON_DISTRIBUTION
                              where wdist.STATUS == "Transfered" && wdist.DISTRIBUTIONBY == _distributionBy
                              join auth in
                                  (
                                      from temp_auth in db.VIEW_AUTH
                                      where temp_auth.PERSON_CODE != _loggedInUser
                                      select new
                                      {
                                          PERSON_NAME = temp_auth.NAME + ", " + temp_auth.PERSON_CODE + ", " + temp_auth.UNIT_NAME,
                                          temp_auth.PERSON_CODE
                                      }
                                      ) on new { PERSON_CODE = wdist.DISTRIBUTIONTO } equals new { PERSON_CODE = auth.PERSON_CODE }
                              select new
                              {
                                  auth.PERSON_CODE,
                                  auth.PERSON_NAME
                              }).Distinct().ToList();


            //var personList = (from wdist in db.TBL_WEAPON_DISTRIBUTION

            //                  join temp_auth in db.VIEW_AUTH on wdist.DISTRIBUTIONTO equals temp_auth.PERSON_CODE
            //                  join hf in db.TBL_WEAPON_DETAIL on wdist.FK_WEAPON_DETAIL equals hf.PK_WEAPONS_ID
            //                  where hf.OWNER == _loggedInUser
            //                  //new { a1 = wdist.OWNER } equals new { a2=temp_auth.ID}
            //                  where
            //                          wdist.DISTRIBUTIONBY == _distributionBy


            //                  select new
            //                  {
            //                      // wdist.FK_WEAPON_DETAIL,
            //                      // hf.WEAPON_NAME
            //                      temp_auth.PERSON_CODE,
            //                      temp_auth.NAME
            //                      //auth.PERSON_CODE,
            //                      //auth.PERSON_NAME
            //                  }).Distinct().ToList();


            ViewBag.PersonList = new SelectList(personList, "PERSON_CODE", "PERSON_NAME");

            var status = new SelectList(new[] 
                {new {id="Active", name = "Active"},
                new {id="Inactive", name="Inactive"},
                new {id="Waived", name="Waived"}
            }, "id", "name");
            ViewBag.Status = status;

            var status1 = new SelectList(new[] 
                {new {id="Goli", name = "Goli"}
                
            }, "id", "name");
            ViewBag.Status1 = status1;
            return View();

        }


        private string insertUpdateTblWeaponLine(TBL_WEAPON_DISTRIBUTION weaponDist, FormCollection val)
        {
            setVal();
            var test = val["WeaponName"];
            weaponDist.PK_WDISTRIBUTION_ID = Guid.NewGuid().ToString();
            //weaponDist.FK_WEAPON_DETAIL = val["hddWeaponId"].ToString();// new Guid(val["getWeaponName"]);
            weaponDist.DISTRIBUTIONBY = this.Session["UnitCode"].ToString();
            string distributionType = val["hddDistributionType"];
            weaponDist.SerialNo = val["hddSerialNo"];
            weaponDist.DISTRIBUTIONTO = val["hddSelectedUnit"];
            switch (weaponDist.DISTRIBUTIONBY)
            {
                //case 1:
                case "PHQ":
                    {
                        weaponDist.ENTRYTYPE = (distributionType=="Unit") ? Convert.ToByte(2) : Convert.ToByte(3);
                        break;
                    }
                default:
                    {
                        weaponDist.ENTRYTYPE = (distributionType == "Unit") ? Convert.ToByte(4) : Convert.ToByte(5);
                        break;
                    }
            }
            weaponDist.STATUS = "Transfered";
            weaponDist.ENTRYDATE = DateTime.Parse(DateTime.Now.ToShortDateString());
            weaponDist.UNIXDATE = "";
            //try
            //{
            //    //using (TransactionScope transaction = new TransactionScope())
            //    {
                    db.TBL_WEAPON_DISTRIBUTION.Add(weaponDist);
                    db.SaveChanges();


                    //NEW CODE

                    var weapon = db.TBL_WEAPON_DETAIL.Find(weaponDist.FK_WEAPON_DETAIL);
                    weapon.OWNER = weaponDist.DISTRIBUTIONTO;
                    try
                    {
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        // return RedirectToAction("MasterWeaponTypeList");
                    }
                    catch (Exception ee)
                    {
                        var exc = ee;
                    }
                    //db.SaveChanges();
                    //

                    //  var _ENTRYTYPE = (int.Parse(this.Session["UnitCode"].ToString()) == 1)? 1 : 2;

                    //var dbUpdate = db.TBL_WEAPON_DISTRIBUTION.Where(wd => wd.STATUS == "Active" && (wd.ENTRYTYPE == _ENTRYTYPE || wd.ENTRYTYPE == _ENTRYTYPE2)

                    //var dbUpdate = db.TBL_WEAPON_DISTRIBUTION.Where(wd => wd.STATUS == "Active" && (wd.ENTRYTYPE == _entryType || wd.ENTRYTYPE == _entryType2 || wd.ENTRYTYPE == _entryType3)
                    //    && wd.FK_WEAPON_DETAIL == weaponDist.FK_WEAPON_DETAIL && wd.DISTRIBUTIONTO == _DistributedTo).SingleOrDefault();
                    //dbUpdate.STATUS = "Transfered";
                    //dbUpdate.SerialNo = weaponDist.SerialNo;
                    //db.SaveChanges();

                   // transaction.Complete();
            //    }
            //}
            //catch (Exception e)
            //{
            //}
            
            return "Index";
        }
        /*[ValidateAntiForgeryToken]*/
        public JsonResult AutoCompleteWeaponDesc(string PK_MASTER_INV_ID)
        {
            setVal();

            var _loggedInUnit = this.Session["UnitCode"].ToString();
            var result = (from wd in db.TBL_WEAPON_DETAIL
                          
                                   where
                                     wd.OWNER == _loggedInUnit &&
                                     wd.FK_MASTER_INV == PK_MASTER_INV_ID
                                   // (TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType2 || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType3)
                                   
                          select new
                          {
                              wd.WEAPON_NAME,
                              wd.SERIAL_NO,
                              wd.PK_WEAPONS_ID
                          });



            //var result = (from wd in db.TBL_WEAPON_DETAIL
            //              join wdis in
            //                  (
            //                      (from TBL_WEAPON_DISTRIBUTION in db.TBL_WEAPON_DISTRIBUTION
            //                       where
            //                         TBL_WEAPON_DISTRIBUTION.DISTRIBUTIONTO == _DistributedTo // &&
            //                       //  TBL_WEAPON_DISTRIBUTION.STATUS == "Active" // &&
            //                       // (TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType2 || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType3)
            //                       select new
            //                       {
            //                           TBL_WEAPON_DISTRIBUTION.FK_WEAPON_DETAIL
            //                       })) on new { FK_WEAPON_DETAIL = wd.PK_WEAPONS_ID } equals new { FK_WEAPON_DETAIL = wdis.FK_WEAPON_DETAIL }
            //              where
            //                wd.FK_MASTER_INV == PK_MASTER_INV_ID
            //              select new
            //              {
            //                  wd.WEAPON_NAME,
            //                  wd.SERIAL_NO,
            //                  wd.PK_WEAPONS_ID
            //              });



            //var result = (from wd in db.TBL_WEAPON_DETAIL
            //              join wdis in
            //                  (
            //                      (from TBL_WEAPON_DISTRIBUTION in db.TBL_WEAPON_DISTRIBUTION
            //                       where
            //                         TBL_WEAPON_DISTRIBUTION.DISTRIBUTIONTO == _DistributedTo &&
            //                         TBL_WEAPON_DISTRIBUTION.STATUS == "Active" &&
            //                         (TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType2 || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType3)
            //                       select new
            //                       {
            //                           TBL_WEAPON_DISTRIBUTION.FK_WEAPON_DETAIL
            //                       })) on new { FK_WEAPON_DETAIL = wd.PK_WEAPONS_ID } equals new { FK_WEAPON_DETAIL = wdis.FK_WEAPON_DETAIL }
            //              where
            //                wd.FK_MASTER_INV == PK_MASTER_INV_ID
            //              select new
            //              {
            //                  wd.WEAPON_NAME,
            //                  wd.SERIAL_NO,
            //                  wd.PK_WEAPONS_ID
            //              });

//            var result = db.Database.SqlQuery<Unit>(@"select PK_Weapons_ID,Weapon_Name from tbl_weapon_detail D
//inner join tbl_weapon_distribution W
//on d.pk_weapons_id = w.fk_weapon_detail
//where w.distributionto='" + _DistributedTo + "'").ToList<Unit>();
            ViewBag.WeaponList = new SelectList(result.ToArray(), "PK_Weapons_ID", "Weapon_Name");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public ActionResult WeaponItemInfo(string id)
        {
            setVal();
//            var data = db.Database.SqlQuery<PRISM.Models.spWeaponOnhand>(@"SELECT TBL_MASTER_INV.ITEMNAME whtype,
//  TBL_MASTER_INV.ITEMTYPE,
//  TBL_WEAPON_DETAIL.WEAPON_NAME whname,
//  TBL_WEAPON_DETAIL.SERIAL_NO whSerial,
//  TBL_WEAPON_DISTRIBUTION.STATUS status,
//  TBL_WEAPON_DISTRIBUTION.DISTRIBUTIONBY,
//  TBL_WEAPON_DISTRIBUTION.DISTRIBUTIONTO,
//  to_char(TBL_WEAPON_PURCHASE_INFO.PURCHASEDDATE, 'RRRR-MM-DD') whpurchasedate,
//  TBL_WEAPON_DETAIL.DESCRIPTION whSPECIFICATION,
//  TBL_WEAPON_DETAIL.PK_WEAPONS_ID whguid
//FROM TBL_MASTER_INV
//INNER JOIN TBL_WEAPON_DETAIL
//ON TBL_MASTER_INV.PK_MASTER_INV_ID = TBL_WEAPON_DETAIL.FK_MASTER_INV
//INNER JOIN TBL_WEAPON_DISTRIBUTION
//ON TBL_WEAPON_DETAIL.PK_WEAPONS_ID = TBL_WEAPON_DISTRIBUTION.FK_WEAPON_DETAIL
//INNER JOIN TBL_WEAPON_PURCHASE_INFO
//ON TBL_WEAPON_DETAIL.PK_WEAPONS_ID   = TBL_WEAPON_PURCHASE_INFO.FK_WEAPONDETAIL
//WHERE TBL_WEAPON_DISTRIBUTION.STATUS = 'Active'
//AND ((TBL_WEAPON_DISTRIBUTION.DISTRIBUTIONBY = '" + _DistributedTo + @"'
//OR TBL_WEAPON_DISTRIBUTION.DISTRIBUTIONTO = '" + _DistributedTo + @"'))
//AND TBL_MASTER_INV.PK_MASTER_INV_ID  = '" + id + @"'
//AND (TBL_WEAPON_DISTRIBUTION.ENTRYTYPE ='" + _entryType + "' OR TBL_WEAPON_DISTRIBUTION.ENTRYTYPE ='" + _entryType2 + "' OR TBL_WEAPON_DISTRIBUTION.ENTRYTYPE ='" + _entryType3 + "') ").ToList<PRISM.Models.spWeaponOnhand>();
            var data = db.Database.SqlQuery<PRISM.Models.spWeaponOnhand>(@"SELECT TBL_MASTER_INV.ITEMNAME whtype,
  TBL_MASTER_INV.ITEMTYPE,
  TBL_WEAPON_DETAIL.WEAPON_NAME whname,TBL_WEAPON_DETAIL.ENTRYDATE,
  TBL_WEAPON_DETAIL.SERIAL_NO whSerial,
  TBL_WEAPON_DISTRIBUTION.STATUS status,
  TBL_WEAPON_DISTRIBUTION.DISTRIBUTIONBY,
  TBL_WEAPON_DISTRIBUTION.DISTRIBUTIONTO,
  to_char(TBL_WEAPON_PURCHASE_INFO.PURCHASEDDATE, 'RRRR-MM-DD') whpurchasedate,
  TBL_WEAPON_DETAIL.DESCRIPTION whSPECIFICATION,
  TBL_WEAPON_DETAIL.PK_WEAPONS_ID whguid
FROM TBL_MASTER_INV
INNER JOIN TBL_WEAPON_DETAIL
ON TBL_MASTER_INV.PK_MASTER_INV_ID = TBL_WEAPON_DETAIL.FK_MASTER_INV
INNER JOIN TBL_WEAPON_DISTRIBUTION
ON TBL_WEAPON_DETAIL.PK_WEAPONS_ID = TBL_WEAPON_DISTRIBUTION.FK_WEAPON_DETAIL
INNER JOIN TBL_WEAPON_PURCHASE_INFO
ON TBL_WEAPON_DETAIL.PK_WEAPONS_ID   = TBL_WEAPON_PURCHASE_INFO.FK_WEAPONDETAIL
WHERE TBL_WEAPON_DISTRIBUTION.STATUS = 'Active'
--AND ((TBL_WEAPON_DISTRIBUTION.DISTRIBUTIONBY = '" + _DistributedTo + @"'
--OR TBL_WEAPON_DISTRIBUTION.DISTRIBUTIONTO = 'MPC'))
AND TBL_MASTER_INV.PK_MASTER_INV_ID  = '" + id + @"'
--AND (TBL_WEAPON_DISTRIBUTION.ENTRYTYPE ='" + _entryType + "' OR TBL_WEAPON_DISTRIBUTION.ENTRYTYPE ='" + _entryType2 + "' OR TBL_WEAPON_DISTRIBUTION.ENTRYTYPE ='" + _entryType3 + "') ").ToList<PRISM.Models.spWeaponOnhand>();

            return View(data);
        }

        [SessionExpireFilter]
        public PartialViewResult iWeaponItemDetail(string id)
        {
            setVal();
            #region commentted
            //var data = (from wdist in db.TBL_WEAPON_DISTRIBUTION
            //            join wd in
            //                (
            //                    (from tbl_Weapon_Detail in db.TBL_WEAPON_DETAIL
            //                     select new
            //                     {
            //                         tbl_Weapon_Detail.PK_WEAPONS_ID,
            //                         tbl_Weapon_Detail.WEAPON_NAME,
            //                         tbl_Weapon_Detail.SERIAL_NO
            //                     })) on new { PK_Weapons_ID = wdist.FK_WEAPON_DETAIL } equals new { PK_Weapons_ID = wd.PK_WEAPONS_ID }
            //            where
            //              wdist.FK_WEAPON_DETAIL == id &&
            //              (wdist.DISTRIBUTIONBY == _DistributedTo ||
            //              //wdist.DISTRIBUTIONBY != 1 ||
            //              wdist.DISTRIBUTIONBY != "PHQ" ||
            //              wdist.DISTRIBUTIONTO == _DistributedTo)
            //            orderby
            //              wdist.DISTRIBUTEDDATE
            //            select new
            //            {
            //                wd.WEAPON_NAME,
            //                wd.SERIAL_NO,
            //                DistributedBy =
            //                (new int[] { 6, 7 }).Contains(wdist.ENTRYTYPE) ?
            //                   ((from Auths in db.VIEW_AUTH
            //                     where
            //                      Auths.PERSON_CODE == wdist.DISTRIBUTIONBY
            //                     select new
            //                     {
            //                         Auths.NAME
            //                     }).FirstOrDefault().NAME) :
            //                 ((from POLICE_UNIT_VIEW in db.VIEW_POLICE_UNIT
            //                   where
            //                     POLICE_UNIT_VIEW.ID == wdist.DISTRIBUTIONBY
            //                   select new
            //                   {
            //                       POLICE_UNIT_VIEW.UNIT_NAME
            //                   }).FirstOrDefault().UNIT_NAME),
            //                DistributedTo =
            //                (new int[] { 1, 2, 4, 6, 7 }).Contains(wdist.ENTRYTYPE) ?
            //                   ((from POLICE_UNIT_VIEW in db.VIEW_POLICE_UNIT
            //                     where
            //                       POLICE_UNIT_VIEW.ID == wdist.DISTRIBUTIONTO
            //                     select new
            //                     {
            //                         POLICE_UNIT_VIEW.UNIT_NAME
            //                     }).FirstOrDefault().UNIT_NAME) :
            //                    ((from Auths in db.VIEW_AUTH
            //                      where
            //                       Auths.PERSON_CODE == wdist.DISTRIBUTIONTO
            //                      select new
            //                      {
            //                          Auths.NAME
            //                      }).FirstOrDefault().NAME),
            //                wdist.STATUS,
            //                wdist.DISTRIBUTEDDATE
            //            }).AsEnumerable().Select(wdd => new PRISM.Models.WeaponDistributionDetail
            //            {
            //                weaponName = wdd.WEAPON_NAME,
            //                serialNumber = wdd.SERIAL_NO,
            //                distributedBy = wdd.DistributedBy,
            //                distributedTo = wdd.DistributedTo,
            //                status = wdd.STATUS,
            //                DISTRIBUTEDDATE = wdd.DISTRIBUTEDDATE
            //            });

            #endregion
            //var args = new DbParameter[] { new SqlParameter { ParameterName = "Major", Value = "Masters" } };
//            var data = db.Database.SqlQuery<PRISM.Models.WeaponDistributionDetail>(
//                                                    @"select wd.WEAPON_NAME weaponname, 
//                                    --case when wdist.ENTRYTYPE=6 or wdist.ENTRYTYPE=7 then auth.NAME else pu.UNIT_NAME end as DistributedBy,
//                                    --case when wdist.ENTRYTYPE=3 or wdist.ENTRYTYPE=6 or wdist.ENTRYTYPE=8 then auth.NAME else pu.UNIT_NAME end as DistributedTo,
//                                    wdist.DISTRIBUTIONBY,wdist.DISTRIBUTIONTO,
//                                    wd.SERIAL_NO serialNumber, wdist.STATUS,wdist.DISTRIBUTEDDATE DISTRIBUTEDDATE from 
//	                                    TBL_WEAPON_DISTRIBUTION wdist 
//                                    inner join 
//	                                    TBL_WEAPON_DETAIL wd 
//                                    on
//	                                    wdist.FK_WEAPON_DETAIL = wd.PK_WEAPONS_ID 
//                                   -- left outer join
//	                                 --   VIEW_AUTH auth
//                                    --on
//	                                  --  auth.PERSON_CODE=wdist.DISTRIBUTIONBY
//                                    --left outer join
//	                                  --  VIEW_POLICE_UNIT pu
//                                    --on
//	                                  --  pu.ID=wdist.DISTRIBUTIONBY
//                                    where 
// 	                                    wdist.FK_WEAPON_DETAIL = '" + id + @"' 
//                                    AND (wdist.ENTRYTYPE <> '1')	
//                                       -- AND (wdist.DISTRIBUTIONBY = '" + _DistributedTo + @"' OR wdist.DISTRIBUTIONBY <> 'PHQ' OR wdist.DISTRIBUTIONTO = '" + _DistributedTo + @"' )	
//                                    order by
// 	                                    wdist.DISTRIBUTEDDATE"
            var data = db.Database.SqlQuery<PRISM.Models.WeaponDistributionDetail>(
                @"select wd.WEAPON_NAME weaponname,wd.ENTRYDATE, 
                                    --case when wdist.ENTRYTYPE=6 or wdist.ENTRYTYPE=7 then auth.NAME else pu.UNIT_NAME end as DistributedBy,
                                    --case when wdist.ENTRYTYPE=3 or wdist.ENTRYTYPE=6 or wdist.ENTRYTYPE=8 then auth.NAME else pu.UNIT_NAME end as DistributedTo,
                                    wdist.DISTRIBUTIONBY DistributedBy,wdist.DISTRIBUTIONTO DistributedTo,
                                    wd.SERIAL_NO serialNumber, wdist.STATUS,wdist.DISTRIBUTEDDATE DISTRIBUTEDDATE from 
	                                    TBL_WEAPON_DISTRIBUTION wdist 
                                    inner join 
	                                    TBL_WEAPON_DETAIL wd 
                                    on
	                                    wdist.FK_WEAPON_DETAIL = wd.PK_WEAPONS_ID 
                                   -- left outer join
	                                 --   VIEW_AUTH auth
                                    --on
	                                  --  auth.PERSON_CODE=wdist.DISTRIBUTIONBY
                                    --left outer join
	                                  --  VIEW_POLICE_UNIT pu
                                    --on
	                                  --  pu.ID=wdist.DISTRIBUTIONBY
                                    where 
 	                                    wdist.FK_WEAPON_DETAIL = '" + id + @"' 
                                       AND wdist.ENTRYTYPE <> 1
                                    order by
 	                                    wdist.DISTRIBUTEDDATE"
                );
            
            return PartialView(data);
        }

        public ActionResult UpdateData(string id)
        {
            try
            {
                var query = (from alx in db.TBL_MASTER_INV
                             join
                             blx in db.TBL_WEAPON_DETAIL
                             on new { masterinv = alx.PK_MASTER_INV_ID } equals new { masterinv = blx.FK_MASTER_INV }
                             join
                             clx in db.TBL_WEAPON_DISTRIBUTION
                             on new { distd = blx.PK_WEAPONS_ID } equals new { distd = clx.FK_WEAPON_DETAIL }
                             join
                             dlx in db.TBL_WEAPON_PURCHASE_INFO
                             on new { pux = blx.PK_WEAPONS_ID } equals new { pux = dlx.FK_WEAPONDETAIL }
                             where blx.SERIAL_NO == id &&
                             clx.STATUS == "Active"
                             select new
                             {alx.PK_MASTER_INV_ID,
                                 blx.PK_WEAPONS_ID,
                                 clx.PK_WDISTRIBUTION_ID,
                                 dlx.PK_PURCHASE_ID,
                                 alx.ITEMCATEGORYCODE,
                                 alx.ITEMNAME,
                                 alx.ITEMTYPE,
                                 alx.SPECIFICATION,
                                 blx.WEAPON_NAME,
                                 
                                 blx.DESCRIPTION,
                                 blx.SERIAL_NO,
                                 blx.BOOKPAGEID,
                                 blx.STATUS,
                                 clx.DISTRIBUTIONBY,
                                 clx.DISTRIBUTIONTO,
                                 clx.ASSIGNEDBYTO,
                                 clx.DISTRIBUTEDDATE,
                                 dlx.EXTRAEXPENSES,
                                 dlx.PURCHASEDDATE,
                                 dlx.UNITPRICE
                             }).AsEnumerable().Select(xs => new PRISM.Models.spWeaponsDetail
                             {
                                 PK_WEAPONS_ID = xs.PK_WEAPONS_ID,
                                pk_master_inv_id = xs.PK_MASTER_INV_ID,
                                pk_wdistribution_id= xs.PK_WDISTRIBUTION_ID,
                                pk_purchase_id = xs.PK_PURCHASE_ID,
                                 ITEMCATEGORYCODE = (xs.ITEMCATEGORYCODE).ToString(),
                                ITEMNAME= xs.ITEMNAME,
                                ITEMTYPE= xs.ITEMTYPE,
                                 SPECIFICATION = xs.SPECIFICATION,
                                WEAPON_NAME = xs.WEAPON_NAME,
                                DESCRIPTION= xs.DESCRIPTION,
                                SERIAL_NO =xs.SERIAL_NO,
                                BOOKPAGEID =xs.BOOKPAGEID.ToString(),
                                STATUS =xs.STATUS,
                                 DISTRIBUTIONBY = xs.DISTRIBUTIONBY,
                                 DISTRIBUTIONTO = xs.DISTRIBUTIONTO,
                                ASSIGNEDBYTO= xs.ASSIGNEDBYTO.ToString(),
                                DISTRIBUTEDDATE= xs.DISTRIBUTEDDATE.HasValue?xs.DISTRIBUTEDDATE.Value.ToString("yyyy-MM-dd"):"",
                                EXTRAEXPENSES = xs.EXTRAEXPENSES.ToString(),
                                PURCHASEDDATE = xs.PURCHASEDDATE.HasValue?xs.PURCHASEDDATE.Value.ToString("yyyy-MM-dd"):"",
                                UNITPRICE=xs.UNITPRICE.ToString() }).FirstOrDefault();
                var status = new SelectList(new[] 
                {new {id="Active", name = "Active"},
                new {id="Inactive", name="Inactive"},
                new {id="Waived", name="Waived"}
            }, "id", "name");

                ViewBag.Status = status;
                return View(query);

            }
            catch (Exception ex)
            {

                return View();
            }
           
            
        }
        //now working
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateData(PRISM.Models.spWeaponsDetail det,FormCollection val) {

            
                ////alx
                var x1 = db.TBL_MASTER_INV.Find(det.pk_master_inv_id);
                x1.ITEMNAME = det.ITEMNAME;
                x1.SPECIFICATION = det.SPECIFICATION;
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();

                ////blx
                var x2 = db.TBL_WEAPON_DETAIL.Find(det.PK_WEAPONS_ID);
                x2.BOOKPAGEID = int.Parse(det.BOOKPAGEID);
                x2.STATUS = det.STATUS;
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();

                ////clx
                var x3 = db.TBL_WEAPON_DISTRIBUTION.Find(det.pk_wdistribution_id);
                x3.DISTRIBUTEDDATE = Convert.ToDateTime(det.DISTRIBUTEDDATE);
                db.Configuration.ValidateOnSaveEnabled = false;
                db.SaveChanges();
               
            

            return RedirectToAction("WeaponsMaster");
        }
 
        
        [SessionExpireFilter]
        public ActionResult ByPerson()
        {
            var _distributionBy = this.Session["UnitCode"].ToString();
            var _loggedInUser = User.Identity.Name;
            //var personList = (from wdist in db.TBL_WEAPON_DISTRIBUTION
            //                  where wdist.STATUS == "Transfered" && wdist.DISTRIBUTIONBY == _distributionBy
            //                  join auth in
            //                      (
            //                          from temp_auth in db.VIEW_AUTH
            //                          where temp_auth.PERSON_CODE != _loggedInUser
            //                          select new
            //                          {
            //                              PERSON_NAME = temp_auth.NAME + ", " + temp_auth.PERSON_CODE + ", " + temp_auth.UNIT_NAME,
            //                              temp_auth.PERSON_CODE
            //                          }
            //                          ) on new { PERSON_CODE = wdist.DISTRIBUTIONTO } equals new { PERSON_CODE = auth.PERSON_CODE }
            //                  select new
            //                  {
            //                      auth.PERSON_CODE,
            //                      auth.PERSON_NAME
            //                  }).Distinct().ToList();


            var personList = (from wdist in db.TBL_WEAPON_DISTRIBUTION

                            join temp_auth in db.VIEW_AUTH on wdist.DISTRIBUTIONTO equals temp_auth.PERSON_CODE
                            join hf in db.TBL_WEAPON_DETAIL on wdist.FK_WEAPON_DETAIL equals hf.PK_WEAPONS_ID
                            where hf.OWNER == _loggedInUser
                            //new { a1 = wdist.OWNER } equals new { a2=temp_auth.ID}
                            where
                                    wdist.DISTRIBUTIONBY == _distributionBy


                            select new
                            {
                               // wdist.FK_WEAPON_DETAIL,
                               // hf.WEAPON_NAME
                                temp_auth.PERSON_CODE,
                                temp_auth.NAME
                                //auth.PERSON_CODE,
                                //auth.PERSON_NAME
                            }).Distinct().ToList();


            //var personList = (from wdist in db.TBL_WEAPON_DETAIL
            //                 // where wdist.OWNER == "Transfered" && wdist.DISTRIBUTIONBY == _distributionBy
            //                  join auth in
            //                      (
            //                          from temp_auth in db.VIEW_AUTH
            //                          where temp_auth.ID == _distributionBy
            //                          select new
            //                          {
            //                              PERSON_NAME = temp_auth.NAME + ", " + temp_auth.PERSON_CODE + ", " + temp_auth.UNIT_NAME,
            //                              temp_auth.PERSON_CODE
            //                          }
            //                          ) on new { PERSON_CODE = wdist.OWNER } equals new { PERSON_CODE = auth.PERSON_CODE }
            //                  select new
            //                  {
            //                      auth.PERSON_CODE,
            //                      auth.PERSON_NAME
            //                  }).Distinct().ToList();

            ViewBag.PersonList = new SelectList(personList, "PERSON_CODE", "NAME");

           

            var status = new SelectList(new[] 
                {new {id="Active", name = "Active"},
                new {id="Inactive", name="Inactive"},
                new {id="Waived", name="Waived"}
            }, "id", "name");



            //var FxWeapon = (from wdist in db.TBL_WEAPON_DETAIL
            //                join temp_auth in db.VIEW_AUTH on

            //                wdist.OWNER equals temp_auth.ID

            //                //new { a1 = wdist.OWNER } equals new { a2=temp_auth.ID}
            //                where
            //                        temp_auth.ID == _distributionBy


            //                select new
            //                {
            //                    wdist.FK_MASTER_INV,
            //                    wdist.WEAPON_NAME
            //                    //auth.PERSON_CODE,
            //                    //auth.PERSON_NAME
            //                }).Distinct().ToList();
            var FxWeapon = (from wdist in db.TBL_WEAPON_DISTRIBUTION
                            
                            join temp_auth in db.VIEW_AUTH on wdist.DISTRIBUTIONTO equals temp_auth.PERSON_CODE
                            join hf in db.TBL_WEAPON_DETAIL on  wdist.FK_WEAPON_DETAIL equals hf.PK_WEAPONS_ID
                            where hf.OWNER == _loggedInUser
                                //new { a1 = wdist.OWNER } equals new { a2=temp_auth.ID}
                            where
                                    wdist.DISTRIBUTIONBY == _distributionBy


                            select new
                            {
                                wdist.FK_WEAPON_DETAIL,
                                hf.WEAPON_NAME
                                
                                //auth.PERSON_CODE,
                                //auth.PERSON_NAME
                            }).Distinct().ToList();


           //   ).ToList();

            //var FxWeapon = (from wdist in db.TBL_WEAPON_DETAIL
            //                where wdist.OWNER == _loggedInUser
            //                  join auth in
            //                      (
            //                          from temp_auth in db.VIEW_AUTH
            //                          where temp_auth.PERSON_CODE == _loggedInUser
            //                          select new
            //                          {
            //                              PERSON_NAME = temp_auth.NAME + ", " + temp_auth.PERSON_CODE + ", " + temp_auth.UNIT_NAME,
            //                              temp_auth.PERSON_CODE
            //                          }
            //                          ) on new { PERSON_CODE = wdist.OWNER } equals new { PERSON_CODE = auth.PERSON_CODE }
            //                  select new
            //                  {
            //                      wdist.FK_MASTER_INV,
            //                      wdist.WEAPON_NAME
            //                  }).ToList();

            ViewBag.FxWeapon = new SelectList(FxWeapon, "FK_WEAPON_DETAIL", "WEAPON_NAME");


            var FxWeaponS = (from wdist in db.TBL_WEAPON_DISTRIBUTION

                            join temp_auth in db.VIEW_AUTH on wdist.DISTRIBUTIONTO equals temp_auth.PERSON_CODE
                            join hf in db.TBL_WEAPON_DETAIL on wdist.FK_WEAPON_DETAIL equals hf.PK_WEAPONS_ID
                             where hf.OWNER == _loggedInUser
                            //new { a1 = wdist.OWNER } equals new { a2=temp_auth.ID}
                            where
                                    wdist.DISTRIBUTIONBY == _distributionBy


                            select new
                            {
                                wdist.FK_WEAPON_DETAIL,
                                hf.SERIAL_NO
                                //auth.PERSON_CODE,
                                //auth.PERSON_NAME
                            }).Distinct().ToList();

            ViewBag.FxWeaponS = new SelectList(FxWeaponS, "FK_WEAPON_DETAIL", "SERIAL_NO");

            ViewBag.Status = status;
            return View();
        }

        [SessionExpireFilter]
        public ActionResult ByUnit()
        {
            Session["IsWeaponReturn"] = true;
            var _distributionBy = this.Session["UnitCode"].ToString();
            //var unitList = (from wdist in db.TBL_WEAPON_DISTRIBUTION
            //                where wdist.STATUS == "Active" && wdist.DISTRIBUTIONBY == _distributionBy
            //                join Police_Unit in
            //                    (
            //                        from pu in db.VIEW_POLICE_UNIT
            //                        where pu.ID != _distributionBy 
            //                        select new
            //                        {
            //                            pu.UNIT_NAME,
            //                            pu.ID
            //                        }
            //                        ) on new { Id = wdist.DISTRIBUTIONTO } equals new { Id = Police_Unit.ID }
            //                select new
            //                {
            //                    Police_Unit.ID,
            //                    Police_Unit.UNIT_NAME
            //                }).Distinct().ToList();

            string query = @"SELECT 
                                    rownum R_NUM,
                                    ID,
                                    UNIT_NAME,
                                    UNIT_NAME_NP,
                                    MASTER,
                                    RID
                                    FROM
                                       view_police_unit
                                    CONNECT BY
                                        ID = PRIOR MASTER
                                    START WITH
                                       ID = '" + Session["UnitCode"].ToString() + "' order by R_NUM desc";

            var unitList = db.Database.SqlQuery<VIEW_POLICE_UNIT>(query);

            ViewBag.UnitList = new SelectList(unitList, "ID", "UNIT_NAME");
            var status = new SelectList(new[] 
                {new {id="Active", name = "Active"},
                new {id="Inactive", name="Inactive"},
                new {id="Waived", name="Waived"}
            }, "id", "name");

            ViewBag.Status = status;
            return View();
        }
        
        /**
         *  entry type for returned weawpon
         *  PERSON => PHQ      6
         *  PERSON => UNIT      7
         *  UNIT => PHQ        8
         * 
         * During return of weapon, the column 'DistributionBy' in tbl_Weapon_Distribution works as 'ReturnedBy' where it may store id of a person or other Unit (not PHQ)
         * the column 'DistributionTo' works as 'ReturnedTo' where it may store id of PHQ or other Unit
         * the column 'AssignedByTO' works as 'AssignedTo', meaning the weapon is returned to logged in user of ReturnedTo Unit.
         * ***/
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult ByPerson(FormCollection value, TBL_WEAPON_DISTRIBUTION wdistribution)
        {
            ReturnInsertUpdate(value, wdistribution);
            return RedirectToAction("ByPerson");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionExpireFilter]
        public ActionResult ByUnit(FormCollection value, TBL_WEAPON_DISTRIBUTION wdistribution)
        {
            if(value["UnitList"]==null)
                ModelState.AddModelError("UnitList","Please select unit");
            ReturnInsertUpdate(value, wdistribution);
            return RedirectToAction("ByUnit");
        }

        public JsonResult AutoCompleteForReturnToUnit(string term)
        {

            string query = @"SELECT 
                                    rownum R_NUM,
                                    ID,
                                    UNIT_NAME,
                                    UNIT_NAME_NP,
                                    MASTER,
                                    RID
                                    FROM
                                       view_police_unit
                                    CONNECT BY
                                        ID = PRIOR MASTER
                                    START WITH
                                       ID = '" + Session["UnitCode"].ToString() + "' order by R_NUM desc";
            var data = db.Database.SqlQuery<VIEW_POLICE_UNIT>(query);
            //TempData["ParentUnits"] = data;
            
            var filteredUnits = data.Where(unit => unit.UNIT_NAME.StartsWith(term.ToUpper())).Select(unit => new { unit.ID, unit.UNIT_NAME });
            JsonResult result = Json(filteredUnits, JsonRequestBehavior.AllowGet);
            return result;
        }

        public void ReturnInsertUpdate(FormCollection value, TBL_WEAPON_DISTRIBUTION wdistribution)
        {
            try
            {
                //in case of return by person, the unit transfering the weapon will enter the data (return from person to unit means perspon distribute to unit as a Returned status)
                //in case of return by unit, the unit itself enter the data (returning back to the unit from unit means unit distribute to other unit(actual distributor) as a Returned status)

                wdistribution.PK_WDISTRIBUTION_ID = Guid.NewGuid().ToString();
               // wdistribution.FK_Weapon_detail = new Guid(value["getWeaponName"]);
               // wdistribution.DistributionBy = int.Parse((value.AllKeys.Contains("UnitList")) ? value["UnitList"] : value["PersonList"]);
                wdistribution.DISTRIBUTIONBY = value["hddDistributionType"] == "Unit" ? Session["UnitCode"].ToString() : wdistribution.DISTRIBUTIONBY;
                wdistribution.DISTRIBUTIONTO = value["hddDistributionType"] == "Unit" ? value["hddSelectedUnit"] : Session["UnitCode"].ToString();
                //wdistribution.DISTRIBUTIONTO = value["hddSelectedUnit"];
                wdistribution.ASSIGNEDBYTO = int.Parse(User.Identity.Name);
                
                switch (wdistribution.DISTRIBUTIONTO)
                {
                    //case 1:
                    case "PHQ":
                        {
                            //wdistribution.EntryType = Convert.ToByte((value.AllKeys.Contains("UnitList")) ? 8 : 6);
                            wdistribution.ENTRYTYPE = Convert.ToByte(value["hddDistributionType"] == "Unit" ? 8 : 6);
                            break;
                        }
                    default:
                        {
                            wdistribution.ENTRYTYPE = 7;
                            break;
                        }
                }
                //wdistribution.Status = value["Status"];
                wdistribution.ENTRYDATE = DateTime.Parse(DateTime.Now.ToShortDateString());
                wdistribution.STATUS = "Returned";

                db.TBL_WEAPON_DISTRIBUTION.Add(wdistribution);
                db.SaveChanges();


                var weapon = db.TBL_WEAPON_DETAIL.Find(wdistribution.FK_WEAPON_DETAIL);
                weapon.OWNER = wdistribution.DISTRIBUTIONTO;
                try
                {
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.SaveChanges();
                    // return RedirectToAction("MasterWeaponTypeList");
                }
                catch (Exception ee)
                {
                    var exc = ee;
                }



                //var updateData = (value.AllKeys.Contains("UnitList")) ? db.tbl_Weapon_Distribution.Where(wdist => wdist.DistributionTo == wdistribution.DistributionBy &&
                //    (wdist.EntryType == 2 || wdist.EntryType == 4 || wdist.EntryType == 7) && wdist.Status == "Active").SingleOrDefault()
                //    : db.tbl_Weapon_Distribution.Where(wdist => wdist.DistributionTo == wdistribution.DistributionBy && (wdist.EntryType == 3 || wdist.EntryType == 5)
                //    && wdist.Status == "Active").SingleOrDefault();
                //var updateData = (value["hddDistributionType"]=="Unit") ? db.TBL_WEAPON_DISTRIBUTION.Where(wdist => wdist.DISTRIBUTIONTO == wdistribution.DISTRIBUTIONBY &&
                //    (wdist.ENTRYTYPE == 2 || wdist.ENTRYTYPE == 4 || wdist.ENTRYTYPE == 7) && wdist.STATUS == "Active").SingleOrDefault()
                //    : db.TBL_WEAPON_DISTRIBUTION.Where(wdist => wdist.DISTRIBUTIONTO == wdistribution.DISTRIBUTIONBY && (wdist.ENTRYTYPE == 3 || wdist.ENTRYTYPE == 5)
                //    && wdist.STATUS == "Active").SingleOrDefault();
                //if (updateData != null)
                //{
                //    updateData.STATUS = "Returned";
                //    db.SaveChanges();
                //}
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                //return View(wdistribution);
            }
        }







        public void ReturnGoliInsertUpdate(FormCollection value, TBL_WEAPON_DISTRIBUTION wdistribution)
        {
            try
            {
                //in case of return by person, the unit transfering the weapon will enter the data (return from person to unit means perspon distribute to unit as a Returned status)
                //in case of return by unit, the unit itself enter the data (returning back to the unit from unit means unit distribute to other unit(actual distributor) as a Returned status)

                wdistribution.PK_WDISTRIBUTION_ID = Guid.NewGuid().ToString();
                // wdistribution.FK_Weapon_detail = new Guid(value["getWeaponName"]);
                // wdistribution.DistributionBy = int.Parse((value.AllKeys.Contains("UnitList")) ? value["UnitList"] : value["PersonList"]);
                wdistribution.DISTRIBUTIONBY = value["hddDistributionType"] == "Unit" ? Session["UnitCode"].ToString() : wdistribution.DISTRIBUTIONBY;
                wdistribution.DISTRIBUTIONTO = value["hddDistributionType"] == "Unit" ? value["hddSelectedUnit"] : Session["UnitCode"].ToString();
                //wdistribution.DISTRIBUTIONTO = value["hddSelectedUnit"];
                wdistribution.ASSIGNEDBYTO = int.Parse(User.Identity.Name);

                switch (wdistribution.DISTRIBUTIONTO)
                {
                    //case 1:
                    case "PHQ":
                        {
                            //wdistribution.EntryType = Convert.ToByte((value.AllKeys.Contains("UnitList")) ? 8 : 6);
                            wdistribution.ENTRYTYPE = Convert.ToByte(value["hddDistributionType"] == "Unit" ? 8 : 6);
                            break;
                        }
                    default:
                        {
                            wdistribution.ENTRYTYPE = 7;
                            break;
                        }
                }
                //wdistribution.Status = value["Status"];
                wdistribution.ENTRYDATE = DateTime.Parse(DateTime.Now.ToShortDateString());
                wdistribution.STATUS = "Returned";

                db.TBL_WEAPON_DISTRIBUTION.Add(wdistribution);
                db.SaveChanges();


                var weapon = db.TBL_WEAPON_DETAIL.Find(wdistribution.FK_WEAPON_DETAIL);
                weapon.OWNER = wdistribution.DISTRIBUTIONTO;
                try
                {
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.SaveChanges();
                    // return RedirectToAction("MasterWeaponTypeList");
                }
                catch (Exception ee)
                {
                    var exc = ee;
                }



                //var updateData = (value.AllKeys.Contains("UnitList")) ? db.tbl_Weapon_Distribution.Where(wdist => wdist.DistributionTo == wdistribution.DistributionBy &&
                //    (wdist.EntryType == 2 || wdist.EntryType == 4 || wdist.EntryType == 7) && wdist.Status == "Active").SingleOrDefault()
                //    : db.tbl_Weapon_Distribution.Where(wdist => wdist.DistributionTo == wdistribution.DistributionBy && (wdist.EntryType == 3 || wdist.EntryType == 5)
                //    && wdist.Status == "Active").SingleOrDefault();
                //var updateData = (value["hddDistributionType"]=="Unit") ? db.TBL_WEAPON_DISTRIBUTION.Where(wdist => wdist.DISTRIBUTIONTO == wdistribution.DISTRIBUTIONBY &&
                //    (wdist.ENTRYTYPE == 2 || wdist.ENTRYTYPE == 4 || wdist.ENTRYTYPE == 7) && wdist.STATUS == "Active").SingleOrDefault()
                //    : db.TBL_WEAPON_DISTRIBUTION.Where(wdist => wdist.DISTRIBUTIONTO == wdistribution.DISTRIBUTIONBY && (wdist.ENTRYTYPE == 3 || wdist.ENTRYTYPE == 5)
                //    && wdist.STATUS == "Active").SingleOrDefault();
                //if (updateData != null)
                //{
                //    updateData.STATUS = "Returned";
                //    db.SaveChanges();
                //}
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                //return View(wdistribution);
            }
        }


        public JsonResult WeapponDistributedToPerson(string personCode, int entryType, int entryType2)
        {
            string distributedByUnit = Session["UnitCode"].ToString();
            var returnedWeapon =
                db.TBL_WEAPON_DISTRIBUTION.Where(
                    wdist => wdist.STATUS == "Returned").Select(x => x.FK_WEAPON_DETAIL);
            var weaponList = (from wdist in db.TBL_WEAPON_DISTRIBUTION
                              where wdist.DISTRIBUTIONTO == personCode && wdist.DISTRIBUTIONBY == distributedByUnit 
                              && wdist.STATUS == "Transfered" && !returnedWeapon.Contains(wdist.FK_WEAPON_DETAIL)
                              join wd in
                                  (
                                      from temp_wd in db.TBL_WEAPON_DETAIL
                                      select new
                                      {
                                          temp_wd.WEAPON_NAME,
                                          temp_wd.PK_WEAPONS_ID,
                                          temp_wd.SERIAL_NO
                                      }
                                      ) on new { PK_Weapons_ID = wdist.FK_WEAPON_DETAIL } equals new { PK_Weapons_ID = wd.PK_WEAPONS_ID }
                              select new
                              {
                                  wd.WEAPON_NAME,
                                  wd.PK_WEAPONS_ID,
                                  wd.SERIAL_NO
                              });
            return Json(weaponList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult WeaponsDistributedByUnit(string distributedByUnit, int entryType, int entryType2)
        {
            string distributedToUnit = Session["UnitCode"].ToString();


            //var returnedWeapon =
            //    db.TBL_WEAPON_DISTRIBUTION.Where(
            //        wdist => wdist.STATUS == "Returned").Select(x=>x.FK_WEAPON_DETAIL);
            //var weaponList = (from wdist in db.TBL_WEAPON_DISTRIBUTION
            //                  where wdist.DISTRIBUTIONBY == distributedByUnit && wdist.DISTRIBUTIONTO == distributedToUnit
            //                  && wdist.STATUS == "Transfered" && !returnedWeapon.Contains(wdist.FK_WEAPON_DETAIL)
            //                  join wd in
            //                      (
            //                          from temp_wd in db.TBL_WEAPON_DETAIL
            //                          select new
            //                          {
            //                              temp_wd.WEAPON_NAME,
            //                              temp_wd.PK_WEAPONS_ID,
            //                              temp_wd.SERIAL_NO
            //                          }
            //                          ) on new { PK_Weapons_ID = wdist.FK_WEAPON_DETAIL } equals new { PK_Weapons_ID = wd.PK_WEAPONS_ID }
            //                  select new
            //                  {
            //                      wd.WEAPON_NAME,
            //                      wd.PK_WEAPONS_ID,
            //                      wd.SERIAL_NO
            //                  });


            var _loggedInUnit = this.Session["UnitCode"].ToString();
            var weaponList = (from wd in db.TBL_WEAPON_DETAIL

                          where
                            wd.OWNER == _loggedInUnit //&&
                            //wd.FK_MASTER_INV == PK_MASTER_INV_ID
                          // (TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType2 || TBL_WEAPON_DISTRIBUTION.ENTRYTYPE == _entryType3)

                          select new
                          {
                              wd.WEAPON_NAME,
                              wd.SERIAL_NO,
                              wd.PK_WEAPONS_ID
                          });

            return Json(weaponList, JsonRequestBehavior.AllowGet);
        }

        
        /*Rehman End*/
        [SessionExpireFilter]
        public ActionResult SearchWeapon()
        {
            return PartialView("iSearchWeapon");
        }

        [SessionExpireFilter]
        public ActionResult DisplaySearchWeapon(FormCollection val)
        {
            var param = val["wcode"].ToUpper();

            var data = db.Database.SqlQuery<PRISM.Models.WeaponDistributionDetail>(
                                                   @"select wd.WEAPON_NAME weaponname, 
                                    case when wdist.ENTRYTYPE in (2,4) then pu.UNIT_NAME
                                    when wdist.ENTRYTYPE in (3,5) then auth.NAME
                                    else '' end as DistributedTo,
                                    unitby.UNIT_NAME as DistributedBy,
                                    wd.SERIAL_NO serialNumber, wdist.STATUS,wdist.DISTRIBUTEDDATE DISTRIBUTEDDATE from 
	                                    TBL_WEAPON_DISTRIBUTION wdist 
                                    inner join 
	                                    TBL_WEAPON_DETAIL wd on
	                                    wdist.FK_WEAPON_DETAIL = wd.PK_WEAPONS_ID 
                                    left outer join
	                                    VIEW_AUTH auth on
	                                    auth.PERSON_CODE=wdist.DISTRIBUTIONTO and wdist.ENTRYTYPE in (3,5)
                                    left outer join
	                                    VIEW_POLICE_UNIT pu
                                    on
	                                    pu.ID=wdist.DISTRIBUTIONTO and wdist.ENTRYTYPE in (2,4)
                                    left outer join
	                                    VIEW_POLICE_UNIT unitby
                                    on
	                                    unitby.ID=wdist.DISTRIBUTIONBY 
                                    where 
 	                                   upper(wd.SERIAL_NO) like '" + param + @"%' or 
                                     upper(wd.WEAPON_NAME) like '" + param + @"%'
                                    order by
 	                                    wdist.DISTRIBUTEDDATE"
               );

            return PartialView("iDisplaySearch", data);
            
        }

        [SessionExpireFilter]
        public ActionResult GetSearchResultDetail(string id,string s)
        {
            Guid g;
            ViewBag.Show = "";
            if (s == "Serial")
            {
                if (Guid.TryParse(id, out g))
                {
                    ViewBag.Show = "Serial";
                    var data = (from wdist in db.TBL_WEAPON_DISTRIBUTION
                                join wd in
                                    (
                                        (from tbl_Weapon_Detail in db.TBL_WEAPON_DETAIL
                                         select new
                                         {
                                             tbl_Weapon_Detail.PK_WEAPONS_ID,
                                             tbl_Weapon_Detail.WEAPON_NAME,
                                             tbl_Weapon_Detail.SERIAL_NO
                                         })) on new { PK_Weapons_ID = wdist.FK_WEAPON_DETAIL } equals new { PK_Weapons_ID = wd.PK_WEAPONS_ID }
                                where
                                  wdist.FK_WEAPON_DETAIL == id
                                //&&
                                //(wdist.DistributionBy == _DistributedTo ||
                                //wdist.DistributionBy != 1 ||
                                //wdist.DistributionTo == _DistributedTo)
                                orderby
                                  wdist.DISTRIBUTEDDATE
                                select new
                                {
                                    wd.WEAPON_NAME,
                                    wd.SERIAL_NO,
                                    DistributedBy =
                                    (new int[] { 6, 7 }).Contains(wdist.ENTRYTYPE) ?
                                       ((from Auths in db.VIEW_AUTH
                                         where
                                          Auths.PERSON_CODE == wdist.DISTRIBUTIONBY
                                         select new
                                         {
                                             Auths.NAME
                                         }).FirstOrDefault().NAME) :
                                     ((from POLICE_UNIT_VIEW in db.VIEW_POLICE_UNIT
                                       where
                                         POLICE_UNIT_VIEW.ID == wdist.DISTRIBUTIONBY.ToString()
                                       select new
                                       {
                                           POLICE_UNIT_VIEW.UNIT_NAME
                                       }).FirstOrDefault().UNIT_NAME),
                                    DistributedTo =
                                    (new int[] { 1, 2, 4, 6, 7 }).Contains(wdist.ENTRYTYPE) ?
                                       ((from POLICE_UNIT_VIEW in db.VIEW_POLICE_UNIT
                                         where
                                           POLICE_UNIT_VIEW.ID == wdist.DISTRIBUTIONTO.ToString()
                                         select new
                                         {
                                             POLICE_UNIT_VIEW.UNIT_NAME
                                         }).FirstOrDefault().UNIT_NAME) :
                                        ((from Auths in db.VIEW_AUTH
                                          where
                                            Auths.PERSON_CODE == wdist.DISTRIBUTIONTO
                                          select new
                                          {
                                              Auths.NAME
                                          }).FirstOrDefault().NAME),
                                    wdist.STATUS,
                                    wdist.DISTRIBUTEDDATE
                                }).AsEnumerable().Select(wdd => new PRISM.Models.WeaponDistributionDetail
                                {
                                    weaponName = wdd.WEAPON_NAME,
                                    serialNumber = wdd.SERIAL_NO,
                                    distributedBy = wdd.DistributedBy,
                                    distributedTo = wdd.DistributedTo,
                                    status = wdd.STATUS,
                                    DISTRIBUTEDDATE = wdd.DISTRIBUTEDDATE
                                });
                    return PartialView("iSearchResultDetail", data);
                }
                else
                {
                    return PartialView("iSearchResultDetail");
                }
               
            }
            else if (s=="State")
            {
                ViewBag.Show = "State";
                if (Guid.TryParse(id, out g))
                {
                    var data = (from a in db.TBL_WEAPON_DISTRIBUTION
                                join b in db.VIEW_POLICE_UNIT on new { DistributionTo = a.DISTRIBUTIONTO.ToString() } equals new { DistributionTo = b.ID }
                              
                                join d in db.TBL_REGION on new { sRegion = b.STATECODE } equals new { sRegion = d.REGIONGUID}
                                where
                                  (new int[] { 1, 2, 4 }).Contains(a.ENTRYTYPE) &&
                                  a.STATUS == "active"
                                 
                                group new { d, a } by new
                                {
                                    
                                    d.REGIONNAME
                                } into gs
                                select new
                                {
                                    gs.Key.REGIONNAME,
                                   
                                    Wcount = (Int64?)gs.Count(p => p.a.FK_WEAPON_DETAIL != null)
                                }).AsEnumerable().Select(xs => new PRISM.Models.WeaponDistributionDetail { 
                                RegionName = xs.REGIONNAME,
                                
                                WeaponCnt = xs.Wcount.ToString()
                                }).ToList();
                    return PartialView("iSearchResultDetail", data);
                }
                else
                {
                    return PartialView("iSearchResultDetail");
                }
            }
            else
            {
                return PartialView("iSearchResultDetail");
            }
            
        }
    }
}
