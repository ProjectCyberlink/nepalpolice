﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PRISM.Controllers
{
    [Authorize(Roles = "Admin")]
    [SessionExpireFilter]
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class PaybookController : Controller
    {
        private NepalPoliceInventoryEntities db = new NepalPoliceInventoryEntities();
       
        public ActionResult Index1()
        {
            var itm = getPayBook();
            return View(itm);
        }

       public IEnumerable<PRISM.Models.paybookModel> getPayBook()
        {
            IEnumerable<PRISM.Models.paybookModel> data = (from a in db.TBL_ASSETLEDGER
                                                           join b in db.TBL_ASSET
                                                           on new { itemid = a.FK_ASSET_ID } equals new { itemid = b.PK_ASSET_ID }
                                                           join c in
                                                               (
                                                               (from x in db.VIEW_AUTH
                                                                select new
                                                                {
                                                                    x.PERSON_CODE,
                                                                    x.NAME,
                                                                    x.UNIT_NAME,
                                                                    x.ADDRESS
                                                                })) on new { entryfor = a.ENTRYFOR } equals new { entryfor = c.PERSON_CODE }
                                                           join d in
                                                               (
                                                               (from y in db.VIEW_AUTH
                                                                select new
                                                                {
                                                                    y.PERSON_CODE,
                                                                    y.NAME,
                                                                    y.UNIT_NAME,
                                                                    y.ADDRESS
                                                                })) on new { entryby = a.ENTRYBY } equals new { entryby = d.PERSON_CODE }

                                                           where
                                                           ((a.ENTRYTYPE == 1 && a.TRANSACTIONTYPE == 1) || (a.ENTRYTYPE == 2 && a.TRANSACTIONTYPE == 2))
                                                           orderby a.POSTINGDATE descending
                                                           select new
                                                           {
                                                               b.ITEMNAME,
                                                               entbyn = c.NAME,
                                                               entbyu = d.UNIT_NAME,
                                                               a.ENTRYFOR,
                                                               a.DESCRIPTION,
                                                               a.QUANTITY,
                                                               a.COMMENTS,
                                                               ent4 = d.NAME,
                                                               ent4n = d.UNIT_NAME,
                                                               ent4a = d.ADDRESS,

                                                               a.POSTINGDATE
                                                           }).AsEnumerable().Select(xs => new PRISM.Models.paybookModel
                                                           {
                                                               pbITEMNAME = xs.ITEMNAME,
                                                               pbPOSTINGDATE = xs.POSTINGDATE,
                                                               pbDesc = xs.DESCRIPTION,
                                                               pbQUANTITY = Math.Abs(decimal.Parse(xs.QUANTITY.ToString())),
                                                               pbENTRYFOR = xs.entbyn, // =int.Parse(xs.ItemCategoryCode.ToString()),
                                                               pbCOMMENTS = xs.COMMENTS,
                                                               pbhandedby = xs.ent4,
                                                               pbAddress = xs.ent4n + "," + xs.ent4a,
                                                           }).ToList().Take(100);



            return data;
        }
       

       public ActionResult Index()
       {
           ViewBag.PersonCode = User.Identity.Name;
           ViewBag.IsAdmin = Roles.GetRolesForUser().Contains("Admin");
           var data = GetPayBookForPersonCode(User.Identity.Name);
           return View(data);
       }

        [HttpPost]
       public ActionResult Index(string pcode)
       {
           ViewBag.PersonCode = pcode;
           ViewBag.IsAdmin = Roles.GetRolesForUser().Contains("Admin");
           var data = GetPayBookForPersonCode(pcode);
           return View(data);
       }

       public IEnumerable<PRISM.Models.paybookModel> GetPayBookForPersonCode(string pbfor)
       {
            return (from a in db.TBL_ASSETLEDGER
                    join b in db.TBL_ASSET
                    on new { itemid = a.FK_ASSET_ID } equals new { itemid = b.PK_ASSET_ID }
                    join c in
                        (
                        (from x in db.VIEW_AUTH
                        select new
                        {
                            x.PERSON_CODE,
                            x.NAME,
                            x.UNIT_NAME,
                            x.ADDRESS
                        })) on new { entryfor = a.ENTRYFOR } equals new { entryfor = c.PERSON_CODE }
                    join d in
                        (
                        (from y in db.VIEW_AUTH
                        select new
                        {
                            y.PERSON_CODE,
                            y.NAME,
                            y.UNIT_NAME,
                            y.ADDRESS
                        })) on new { entryby = a.ENTRYBY } equals new { entryby = d.PERSON_CODE }
                        join cp in (from tcp in db.TBL_CONSUMPTION_PERIOD 
                    select new {
                        tcp.FK_ITEMID,
                        tcp.CONSUMPTIONPERIOD,
                        tcp.CONSUMPTIONTIME
                    }) on new { FK_ItemId = b.PK_ASSET_ID} equals new { FK_ItemId = cp.FK_ITEMID}
                    

                    where
                    ((a.ENTRYTYPE == 1 && a.TRANSACTIONTYPE == 1) || (a.ENTRYTYPE == 2 && a.TRANSACTIONTYPE == 2))
                    &&
                    a.ENTRYFOR == pbfor
                    orderby a.POSTINGDATE descending
                    select new
                    {
                        b.ITEMNAME,
                        entbyn = c.NAME,
                        entbyu = c.UNIT_NAME,
                        a.ENTRYFOR,
                        a.DESCRIPTION,
                        a.QUANTITY,
                        a.COMMENTS,
                        ent4 = d.NAME,
                        ent4n = d.UNIT_NAME,
                        ent4a = d.ADDRESS,
                        a.POSTINGDATE,
                        cp.CONSUMPTIONPERIOD,
                        cp.CONSUMPTIONTIME
                    }).AsEnumerable().Select(xs => new PRISM.Models.paybookModel
                    {
                        pbITEMNAME = xs.ITEMNAME,
                        pbPOSTINGDATE = xs.POSTINGDATE,
                        pbDesc = xs.DESCRIPTION,
                        pbQUANTITY = Math.Abs(decimal.Parse(xs.QUANTITY.ToString())),
                        pbENTRYFOR = xs.entbyn + "," + xs.entbyu,
                        pbCOMMENTS = xs.COMMENTS,
                        pbhandedby = xs.ent4,
                        pbAddress = xs.ent4n,
                        consumpPeriod=(int)xs.CONSUMPTIONPERIOD,
                        ConsumpTime = xs.CONSUMPTIONTIME
                    }).ToList();
       }
        

    }
}
